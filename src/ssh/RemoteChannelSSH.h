#ifndef REMOTECHANNELSSH_H
#define REMOTECHANNELSSH_H

#include <libssh/libsshpp.hpp>
#include <QObject>
#include "RemoteNodeSSH.h"

// Signle use channel for simplicity.
class RemoteChannelSSH : public QObject , ssh::Channel
{
    Q_OBJECT

    Q_DISABLE_COPY( RemoteChannelSSH )


    enum Status{ OpenSession = 1  ,
                 CommandExecuted = 2 };
public:
    RemoteChannelSSH( RemoteNodeSSH &remoteNodeSSH ,
                      QObject *parent = 0 );

    void executeRemoteCommand( const std::string remoteCommand );

    void transferLocalFile( QString localFileLocation , QString remotePath );

    bool isOpenSession() const ;

    bool isCommandExecuted() const ;
private:
    void openSession_( );

Q_SIGNALS:
    void channelOpened_SIGNAL( bool opened );

    void commandExecuted_SIGNAL( bool executed );


private:

    int status_ ;

};

#endif // REMOTECHANNELSSH_H
