#include "ScanTask.h"
#include "Logger.h"


ScanTask::ScanTask( const QString address ,
                    const int port )
    :  address_( address.toStdString( )) , port_( port )
{
//    setAutoDelete( false );

}





void ScanTask::run()
{
    std::printf("Scan Task [%s]\n", address_.c_str());

    ssh_session session = ssh_new();


    ssh_options_set( session , SSH_OPTIONS_HOST , address_.c_str( ));

    ssh_options_set( session , SSH_OPTIONS_PORT , &port_);

    ssh_set_blocking( session , 1 );

    int state = ssh_connect( session );

    ssh_disconnect( session );
    ssh_free( session );


    if( state != SSH_OK )
        Q_EMIT this->detectedNode_SIGNAL( false , 0 );
    else
        Q_EMIT this->detectedNode_SIGNAL( true ,
                                        QHostAddress( QString( address_
                                                               .c_str( )))
                                        .toIPv4Address( ));


}
