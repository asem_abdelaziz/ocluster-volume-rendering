#ifndef SSHSCANNER_H
#define SSHSCANNER_H


#include <QObject>
#include <QString>
#include <QNetworkInterface>
#include <QThreadPool>
#include "ScanTask.h"

#define MAXIMUM_TASKS 255
#define MAX_SUBNET_SIZE 255



/**
 * @brief The SSHScanner class
 */
class SSHScanner : public QObject
{
    Q_OBJECT

    Q_DISABLE_COPY( SSHScanner )
public:
    /**
     * @brief SSHScanner
     * @param user
     * @param port
     */
    SSHScanner( const int port = 22 ,
                const int threadsCount = QThread::idealThreadCount( ));


    /**
     * @brief scanInterface
     * @param interfaceLabel
     */
    void scanInterface( const QString interfaceLabel );


    /**
     * @brief scanRange
     * @param ipFirst
     * @param ipLast
     */
    void scanRange( const quint32 ipFirst ,
                    const quint32 ipLast );

    /**
     * @brief getActiveMachines
     * @return
     */
    QList< quint32 > getActiveMachines();


    /**
     * @brief isBusy
     * @return
     */
    bool isBusy() const ;

    /**
     * @brief systemInterfaces
     * @return
     */
    static QMap< QString , quint32 > interfacesIPv4Address();

private:
    /**
     * @brief bruteForceScan_
     * @param scanningList
     */
    void bruteForceScan_( QList< quint32 > scanningList );

    /**
     * @brief expandSubnet_
     * @param address
     * @param subnet
     * @return
     */
    QList< quint32 > expandSubnet_( quint32 address , quint32 subnet );

Q_SIGNALS:

    /**
     * @brief finishedScan_SIGNAL
     */
    void finishedScan_SIGNAL( );

    /**
     * @brief detectedNode_SIGNAL
     * @param address
     */
    void nodeDetected_SIGNAL( quint32 address );


public Q_SLOTS:
    /**
     * @brief detectedNode_SLOT
     * @param detected
     * @param address
     */
    void detectedNode_SLOT( bool detected , quint32 address );

    quint32 min_( const quint32 a , const quint32 b );
private:

    /**
     * @brief port_
     */
    const int port_;

    /**
     * @brief scannerPool_
     */
    QThreadPool scannerPool_ ;

    /**
     * @brief activeMachines_
     */
    QList< quint32 > activeMachines_ ;

    /**
     * @brief scanCounter_
     */
    quint32 scanCounter_ ;

};

#endif // SSHSCANNER_H
