#ifndef SCANTASK_H
#define SCANTASK_H

#include <QObject>
#include <QRunnable>
#include <libssh/libssh.h>
#include <libssh/libsshpp.hpp>
#include <QHostAddress>
#include <QMutex>
#include <QThread>
#include <libssh/callbacks.h>
#include <thread>
/**
 * @brief The ScanTask class
 */
class ScanTask : public QObject , public QRunnable
{
    Q_OBJECT

    Q_DISABLE_COPY( ScanTask )
public:

    /**
     * @brief ScanTask
     * @param address
     * @param user
     * @param port
     */
    ScanTask( const QString address ,
              const int port );

protected:
    /**
     * @brief run
     */
    void run();

Q_SIGNALS:

    /**
     * @brief detectedNode_SIGNAL
     * @param detected
     * @param address
     */
    void detectedNode_SIGNAL( bool detected , quint32 address );

private:
    /**
     * @brief address_
     */
    const std::string address_ ;

    /**
     * @brief port_
     */
    const int port_;

    ssh_threads_callbacks_struct cb_;

//    QMutex &mutex_ ;
};

#endif // SCANTASK_H
