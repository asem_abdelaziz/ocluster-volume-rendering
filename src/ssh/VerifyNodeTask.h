#ifndef VERIFYNODETASK_H
#define VERIFYNODETASK_H

#include <libssh/libssh.h>
#include <QObject>
#include <QRunnable>
#include <QHostAddress>
#include "RemoteNodeSSH.h"


/**
 * @brief The VerifyNodeTask class
 * This task initiates connection and authentication
 * with the given target. If target is active on the given address and
 * matching the given credentials, then VerifyNodeTask::validRemoteNode_SIGNAL()
 * will be Q_EMITted with first argument (i.e valid) is set to true.
 *
 */
class VerifyNodeTask : public QObject , public QRunnable
{
    Q_OBJECT

    Q_DISABLE_COPY( VerifyNodeTask )

public:
    /**
     * @brief VerifyNodeTask
     * @param hostAddress
     * @param user
     * @param password
     * @param sshPort
     */
    VerifyNodeTask( const std::string hostAddress ,
                    const std::string user ,
                    const std::string password ,
                    const int sshPort = 22 );
protected:
    void run();

Q_SIGNALS :
    /**
     * @brief validRemoteNode_SIGNAL
     * @param valid
     */
    void validRemoteNode_SIGNAL( bool valid , quint32 ip );

private:
    RemoteNodeSSH remoteNodeSSH_ ;

};

#endif // VERIFYNODETASK_H
