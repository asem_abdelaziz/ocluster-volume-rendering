#include "VerifyNodeTask.h"

VerifyNodeTask::VerifyNodeTask( const std::string hostAddress ,
                                const std::string user ,
                                const std::string password ,
                                const int sshPort )
    : remoteNodeSSH_( hostAddress , user , password , sshPort  )
{
//    setAutoDelete( false );
}

void VerifyNodeTask::run()
{
    remoteNodeSSH_.fetchNode( true );

    QString address = QString( remoteNodeSSH_.getAddress().c_str());
    quint32 ip = QHostAddress( address ).toIPv4Address( );

    Q_EMIT this->validRemoteNode_SIGNAL( remoteNodeSSH_.isNodeFetched() , ip );

}
