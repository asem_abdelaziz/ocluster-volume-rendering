#include "SSHScanner.h"
#include "Logger.h"

SSHScanner::SSHScanner( const int port ,
                        const int threadCount )
    : port_( port )
{
    scannerPool_.setExpiryTimeout( -1 );
    scannerPool_.setMaxThreadCount( threadCount );
    scanCounter_ = 0 ;
}


void SSHScanner::scanInterface( const QString interfaceLabel )
{

//    if( isBusy( ))
//    {
//        LOG_DEBUG("SSHScanner is busy!");
//        return ;
//    }

    LOG_DEBUG("Scanning interface [%s]", interfaceLabel.toStdString().c_str( ));


    // Generate all addresses given an addreess and a subnet mask.
    QList< quint32 > listSubnet;


    // Return all network interfaces in the system.
    for( const QNetworkInterface &iface : QNetworkInterface::allInterfaces( ))
        // Filterate interfaces by the given interface name.
        if( iface.name() == interfaceLabel )
            // Filterate out non-IPV4 entries.
            for( const QNetworkAddressEntry &address : iface.addressEntries( ))
                if ( address.ip().protocol() ==  QAbstractSocket::IPv4Protocol)
                    if( address.ip() != QHostAddress::LocalHost )
                        listSubnet << expandSubnet_(
                                          address.ip().toIPv4Address() ,
                                          address.netmask().toIPv4Address( ));
                    else
                        listSubnet << QHostAddress( QHostAddress::LocalHost )
                                      .toIPv4Address();

    // Brute force scan.
    bruteForceScan_( listSubnet );
}

void SSHScanner::scanRange( const quint32 ipFirst ,
                            const quint32 ipLast )
{
    if( isBusy( ))
    {
        LOG_DEBUG("SSHScanner is busy!");
        return ;
    }

    if(  ipFirst > ipLast )
    {
        LOG_WARNING("First IP > Last IP!");
        return ;
    }
    if( ipLast - ipFirst >= 255 )
    {
        LOG_WARNING("Ip range is very large!");
        //        return ;
    }

    for( quint32 ip = ipFirst ;
         ip <=  ipLast && ip <=  ( ipFirst + MAXIMUM_TASKS ) ; ip++ )
    {
        const QString addressStr = QHostAddress( ip ).toString();
        ScanTask *task = new ScanTask( addressStr ,
                                       port_ );

        connect( task , SIGNAL( detectedNode_SIGNAL( bool , quint32 )),
                 this , SLOT( detectedNode_SLOT( bool , quint32 )) ,
                 Qt::BlockingQueuedConnection );
        scannerPool_.start( task );
    }
}

QList< quint32 > SSHScanner::getActiveMachines( )
{
    return activeMachines_;
}

bool SSHScanner::isBusy() const
{
    return scanCounter_ != 0 ;
}

QMap< QString, quint32 > SSHScanner::interfacesIPv4Address()
{
    QMap< QString , quint32 > interfaces ;

    // Return all network interfaces in the system.
    for( const QNetworkInterface &iface : QNetworkInterface::allInterfaces( ))
        for( const QNetworkAddressEntry &address : iface.addressEntries( ))
            // Filterate out non-IPV4 enteries.
            if ( address.ip().protocol() == QAbstractSocket::IPv4Protocol  )
                interfaces[ iface.name() ] = address.ip().toIPv4Address();

    return interfaces;
}

void SSHScanner::bruteForceScan_( QList< quint32 > scanningList )
{

    for( const quint32 address : scanningList )
    {
        const QString addressStr = QHostAddress( address ).toString();
        ScanTask *task = new ScanTask( addressStr ,
                                       port_ );

        connect( task , SIGNAL( detectedNode_SIGNAL( bool , quint32 )),
                 this , SLOT( detectedNode_SLOT( bool , quint32 )));

        scannerPool_.start( task );

        scanCounter_++;
    }
}


QList< quint32 > SSHScanner::expandSubnet_( quint32 address ,
                                            quint32 subnet  )
{
    QList< quint32 > addresses ;


    if( ~subnet > 256 )
        LOG_WARNING("The subnet to scan is too large [%d]!"
                    " this will either cause bad_alloc(s) or"
                    "very long time scanning connected machines."
                    "The scan will be performed on the first [%d] addresses "
                    "in the subnet" ,
                    ~subnet , min_( ~subnet  , MAX_SUBNET_SIZE ));


    for( quint32 i=1 ;
         i < min_( ~subnet , MAX_SUBNET_SIZE ) ;
         i++  )
    {
        std::cout << i << " ";
        addresses << ( subnet & address ) + i  ;
    }
    return addresses ;
}

void SSHScanner::detectedNode_SLOT( bool detected , quint32 address )
{

    scanCounter_-- ;

    if( detected )
    {
        activeMachines_ <<  address ;
        Q_EMIT nodeDetected_SIGNAL( address );
    }

    if( !isBusy())
        Q_EMIT this->finishedScan_SIGNAL();


    if( detected )
        LOG_DEBUG("Node detected:%s",
                  QHostAddress( address ).toString().
                  toStdString().c_str());

    return ;
}

inline quint32 SSHScanner::min_( const quint32 a , const quint32 b )
{
    return ( a < b )? a : b ;
}
