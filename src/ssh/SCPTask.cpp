//#include "SCPTask.h"
//#include "Logger.h"


//#define EXIT_TASK( cSession , coppied , ip ) \
//    do{ \
//    Q_EMIT this->fileCopied_SIGNAL( coppied , ip );\
//    if( !coppied ) \
//    LOG_WARNING("Error Copying: %s" , ssh_get_error( cSession )); \
//    ssh_disconnect( cSession );\
//    ssh_free( cSession );\
//    return; \
//    } while(0)



//SCPTask::SCPTask( const RemoteNodeSSH &remoteNodeSSH ,
//                  const int scpMode ,
//                  const QString localPath ,
//                  const QString remotePath ,
//                  QObject *parent )
//    : remoteNodeSSH_( remoteNodeSSH ) ,
//      scpMode_( scpMode ) ,
//      localPath_( localPath ) ,
//      remotePath_( remotePath ),
//      remoteAddress_( QHostAddress( remoteNodeSSH.getAddress( ))
//                      .toIPv4Address( )) ,
//      port_( remoteNodeSSH.getPort( )),
//      QObject( parent )
//{


//}

//void SCPTask::run()
//{
//    cSession_ = ssh_new();
//    if( cSession_ == 0 )
//        EXIT_TASK( cSession_ , false , remoteAddress_ );

//    ssh_options_set( cSession_ , SSH_OPTIONS_HOST ,
//                     remoteNodeSSH_.getAddress().toStdString().c_str( ));

//    ssh_options_set( cSession_ , SSH_OPTIONS_USER ,
//                     remoteNodeSSH_.getUsername().toStdString().c_str( ));

//    ssh_options_set( cSession_ , SSH_OPTIONS_PORT , &port_ );


//    if( ssh_connect( cSession_ ) != SSH_OK )
//        EXIT_TASK( cSession_ , false , remoteAddress_ );


//    switch ( ssh_is_server_known( cSession_ ))
//    {
//    case SSH_SERVER_KNOWN_OK:
//        break; /* ok */

//    case SSH_SERVER_KNOWN_CHANGED:
//        EXIT_TASK( cSession_ , false , remoteAddress_ );

//    case SSH_SERVER_FOUND_OTHER:
//        EXIT_TASK( cSession_ , false , remoteAddress_ );

//    case SSH_SERVER_FILE_NOT_FOUND:
//        /* fallback to SSH_SERVER_NOT_KNOWN behavior */

//    case SSH_SERVER_NOT_KNOWN:
//        if ( ssh_write_knownhost( cSession_ ) < 0 )
//            EXIT_TASK( cSession_ , false , remoteAddress_ );

//        break;
//    case SSH_SERVER_ERROR:
//        EXIT_TASK( cSession_ , false , remoteAddress_ );
//    }

//    const char* password = remoteNodeSSH_.getPassword().toStdString().c_str();

//    if( ssh_userauth_password( cSession_ , NULL , password ) !=
//            SSH_AUTH_SUCCESS )
//        EXIT_TASK( cSession_ , false , remoteAddress_ );


//    scp_ = ssh_scp_new( cSession_ , scpMode_ | SSH_SCP_RECURSIVE, "." );

//    if( scp_ == NULL )
//        EXIT_TASK( cSession , false , remoteAddress_ );

//    if( ssh_scp_init( scp_ ) != SSH_OK )
//    {
//        ssh_scp_free( scp_ );
//        EXIT_TASK( cSession_ , false ,);
//    }


//    ssh_scp_close( scp_ );
//    ssh_scp_free( scp_ );
//}

