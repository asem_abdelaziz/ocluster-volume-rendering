#include "RemoteNodeSSH.h"
#include "errno.h"
#include "Logger.h"

RemoteNodeSSH::RemoteNodeSSH( const std::string address ,
                              const std::string username ,
                              const std::string password ,
                              const int sshPort ,
                              QObject *parent )
    : address_( address ) ,
      username_( username ) ,
      password_( password ) ,
      status_( 0 ),
      sshPort_( sshPort ),
      QObject( parent ) ,
      ssh::Session()
{



}

const std::string &RemoteNodeSSH::getAddress() const
{
    return address_ ;
}

int RemoteNodeSSH::getPort() const
{
    return sshPort_ ;
}

const std::string &RemoteNodeSSH::getUsername() const
{
    return username_;
}

const std::string &RemoteNodeSSH::getPassword() const
{
    return password_;
}

bool RemoteNodeSSH::isConnected() const
{
    return status_ & Status::Connected ;
}

bool RemoteNodeSSH::isAuthenticationEstablished() const
{
    return ( status_ & Status::NodeAuthenticated ) &&
            ( status_ & Status::RootAuthenticated );
}

bool RemoteNodeSSH::isNodeFetched() const
{
    return status_ & Status::NodeFetched ;
}

void RemoteNodeSSH::fetchNode( const bool blockingRoutine )
{
    int verbosity = SSH_LOG_PROTOCOL;
    const char *addressCString = address_.c_str( );

    // Set hostname, verbosity, port number.
    this->setOption( SSH_OPTIONS_HOST , addressCString );
//    this->setOption( SSH_OPTIONS_LOG_VERBOSITY , &verbosity );
    this->setOption( SSH_OPTIONS_PORT ,  (void*) &sshPort_ );


    connectToNode_();
    establishAuthentication_();

    if( ( status_ & Status::Connected ) &&
            ( status_ & Status::NodeAuthenticated ) &&
            ( status_ & Status::RootAuthenticated ))
        status_ |= Status::NodeFetched;
    else
        LOG_WARNING("Error fetching node.");

    Q_EMIT this->nodeFetched_SIGNAL( this->isNodeFetched( ) , this );
}

void RemoteNodeSSH::connectToNode_()
{
    try
    {
        this->ssh::Session::connect();
        status_ |= Status::Connected ;
    }
    catch( ssh::SshException e )
    {
        LOG_WARNING("Error connecting to node <%s>: %s" ,
                    address_.c_str() ,
                    e.getError().c_str());
        status_ &= (int) !( Status::Connected );
    }

    Q_EMIT this->nodeConnected_SIGNAL( this->isConnected( ));
}

void RemoteNodeSSH::establishAuthentication_()
{
    if( !isConnected( ))
    {
        LOG_WARNING("Not connected yet with remote node!");
        return;
    }

    // Authenticate the node side (the other side).
    authenticateNode_();
    // Authenticate this side.
    authenticateRoot_();

    Q_EMIT authenticationEstablished_SIGNAL( isAuthenticationEstablished( ));
}

void RemoteNodeSSH::authenticateNode_()
{
    // Check if server (the other side) is known.
    switch(  this->isServerKnown( ))
    {
    case SSH_SERVER_KNOWN_OK :
        //Good.
        status_ |= Status::NodeAuthenticated ;
        break;

    case SSH_SERVER_KNOWN_CHANGED:
        LOG_WARNING("Host key for server changed!");
        LOG_WARNING("For security reasons,"
                    " connection will be stopped.");
        break;

    case SSH_SERVER_FOUND_OTHER:
        LOG_WARNING("The host key for this server was not "
                    "found but an other type of key exists.\n");
        LOG_WARNING("An attacker might change the default "
                    "server key to confuse your client into "
                    "thinking the key does not exist.");
        break;

    case SSH_SERVER_FILE_NOT_FOUND:
        LOG_DEBUG("Could not find known host file.");
        LOG_DEBUG("The file will be created automatically.");
        /* fallback to SSH_SERVER_NOT_KNOWN behavior */

    case SSH_SERVER_NOT_KNOWN:
        if( this->writeKnownhost() < 0 )
            LOG_WARNING("Node authentication failed: %s" ,
                        strerror(errno));
        else
            status_ |= Status::NodeAuthenticated ;

        break;

    case SSH_SERVER_ERROR :
        LOG_WARNING("Error: %s" ,
                    this->getError( )) ;
        break ;

    default :
        LOG_WARNING("Unexpected behaviour!");
        break;
    }
}

void RemoteNodeSSH::authenticateRoot_()
{
    if( !isConnected( ))
    {
        LOG_WARNING("Not connected yet with remote node!");
        return ;
    }

    if( !( status_ & Status::NodeAuthenticated ))
    {
        LOG_WARNING("Node must be authenticated first.");
        return ;
    }

    const char *usernameCString = username_.c_str( );
    const char *passwordCString = password_.c_str();

    LOG_DEBUG("Username:%s" , usernameCString );
    LOG_DEBUG("Password:%s" , passwordCString );

    this->setOption( SSH_OPTIONS_USER ,  usernameCString );

    int authenticationStatue = this->userauthPassword( passwordCString );
    if ( authenticationStatue != SSH_AUTH_SUCCESS )
        LOG_WARNING("Error authenticating with password: %s.[%s,%s]:%s" ,
                    this->getError( ) ,
                    usernameCString ,
                    passwordCString ,
                    address_.c_str()) ;
    else
        status_ |= Status::RootAuthenticated ;
}
