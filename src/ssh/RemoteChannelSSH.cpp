#include "RemoteChannelSSH.h"
#include "Logger.h"

RemoteChannelSSH::RemoteChannelSSH( RemoteNodeSSH &remoteNodeSSH  ,
                                    QObject *parent )
    : status_( 0 ) ,
      ssh::Channel( remoteNodeSSH ) ,
      QObject( parent )
{
//    this->openSession();
    openSession_();
}

void RemoteChannelSSH::executeRemoteCommand( const std::string remoteCommand )
{
    LOG_DEBUG("Executing command <%s>" , remoteCommand.c_str());
    if( !this->isOpen( ))
    {
        LOG_WARNING("Channel is not open!");
        return ;
    }

    try
    {
        this->requestExec( remoteCommand.c_str( ));
        status_ |= Status::CommandExecuted ;
    }
    catch( ssh::SshException &e )
    {
        LOG_WARNING("Error executing remote command: %s",
                    e.getError( ).c_str( ));
        status_ &= ! Status::CommandExecuted ;

    }

    Q_EMIT this->commandExecuted_SIGNAL( isCommandExecuted( ));
}

void RemoteChannelSSH::transferLocalFile( QString localFileLocation , QString remotePath )
{

}

bool RemoteChannelSSH::isOpenSession() const
{
    return status_ & Status::OpenSession ;
}

bool RemoteChannelSSH::isCommandExecuted() const
{
    return status_ & Status::CommandExecuted ;
}

void RemoteChannelSSH::openSession_()
{
    try
    {
        // Open remote shell session.
        this->openSession();

        status_ |= Status::OpenSession ;
    }
    catch( ssh::SshException &e )
    {
        LOG_WARNING("Error opening channel: %s",
                    e.getError().c_str( ));

        status_ &= ! Status::OpenSession ;
    }

    Q_EMIT this->channelOpened_SIGNAL( isOpenSession( ));
}
