#ifndef REMOTENODESSH_H
#define REMOTENODESSH_H

#include <QString>
#include <QObject>
#include <libssh/libsshpp.hpp>

/**
 * @brief The RemoteNodeSSH class
 */
class RemoteNodeSSH : public QObject , public ssh::Session
{
    Q_OBJECT

    Q_DISABLE_COPY( RemoteNodeSSH )

    /**
     * @brief The Status enum
     */
    enum Status{ Connected = 1 ,
                 NodeAuthenticated = 2 ,
                 RootAuthenticated = 4 ,
                 NodeFetched = 8 };

public:
    /**
     * @brief RemoteNodeSSH
     * @param address
     * @param username
     * @param password
     */
    RemoteNodeSSH( const std::string address ,
                   const std::string username ,
                   const std::string password ,
                   const int sshPort = 22 ,
                   QObject *parent = 0 );

    /**
     * @brief getAddress
     * @return
     */
    const std::string &getAddress( ) const ;

    /**
     * @brief getPort
     * @return
     */
    int getPort() const ;

    /**
     * @brief getUsername
     * @return
     */
    const std::string &getUsername( ) const ;

    /**
     * @brief getPassword
     * @return
     */
    const std::string &getPassword( ) const ;


    /**
     * @brief isConnected
     * @return
     */
    bool isConnected( ) const ;

    /**
     * @brief isAuthenticationEstablished
     * @return
     */
    bool isAuthenticationEstablished( ) const ;

    /**
     * @brief isNodeFetched
     * @return
     */
    bool isNodeFetched( ) const ;

    /**
     * @brief fetchNode
     * @param blockingRoutine
     */
    void fetchNode( const bool blockingRoutine = true );

    /**
     * @brief disconnectNode
     * @param blockingRoutine
     */
    void disconnectNode( const bool blockingRoutine = true );

private:
    /**
     * @brief connectToNode_
     */
    void connectToNode_();

    /**
     * @brief establishAuthentication_
     */
    void establishAuthentication_( );

    /**
     * @brief authenticateNode_
     */
    void authenticateNode_();

    /**
     * @brief authenticateRoot_
     */
    void authenticateRoot_();

Q_SIGNALS :
    /**
     * @brief clientConnected_SIGNAL
     * @param connected
     */
    void nodeConnected_SIGNAL( bool connected );

    /**
     * @brief nodeAuthenticationEstablished_SIGNAL
     * @param authenticated
     */
    void authenticationEstablished_SIGNAL( bool authenticated );


    /**
     * @brief nodeFetched_SIGNAL
     * @param fetched
     */
    void nodeFetched_SIGNAL( bool fetched , RemoteNodeSSH *thisPtr );

public Q_SLOTS:


private:
    /**
     * @brief address_
     */
    const std::string address_ ;

    /**
     * @brief sshPort_
     */
    const int sshPort_ ;
    /**
     * @brief username_
     */
    const std::string username_ ;

    /**
     * @brief password_
     */
    const std::string password_ ;

    /**
     * @brief status_
     */
    uint status_ ;

};

#endif // REMOTENODESSH_H
