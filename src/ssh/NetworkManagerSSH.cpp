#include "NetworkManagerSSH.h"
#include "Logger.h"
#include <QTextStream>
#include "SSHThreadsCallbacks.h"

NetworkManagerSSH::NetworkManagerSSH( const std::string user ,
                                      const std::string password ,
                                      const int sshPort ,
                                      const int maxThreadsCount ,
                                      QObject *parent )
    : user_( user ) ,
      password_( password ),
      sshPort_( sshPort ),
      sshScanner_( sshPort , maxThreadsCount ) ,
      verificationCounter_( 0 ),
      QObject( parent )
{
    /////////////////// VERY IMPORTANT //////////////////
    ///
    /// To support multithreading in ssh provide some callbacks
    /// to the libssh to get control on QThreads.

    ssh_threads_set_callbacks( SSHThreadsCallbacks::get( ));
    ssh_init();


    //    connect( &sshScanner_ , SIGNAL(finishedScan_SIGNAL()) ,
    //             this , SLOT( sshScanFinished_SLOT( )));

    connect( &sshScanner_ , SIGNAL( nodeDetected_SIGNAL( quint32 )) ,
             this , SLOT( nodeDetected_SLOT( quint32 )));

    threadPool_.setExpiryTimeout( -1 );
}

void NetworkManagerSSH::surveyRemoteNodes()
{
    if( isBusy( ))
    {
        LOG_DEBUG("NetworkManagerSSH is busy.");
        return ;
    }

    interactiveScanSSH_();
}

void NetworkManagerSSH::scanInterface( const QString interfaceLabel )
{
    if( isBusy( ))
    {
        LOG_DEBUG("NetworkManagerSSH is busy.");
        return ;
    }

    sshScanner_.scanInterface( interfaceLabel );
}

/**
 * @brief NetworkManagerSSH::scanRange
 * @param ipFirst
 * @param ipLast
 */
void NetworkManagerSSH::scanRange( const quint32 ipFirst,
                                   const quint32 ipLast )
{
    if( isBusy( ))
    {
        LOG_DEBUG("NetworkManagerSSH is busy.");
        return ;
    }

    sshScanner_.scanRange( ipFirst , ipLast );
}

void NetworkManagerSSH::fetchRemoteNodes( const QList< quint32 > ipList ,
                                          const std::string username ,
                                          const std::string password )
{

    for( const quint32 ip : ipList )
    {
        const std::string addressStr =
                QHostAddress( ip ).toString().toStdString();
        RemoteNodeSSH *remoteNode =
                new RemoteNodeSSH( addressStr ,
                                   ( username == "" )? user_ : username  ,
                                   ( password == "" )? password_ : password ,
                                   sshPort_ );

        remoteNode->fetchNode();

        if( remoteNode->isNodeFetched( ))
        {
            attachedNodes_[ ip ] = remoteNode ;

            if( !sshActiveMachines_.contains( ip ))
                sshActiveMachines_ << ip ;
        }
        else
            delete remoteNode ;

    }
    Q_EMIT this->remoteNodesFetched_SIGNAL();

}

bool NetworkManagerSSH::executeRemoteCommand( const quint32 ip ,
                                              const std::string command )
{
    if( !attachedNodes_.contains( ip ))
    {
        LOG_WARNING("No active SSH connection with the given ip [%s]. ",
                    QHostAddress( ip ).toString().toStdString().c_str());
        return false ;
    }

    RemoteNodeSSH *remoteNode = attachedNodes_[ ip ];
    RemoteChannelSSH channel( *remoteNode );
    channel.executeRemoteCommand( command );
    return channel.isCommandExecuted();
}

void NetworkManagerSSH::verifyRemoteNode( const quint32 ip,
                                          const std::string username,
                                          const std::string password )
{
    const std::string addressStr =
            QHostAddress( ip ).toString().toStdString();

    VerifyNodeTask *task = new VerifyNodeTask( addressStr ,
                                               username  ,
                                               password ,
                                               sshPort_ ) ;

    connect( task , SIGNAL(validRemoteNode_SIGNAL( bool , quint32 )) ,
             this , SLOT(nodeVeried_SLOT( bool , quint32 )));

    verificationCounter_++;
    threadPool_.start( task );
}


QList< quint32 >
NetworkManagerSSH::executeRemoteCommandAll( const std::string command )
{
    QList< quint32 > responsiveNodes ;
    for( const quint32 ip : attachedNodes_.keys( ))
    {
        LOG_DEBUG("Machine to execute command: %s",
                  QHostAddress(ip).toString().toStdString().c_str());

        RemoteNodeSSH *remoteNode = attachedNodes_[ ip ];
        RemoteChannelSSH channel( *remoteNode );
        channel.executeRemoteCommand( command );
        if( channel.isCommandExecuted( ))
            responsiveNodes << ip ;
    }

    return responsiveNodes ;
}

const QSet< quint32 > &NetworkManagerSSH::getSSHActiveMachines() const
{
    return sshActiveMachines_ ;
}

const QSet< quint32 > &NetworkManagerSSH::getVerfiedRemoteNodes() const
{
    return verifiedRemoteNodes_ ;
}

const QMap< quint32 , RemoteNodeSSH * > &NetworkManagerSSH::getAttachedNodes() const
{
    return attachedNodes_ ;
}

bool NetworkManagerSSH::isAttached( quint32 ip ) const
{
    return attachedNodes_.keys().contains( ip );
}

QHostAddress NetworkManagerSSH::getCorrespondingInterface( const QHostAddress ip )
{

    // Return all network interfaces in the system.
    QList< QNetworkInterface > interfaces = QNetworkInterface::allInterfaces();

    for( const QNetworkInterface &interface : interfaces )
        for( const QNetworkAddressEntry &entry : interface.addressEntries( ))
        {
            quint32 result =
                    entry.netmask().toIPv4Address() &
                    ip.toIPv4Address();

            quint32 subnet = entry.netmask().toIPv4Address() &
                             entry.ip().toIPv4Address();

            if(( result == subnet ) && subnet != 0 )
                return entry.ip();
        }
}

bool NetworkManagerSSH::isBusy() const
{
    return verificationCounter_ != 0 ;
}

void NetworkManagerSSH::interactiveScanSSH_()
{
    // Return all network interfaces in the system.
    QList< QNetworkInterface > interfaces = QNetworkInterface::allInterfaces();

    std::cout << "Active interfaces (choose one to scan for "
                 "connected machines:" << std::endl;


    // Filterate interfaces by the given interface name.
    for( const QNetworkInterface &interface : interfaces )
        std::cout << "[" << interface.name().toStdString() << "]" << std::endl ;

    std::cout << "Interface name:" ;
    QTextStream textStream( stdin );
    QString interfaceName ;
    textStream >> interfaceName ;


    scanInterface( interfaceName );

}

void NetworkManagerSSH::verifyRemoteNodes_()
{

    QList< quint32 > activeMachines = sshScanner_.getActiveMachines( );

    for( const quint32 &it : activeMachines )
        sshActiveMachines_ << it ;

    for( const quint32 address : activeMachines )
    {
        const std::string addressStr =
                QHostAddress( address ).toString().toStdString();
        VerifyNodeTask *task = new VerifyNodeTask( addressStr ,
                                                   user_ ,
                                                   password_ ,
                                                   sshPort_ ) ;

        connect( task , SIGNAL(validRemoteNode_SIGNAL( bool , quint32 )) ,
                 this , SLOT(nodeVeried_SLOT( bool , quint32 )));

        verificationCounter_++;
        threadPool_.start( task );

    }
}

void NetworkManagerSSH::nodeDetected_SLOT( quint32 ip )
{
    const std::string addressStr =
            QHostAddress( ip ).toString().toStdString();
    VerifyNodeTask *task = new VerifyNodeTask( addressStr ,
                                               user_ ,
                                               password_ ,
                                               sshPort_ ) ;

    connect( task , SIGNAL(validRemoteNode_SIGNAL( bool , quint32 )) ,
             this , SLOT(nodeVeried_SLOT( bool , quint32 )));

    verificationCounter_++;
    threadPool_.start( task );
}

void NetworkManagerSSH::nodeVeried_SLOT( bool valid , quint32 ip )
{
    verificationCounter_-- ;

    if( valid )
    {
        verifiedRemoteNodes_ << ip ;
        sshActiveMachines_ << ip ;
    }

    if( !isBusy( ))
        Q_EMIT this->remoteNodesVerified_SIGNAL();

}
