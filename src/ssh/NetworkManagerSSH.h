#ifndef NETWORKMANAGERSSH_H
#define NETWORKMANAGERSSH_H

#include <QObject>
#include <libssh/libssh.h>
#include "RemoteNodeSSH.h"
#include "RemoteChannelSSH.h"
#include "SSHScanner.h"
#include "VerifyNodeTask.h"

/**
 * @brief The NetworkManagerSSH class
 */
class NetworkManagerSSH : public QObject
{
    Q_OBJECT

    Q_DISABLE_COPY( NetworkManagerSSH )

public:

    /**
     * @brief NetworkManagerSSH
     * @param user
     * @param password
     * @param sshPort
     * @param maxThreadsCount
     */
    NetworkManagerSSH( const std::string user  ,
                       const std::string password  ,
                       const int sshPort  ,
                       const int maxThreadsCount  ,
                       QObject *parent = 0 );


    /**
     * @brief surveyRemoteNodes
     */
    void surveyRemoteNodes( );


    /**
     * @brief scanInterface
     * @param interfaceLabel
     */
    void scanInterface( const QString interfaceLabel );

    /**
     * @brief scanRange
     * @param ipFirst
     * @param ipLast
     */
    void scanRange( const quint32 ipFirst , const quint32 ipLast );

    /**
     * @brief fetchRemoteNodes
     */
    void fetchRemoteNodes( const QList< quint32 > ipList ,
                           const std::string username = "" ,
                           const std::string password = "" );

    /**
     * @brief executeRemoteCommand
     * @param ip
     * @param command
     * @return
     */
    bool executeRemoteCommand( const quint32 ip  ,
                               const std::string command );

    /**
     * @brief verifyRemoteNode
     * @param ip
     * @param username
     * @param password
     */
    void verifyRemoteNode( const quint32 ip ,
                           const std::string username ,
                           const std::string password );


    /**
     * @brief executeRemoteCommandAll
     * @return List of all addresses where command successfully executed.
     */
    QList< quint32 > executeRemoteCommandAll( const std::string command );

    /**
     * @brief getSSHActiveMachines
     * @return
     */
    const QSet< quint32 > &getSSHActiveMachines() const ;

    /**
     * @brief getVerfiedRemoteNodes
     * @return
     */
    const QSet< quint32 >  &getVerfiedRemoteNodes() const ;

    /**
     * @brief getAttachedNodes
     * @return
     */
    const QMap< quint32 , RemoteNodeSSH* > &getAttachedNodes() const ;


    /**
     * @brief isFetched
     * @param ip
     * @return
     */
    bool isAttached( quint32 ip ) const ;


    /**
     * @brief getCorrespondingInterface
     * @param ip
     * @return
     * return interface which coincides with the same subnet of the given
     * ip.
     */
    static QHostAddress getCorrespondingInterface( const QHostAddress ip );


    /**
     * @brief isBusy
     * @return
     */
    bool isBusy() const ;

private:
    /**
     * @brief interactiveScanSSH_
     */
    void interactiveScanSSH_();

    /**
     * @brief verifyRemoteNodes_
     */
    void verifyRemoteNodes_();

Q_SIGNALS:

    /**
     * @brief remoteNodesVerified_SIGNAL
     */
    void remoteNodesVerified_SIGNAL();

    /**
     * @brief remoteNodesFetched_SIGNAL
     */
    void remoteNodesFetched_SIGNAL();


    /**
     * @brief busy_SIGNAL
     * @param busy
     */
    void busy_SIGNAL( bool busy );


public Q_SLOTS:

    /**
     * @brief nodeDetected_SLOT
     * @param ip
     */
    void nodeDetected_SLOT( quint32 ip );

    /**
     * @brief nodesVerificationFinished_SLOT
     */
    void nodeVeried_SLOT( bool valid , quint32 ip );


private:
    /**
     * @brief user_
     */
    const std::string user_ ;

    /**
     * @brief password_
     */
    const std::string password_ ;

    /**
     * @brief sshPort_
     */
    const int sshPort_ ;

    /**
     * @brief sshScanner_
     */
    SSHScanner sshScanner_ ;

    /**
     * @brief nodeScanner_
     */
    QThreadPool threadPool_ ;

    /**
     * @brief sshActiveMachines_
     */
    QSet< quint32 > sshActiveMachines_;

    /**
     * @brief verifiedUsers_
     */
    QSet< quint32 > verifiedRemoteNodes_ ;

    /**
     * @brief attachedNodes_
     */
    QMap< quint32 , RemoteNodeSSH* > attachedNodes_ ;

    /**
     * @brief verificationCounter_
     */
    quint32 verificationCounter_ ;
};

#endif // NETWORKMANAGERSSH_H
