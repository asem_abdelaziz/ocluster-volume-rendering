#include "SSHThreadsCallbacks.h"


struct ssh_threads_callbacks_struct SSHThreadsCallbacks::callbacks_ = {
    "threads_cpp",
    mutexInit,
    mutexDestroy,
    mutexLock,
    mutexUnlock,
    threadId
};

unsigned long SSHThreadsCallbacks::threadIdNext_ {};


QMap< QThread* , int > SSHThreadsCallbacks::threadIdMap_{};

QMutex SSHThreadsCallbacks::threadIdMutex_ {};


int SSHThreadsCallbacks::mutexInit( void **prive )
{
    *prive = new QMutex();
    return 0 ;
}

int SSHThreadsCallbacks::mutexLock( void **lock )
{
    ((QMutex*)*lock)->lock();
    return 0 ;
}

int SSHThreadsCallbacks::mutexUnlock(void **lock)
{
    ((QMutex*)*lock)->unlock();
    return 0 ;
}

int SSHThreadsCallbacks::mutexDestroy(void **lock)
{
    delete (QMutex*) *lock;
    return 0 ;
}

unsigned long SSHThreadsCallbacks::threadId()
{
    QMutexLocker lock( &threadIdMutex_ );

    QMap< QThread* , int >::iterator it =
            threadIdMap_.find(  QThread::currentThread( ));
    if( it == threadIdMap_.end())
    {
        unsigned long id = ++threadIdNext_;
        threadIdMap_[ QThread::currentThread() ] = id ;
        return id ;
    }

    return it.value();
}

ssh_threads_callbacks_struct *SSHThreadsCallbacks::get()
{
    return &callbacks_;
}
