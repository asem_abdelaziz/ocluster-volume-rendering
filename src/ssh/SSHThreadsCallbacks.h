#ifndef SSHTHREADSCALLBACKS_H
#define SSHTHREADSCALLBACKS_H
#include <QMutex>
#include <QMutexLocker>
#include <QThread>
#include <QMap>
#include <libssh/callbacks.h>

class SSHThreadsCallbacks
{
public:

    static int mutexInit( void **prive );

    static int mutexLock( void **mutex );

    static int mutexUnlock( void **mutex );

    static int mutexDestroy( void **mutex );

    static long unsigned threadId( );

    static struct ssh_threads_callbacks_struct *get( );

private:

    /// %Mutex for serializing access to static data members
    static QMutex threadIdMutex_;



    static QMap< QThread* , int > threadIdMap_ ;

    /// libssh callbacks structure
    static struct ssh_threads_callbacks_struct callbacks_;


    /// Next thread ID
    static unsigned long threadIdNext_ ;

};

#endif // SSHTHREADSCALLBACKS_H
