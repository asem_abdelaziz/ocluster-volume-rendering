#include "TestSerialization.h"


TestSerialization::TestSerialization()
{
}

//void TestSerialization::testVolumeSerializationUchar()
//{

////    REGISTER_STREAM_OPERATOR_VOLUME();

//    const Dimensions3D volumeDimensions( qrand() %512,
//                                         qrand() %512,
//                                         qrand() %512 );

//    const Coordinates3D volumeCoordinates( qrand() %512,
//                                           qrand() %512,
//                                           qrand() %512 );

//    const Coordinates3D volumeUnitCubeCenter(
//                volumeCoordinates.x / volumeDimensions.x ,
//                volumeCoordinates.y / volumeDimensions.y ,
//                volumeCoordinates.z / volumeDimensions.z );

//    const Coordinates3D unitCubeScaleFactors( 1 , 1 , 1 );

//    uchar *data = new uchar[ volumeDimensions.volumeSize() ];
//    for( quint64 i = 0 ; i < volumeDimensions.volumeSize() ; i++ )
//        data[ i ] = qrand() % 200 ;



//    const Volume< uchar > volume = Volume< uchar >( volumeCoordinates ,
//                                                    volumeDimensions ,
//                                                    volumeUnitCubeCenter ,
//                                                    unitCubeScaleFactors ,
//                                                    data );

//    const SerializableVolume< uchar > serializedVolume( volume );


//    const Serializable toSerialize =
//            Serializable::fromValue( serializedVolume );

//    const Serializable deserialized = getDeserialized( toSerialize );

//    SerializableVolume< uchar > deserializedVolume =
//            deserialized.value< SerializableVolume< uchar >>();

//    QVERIFY2( QString( toSerialize.typeName( )) ==
//              QString( deserialized.typeName( )) ,
//              "!QVariant!" ) ;

//    QVERIFY2( serializedVolume.getCoordinates() ==
//              deserializedVolume.getCoordinates() ,
//              "!Coordinates!" ) ;

//    QVERIFY2( serializedVolume.getDimensions() ==
//              deserializedVolume.getDimensions() ,
//              "!Dimensions!");

//    QVERIFY2( serializedVolume.getUnitCubeCenter() ==
//              deserializedVolume.getUnitCubeCenter(),
//              "!UnitCubeCenter!") ;

//    QVERIFY2( serializedVolume.getUnitCubeScaleFactors() ==
//              deserializedVolume.getUnitCubeScaleFactors(),
//              "!UnitCubeScaleFactors!");

//    QVERIFY2( serializedVolume.checksum() ==
//              deserializedVolume.checksum() ,
//              "!Checksum!");
//}

//void TestSerialization::testFrameSerializationFloat()
//{
//    const Dimensions2D frameDimensions( qrand()%1000 ,
//                                        qrand()%1000 );

//    float *data = new float[ frameDimensions.imageSize()];

//    for( quint64 i = 0 ; i < frameDimensions.imageSize() ; i++ )
//        data[ i ] =
//                static_cast< float >( qrand() ) /
//                static_cast< float >( RAND_MAX );


//    SerializableFrame< float > serializedFrame( data , frameDimensions );


//    const Serializable toSerialize =
//            Serializable::fromValue( serializedFrame );

//    const Serializable deserialized = getDeserialized( toSerialize );

//    SerializableFrame< float > deserializedFrame =
//            deserialized.value< SerializableFrame< float >>();


//    QVERIFY2( QString( toSerialize.typeName( )) ==
//              QString( deserialized.typeName( )) ,
//              "!QVariant!" ) ;

//    QVERIFY2( serializedFrame.getDimensions() ==
//              deserializedFrame.getDimensions() ,
//              "!Dimensions!");


//    QVERIFY2( serializedFrame.checksum() ==
//              deserializedFrame.checksum() ,
//              "!Checksum!");
//}

//Serializable
//TestSerialization::getDeserialized( const Serializable toSerialize )
//{
//    Serializable deserializedVariant ;

//    QByteArray buffer;
//    StreamType stream( &buffer , QIODevice::WriteOnly );
//    stream.setVersion( StreamType::Qt_4_0 );
//    stream << toSerialize ;

//    StreamType stream2(&buffer , QIODevice::ReadOnly );
//    stream2.setVersion( StreamType::Qt_4_0 );
//    stream2 >> deserializedVariant;

//    return deserializedVariant ;
//}


QTEST_APPLESS_MAIN(TestSerialization)

#include "TestSerialization.moc"
