#ifndef CLRENDEREREND_H
#define CLRENDEREREND_H

// std
#include <map>

// local
#include "clparen/clparen.h"

#include "RemoteObject.h"
#include "TcpChannel.h"
#include "CLRendererNodeInfo.h"

#include "SerializableTransformation.h"
#include "SerializableVolume.h"

#define FRAME_WIDTH 512
#define FRAME_HEIGHT 512


namespace ocluster
{
namespace Node
{

template< class V , class F >
class CLRendererEnd : public RemoteObject
{
    Q_DISABLE_COPY( CLRendererEnd )

public:

    CLRendererEnd( const QHostAddress serverAddress ,
                   const int serverPort ,
                   QObject *parent = 0 );


    void instructionReceived_SLOT( Instruction instruction ) Q_DECL_OVERRIDE;
    void instructionTransmitted_SLOT( Instruction ) Q_DECL_OVERRIDE;


private:

    /**
     * @brief _parallelRendering
     */
    void _parallelRenderTransmit();

    /**
     * @brief renderTransmit_
     */
    void renderTransmit_( int gpuIndex );


    /**
     * @brief deployGPU_
     * @param gpuIndex
     */
    void deployGPU_( const quint32 gpuIndex );


    /**
     * @brief loadVolume_
     * @param gpuIndex
     */
    void loadVolume_( const SerializableVolume<V> &v );


    /**
     * @brief retrieveUsername_
     */
    QString retrieveUsername_();

private:
    /**
     * @brief nodeInfo_
     */
    CLRendererNodeInfo nodeInfo_ ;

    /**
     * @brief serializableVolume_
     */
    std::map< quint32 , SerializableVolume< V >> serializableVolumes_ ;


    /**
     * @brief globalTransformations_
     */
    Transformation globalTransformations_ ;


    //oclHWDl utilities
    /**
     * @brief clHardware_
     */
    oclHWDL::Hardware  clHardware_;

    /**
     * @brief listGPUs_
     */
    oclHWDL::Devices listGPUs_;

    /**
     * @brief machineGPUsCount_
     */
    uint machineGPUsCount_ ;

    /**
     * @brief clRenderers_
     */
    std::map< int , clparen::Renderer::CLRenderer< V , F >* > clRenderers_ ;
    std::map< int , std::future< void >> _renderingTasks;


    /**
     * @brief rendererPool_
     * for rendering tasks.
     */
    QThreadPool *threadPool_ ;


};

}
}
#endif // CLRENDEREREND_H
