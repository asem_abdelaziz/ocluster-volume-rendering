#ifndef CLRENDERERNODE_IPP
#define CLRENDERERNODE_IPP

namespace ocluster
{
namespace Node
{

//template class CLusterSortLast< uint8_t , uint8_t >;
//template class CLusterSortLast< uint8_t , uint16_t >;
//template class CLusterSortLast< uint8_t , uint32_t >;
//template class CLusterSortLast< uint8_t , half  >;
template class CLRendererNode< uchar , float > ;


}
}

#endif // CLRENDERERNODE_IPP
