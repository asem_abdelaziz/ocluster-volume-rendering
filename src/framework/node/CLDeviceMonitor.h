#ifndef CLDEVICEMONITOR_H
#define CLDEVICEMONITOR_H


#include <QtGlobal>
#include <QMetaType>

struct CLDeviceMonitor
{

    CLDeviceMonitor();

    /**
     * @brief volumeSize
     * Volume size loaded to GPU.
     */
    quint64 volumeSize;

    /**
     * @brief segmentsCount
     */
    quint64 segmentsCount;

    /**
     * @brief volumeLoadingProgress
     */
    quint64 segmentsSent;

    /**
     * @brief renderingAverageTime
     */
    float renderingAverageTime;

    /**
     * @brief metaTypeRegistered
     */
    static bool metaTypeRegistered ;
};

Q_DECLARE_METATYPE( CLDeviceMonitor )


#endif // CLDEVICEMONITOR_H
