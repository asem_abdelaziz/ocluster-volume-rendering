#include "CLRendererEnd.h"
#include "Logger.h"


namespace ocluster
{
namespace Node
{


template< class V , class F >
CLRendererEnd< V , F >::CLRendererEnd( const QHostAddress serverAddress,
                                       const int serverPort ,
                                       QObject *parent )
    : RemoteObject( serverAddress, serverPort , parent )
{

    listGPUs_ = clHardware_.getListGPUs();
    machineGPUsCount_ = listGPUs_.size();


    nodeInfo_.setName( retrieveUsername_());



    for( uint i = 0 ; i < listGPUs_.size() ; i++ )
    {
        oclHWDL::Device *device = listGPUs_[ i ] ;
        QString vendor = QString::fromStdString( device->getVendor( ));
        QString deviceName = QString::fromStdString( device->getName( ));
        quint64 globalMemorySize = device->getGlobalMemorySize( );

        LOG_DEBUG("GPU NAME: [%s]", device->getName().c_str());
        nodeInfo_.addRendererDeviceInfo( i ,
                                         vendor ,
                                         deviceName ,
                                         globalMemorySize );
    }

    threadPool_ = new QThreadPool();
    threadPool_->setExpiryTimeout( -1 );

}

template< class V , class F >
void CLRendererEnd< V , F >::instructionReceived_SLOT( Instruction instruction )
{
    const Instruction::Message message = instruction.getMessage();
    try{

        // If it is a rendering request, immediately process.
        // Otherwise, go for switch-case.
        if( message == Instruction::Message::Render )
        {
            LOG_DEBUG("Rendering Request");
            auto transformation =
                    dynamic_cast< const SerializableTransformation& >( instruction.getData());
            globalTransformations_ = transformation ;
            _parallelRenderTransmit();

        }
        else
        {
            switch( message )
            {

                case Instruction::Message::SwitchRenderingKernel :
                {
                    LOG_DEBUG("Switch Rendering Kernel");
                    auto mode = dynamic_cast< const SerializableNumber< quint32 >& >(
                                    instruction.getData());

                    clparen::CLKernel::RenderingMode renderingMode =
                            static_cast< clparen::CLKernel::RenderingMode >( mode.getNumber());

                    for( const auto &renderer : clRenderers_ )
                        clRenderers_[ renderer.first ]->switchRenderingKernel( renderingMode );
                } break;
                case Instruction::Message::NodeInfo :
                {
                    LOG_DEBUG("Node Info Request");
                    _controlChannel->sendInstruction(
                                Instruction::Message::NodeInfo,
                                Instruction::MessageDirection::Reply ,
                                Instruction::DataType::NodeInfo ,
                                nodeInfo_.toSharedPointer());
                } break;

                case Instruction::Message::LoadVolume :
                {
                    //                LOG_DEBUG("Load Volume Request");
                    const SerializableVolume< V >& v =
                            dynamic_cast< const SerializableVolume< V >& >(
                                instruction.getData());

                    loadVolume_( v );
                } break;
                case Instruction::Message::DeployGPU :
                {
                    LOG_DEBUG("Deploy GPU Request.");
                    auto gpuIndex = dynamic_cast< const SerializableNumber< quint32 >& >(
                                        instruction.getData());

                    deployGPU_( gpuIndex.getNumber());
                } break;

            }
        }
    } catch( const std::bad_cast &e )
    {
        LOG_ERROR("Bad cast!");
    }


}

template< class V , class F >
void CLRendererEnd< V , F >::renderTransmit_( int gpuIndex )
{
    auto &clRenderer = *clRenderers_.at( gpuIndex );
    // 1. Start Rendering on Device.
    clRenderer.applyTransformation();
    // 2. Load Rendered Frame from Device.
    clRenderer.downloadFrame( );
    SerializableFrame< F > serializableFrame( clRenderer.getCLImage2D( ));
    serializableFrame.setGPUIndex( clRenderer.getGPUIndex( ));

    _frameChannel->sendInstruction_SYNC( Instruction::Message::Render ,
                                         Instruction::MessageDirection::Reply ,
                                         Instruction::DataType::Frame ,
                                         &serializableFrame );
}

template< class V , class F >
void CLRendererEnd< V , F >::deployGPU_( const quint32 gpuIndex )
{
    serializableVolumes_.emplace( gpuIndex , SerializableVolume< V >());

    clRenderers_[ gpuIndex ] =
            new clparen::Renderer::CLRenderer< V , F >(
                gpuIndex ,
                globalTransformations_ ,
                Dimensions2D( FRAME_WIDTH , FRAME_HEIGHT ) ,
                clparen::CLData::FRAME_CHANNEL_ORDER::ORDER_INTENSITY );

    _controlChannel->sendInstruction_SYNC(
                Instruction::Message::DeployGPU ,
                Instruction::MessageDirection::Reply ,
                Instruction::DataType::UInterger32 ,
                SerializableNumber< quint32 >( gpuIndex ));

}

template< class V , class F >
void CLRendererEnd< V , F >::loadVolume_( const SerializableVolume< V > &v )
{

    assertThread_();

    const quint32 gpuIndex = v.getTargetGPUIndex() ;
    SerializableVolume< V > &targetVolume =
            serializableVolumes_[ gpuIndex ];

    targetVolume.insertSegment( v );

    if( targetVolume.allSegmentsAvailable( ))
    {
        targetVolume.mergeSegments();

        Volume< V > *volume =
                new Volume< V >( targetVolume.getCoordinates( ) ,
                                 targetVolume.getDimensions( ) ,
                                 targetVolume.getUnitCubeCenter( ) ,
                                 targetVolume.getUnitCubeScaleFactors( ) ,
                                 targetVolume.getData( ));

        quint64 checksum = 0 ;
        for( quint64 i = 0 ; i < volume->getDimensions().volumeSize() ; i++ )
            checksum += volume->getData()[ i ];


        clRenderers_.at( gpuIndex )->loadVolume( volume );

        nodeInfo_.setVolumeLoaded( true , gpuIndex );

        for( const auto & renderer : clRenderers_ )
        {
            const auto gpuIndex = renderer.first;
            if( nodeInfo_.volumeLoaded( gpuIndex ))
                LOG_DEBUG("Volume Loaded to <%d>" , gpuIndex );
            else return ;
        }

        LOG_DEBUG("All devices are loaded");
        _controlChannel->sendInstruction_SYNC(
                    Instruction::Message::NodeReady ,
                    Instruction::MessageDirection::Request );
    }
}


template< class V , class F >
QString CLRendererEnd< V , F >::retrieveUsername_()
{

    QString name = qgetenv("USER");

    if( name.isEmpty())
        name = qgetenv("USERNAME");

    if( name.isEmpty())
    {

#ifdef Q_OS_WIN
        char acUserName[MAX_USERNAME];
        DWORD nUserName = sizeof(acUserName);
        if(GetUserName(acUserName, &nUserName))
            name = QString( acUserName );

#endif

    }

    if( name.isEmpty())
        name = QString("Unknown");


    return name;

}

template< class V , class F >
void CLRendererEnd< V , F >::instructionTransmitted_SLOT(Instruction )
{

}

template< class V , class F >
void CLRendererEnd< V , F >::_parallelRenderTransmit()
{
    for( auto &renderer : clRenderers_ )
    {
        LOG_DEBUG("Spawning thread..");
        _renderingTasks[ renderer.first ] =
                std::async( std::launch::async ,
                            &CLRendererEnd< V , F >::renderTransmit_ ,
                            this , renderer.first );
        LOG_DEBUG("[DONE] Spawning thread..");
    }
}


}
}

#include "CLRendererEnd.ipp"
