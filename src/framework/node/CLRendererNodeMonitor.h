#ifndef CLRENDERERNODEMONITOR_H
#define CLRENDERERNODEMONITOR_H

#include <CLRendererNodeInfo.h>
#include <SerializableVolume.h>
#include <CLDeviceMonitor.h>
// Forward declaration: to be a friend class.
class CLRendererNode ;




class CLRendererNodeMonitor
{
public:
    CLRendererNodeMonitor();

private:
    friend class CLRendererNode ;

    /**
     * @brief volumeTransmitted_
     * @param volume
     */
    void volumeTransmitted_( const SerializableVolume< uchar > &volume );

private:
    /**
     * @brief deviceMonitor_
     */
    QMap< quint32 , CLDeviceMonitor > deviceMonitor_ ;

    /**
     * @brief frameAverageTransmittingTime_
     */
    float frameAverageTransmittingTime_ ;


};

#endif // CLRENDERERNODEMONITOR_H
