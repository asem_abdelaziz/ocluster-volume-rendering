#include "RemoteObject.h"

namespace ocluster
{
namespace Node
{


RemoteObject::RemoteObject(
        const quintptr socketDescriptor ,
        QObject *parent )
    : QObject(parent)
{
    qRegisterMetaType< Instruction >("Instruction");

    _controlChannel.reset( new TcpChannel( socketDescriptor ));

    connect( _controlChannel.get() ,
             SIGNAL(instructionReceived_SIGNAL( Instruction )),
             this , SLOT(instructionReceived_SLOT( Instruction ))) ;

    connect( _controlChannel.get() , SIGNAL(disconnected_SIGNAL()) ,
             this , SLOT(nodeDisconnected_SLOT( )));


    ip_ = _controlChannel->getSocket().peerAddress().toIPv4Address();
    ipStr_ = _controlChannel->getSocket().peerAddress().toString().toStdString();

    networkThreadPool_ = new QThreadPool();
    networkThreadPool_->setExpiryTimeout( -1 );
    networkThreadPool_->setMaxThreadCount( 2 );
}

RemoteObject::RemoteObject(
        const QHostAddress serverAddress ,
        const int serverPort, QObject *parent )
    : QObject( parent )
{

    qRegisterMetaType< Instruction >("Instruction");

    _controlChannel.reset( new TcpChannel( serverAddress , serverPort ));
    _frameChannel.reset( new TcpChannel( serverAddress , serverPort + 1 ));


    connect( _controlChannel.get() ,
             SIGNAL(instructionReceived_SIGNAL( Instruction )),
             this , SLOT(instructionReceived_SLOT( Instruction ))) ;

    connect( _controlChannel.get() , SIGNAL(disconnected_SIGNAL()) ,
             this , SLOT(nodeDisconnected_SLOT( )));


    connect( _frameChannel.get() ,
             SIGNAL(instructionReceived_SIGNAL( Instruction )),
             this , SLOT(frameReceived_SLOT( Instruction ))) ;

    connect( _frameChannel.get() , SIGNAL(disconnected_SIGNAL()) ,
             this , SLOT(nodeDisconnected_SLOT( )));

    ip_ = _controlChannel->getSocket().peerAddress().toIPv4Address();
    ipStr_ = _controlChannel->getSocket().peerAddress().toString().toStdString();

    networkThreadPool_ = new QThreadPool();
    networkThreadPool_->setExpiryTimeout( -1 );
    networkThreadPool_->setMaxThreadCount( 2 );

}

const QTcpSocket &RemoteObject::getControlSocket() const
{
    return _controlChannel->getSocket() ;
}

const QTcpSocket &RemoteObject::getFrameSocket() const
{
    return _frameChannel->getSocket() ;
}

quint32 RemoteObject::ip() const
{
    return ip_;
}

std::string RemoteObject::ipStr() const
{
    return ipStr_;
}

void RemoteObject::nodeDisconnected_SLOT()
{
    LOG_DEBUG("Disconnected: %s", ipStr_.c_str( ));
    Q_EMIT this->nodeDisconnected_SIGNAL( ip_ );
}

void RemoteObject::incomingFrame_SLOT()
{
    LOG_ERROR("not to invoke, unless overriden");
}

void RemoteObject::frameReceived_SLOT( Instruction )
{
    LOG_ERROR("not to invoke, unless overriden");
}


void RemoteObject::setFrameSocket_( QTcpSocket *socket )
{
    _frameChannel.reset( new TcpChannel( socket ));


    connect( _frameChannel.get() ,
             SIGNAL(instructionReceived_SIGNAL( Instruction )),
             this ,
             SLOT(frameReceived_SLOT( Instruction ))) ;

    connect( _frameChannel.get() , SIGNAL(disconnected_SIGNAL()) ,
             this , SLOT(nodeDisconnected_SLOT( )));
}

void RemoteObject::assertThread_()
{
    if( QThread::currentThread() != this->thread( ))
        LOG_ERROR("Current thread != master thread");
}

}
}
