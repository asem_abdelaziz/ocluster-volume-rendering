#ifndef CLRENDERERNODEINFO_H
#define CLRENDERERNODEINFO_H

#include "CLRendererDeviceInfo.h"
#include "Serializable.h"
#include <QMap>
#include <QMetaType>

struct CLRendererNodeInfo : public Serializable
{
    /**
     * @brief CLRendererNodeInfo
     */
    CLRendererNodeInfo();

    /**
     * @brief CLRendererNodeInfo
     * @param other
     */
    CLRendererNodeInfo( const CLRendererNodeInfo &other );

    /**
     * @brief addRendererDeviceInfo
     * @param localGPUIndex
     * @param vendor
     * @param name
     * @param globalMemory
     */
    void addRendererDeviceInfo( const quint32 localGPUIndex ,
                                const QString vendor ,
                                const QString name ,
                                const quint64 globalMemory );

    /**
     * @brief getRendererDevicesInfo
     * @return
     */
    const QMap< quint32 , CLRendererDeviceInfo >
    &getRendererDevicesInfo() const ;

    /**
     * @brief getName
     * @return
     */
    const QString getName( ) const ;

    /**
     * @brief setName
     * @param name
     */
    void setName( const QString name ) ;

    /**
     * @brief setVolumeLoaded
     * @param volumeLoaded
     * @param gpuIndex
     */
    void setVolumeLoaded( bool volumeLoaded , const quint32 gpuIndex );


    bool volumeLoaded( const quint32 gpuIndex ) const;

    SharedPointerType toSharedPointer() const override;
protected:
    Serializable::StreamType &_serialize( StreamType &stream ) const override;
    Serializable::StreamType &_deserialize( StreamType &stream ) override;
    std::string _getSerializableName() const override;

private:

    /**
     * @brief name_
     */
    QString username_ ;

    /**
     * @brief rendererDevices_
     */
    QMap< quint32 , CLRendererDeviceInfo > rendererDevices_ ;
};




#endif // CLRENDERERNODEINFO_H
