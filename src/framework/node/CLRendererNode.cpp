#include "CLRendererNode.h"

namespace ocluster
{
namespace Node
{

template< class V , class F >
CLRendererNode< V , F >::CLRendererNode(
        const quintptr socketDescriptor,
        const SerializableTransformation &transformations,
        const clparen::CLData::FRAME_CHANNEL_ORDER frameChannelOrder,
        QObject *parent )
    : RendererNode( socketDescriptor , transformations ,
                    frameChannelOrder ,parent )
{

}

template< class V , class F >
const QMap< quint32, clparen::CLData::CLImage2D< F >*>
&CLRendererNode< V , F >::getFrames() const
{
    return renderedFrames_ ;
}

template< class V , class F >
void CLRendererNode< V , F >::loadVolumeToDevice(
        std::shared_ptr< SerializableVolume< V >> serializableVolume ,
        const quint32 gpuIndex )
{
    assertThread_();

    if( gpuIndex >= nodeInfo_.getRendererDevicesInfo().size( ))
    {
        LOG_WARNING("GPU Index out of range!");
        return ;
    }


    deviceMonitor_[ gpuIndex ] = CLDeviceMonitor() ;
    deviceMonitor_[ gpuIndex ].volumeSize = serializableVolume->dataSize() ;

    serializableVolume->setTargetGPUIndex( gpuIndex );
    framesCenters_[ gpuIndex ] = serializableVolume->getUnitCubeCenter();


    //    LOG_DEBUG("Load Volume (checksum:%ld) to GPU< %d >",
    //              serializableVolume.checksum() , gpuIndex );


    // If volume to send is less than max segment size, send as one piece.
    // Otherwise, segment to small pieces.
    if( serializableVolume->dataSize() <= serializableVolume->maxSegmentSize( ))
    {
        _controlChannel->sendInstruction( Instruction::Message::LoadVolume ,
                                          Instruction::MessageDirection::Request ,
                                          Instruction::DataType::Volume ,
                                          serializableVolume );
        deviceMonitor_[ gpuIndex ].segmentsCount = 1 ;
    }

    else
    {
        auto segments = serializableVolume->makeSegments();
        for( auto segment : segments )
            _controlChannel->sendInstruction( Instruction::Message::LoadVolume ,
                                              Instruction::MessageDirection::Request ,
                                              Instruction::DataType::Volume ,
                                              segment.second );
        deviceMonitor_[ gpuIndex ].segmentsCount = segments.size();
    }
    Q_EMIT loadVolumeProgress_SIGNAL( ip_ , gpuIndex , &deviceMonitor_[ gpuIndex ]);
}

template< class V , class F >
void CLRendererNode< V , F >::frameReceived_SLOT(
        Instruction instruction )
{

    assertThread_();

    auto frame = dynamic_cast< SerializableFrame< F > const * >( instruction.getDataPtr() );

    if( frame == nullptr )
    {
        LOG_WARNING("Failed to receive frame!");
        return;
    }

    auto gpuIndex = frame->getGPUIndex();
    frame->shareData( *renderedFrames_[ gpuIndex ] );

    Q_EMIT this->frameReady_SIGNAL( ip_ , gpuIndex );
}


template< class V , class F >
bool CLRendererNode< V , F >::deployGPU( const quint32 gpuIndex )
{

    assertThread_();

    if( gpuIndex >= nodeInfo_.getRendererDevicesInfo().size( ))
    {
        LOG_WARNING("GPU Index out of range!");
        return false ;
    }
    _controlChannel->sendInstruction( Instruction::Message::DeployGPU ,
                                      Instruction::MessageDirection::Request ,
                                      Instruction::DataType::UInterger32 ,
                                      SerializableNumber< quint32 >( gpuIndex ));

    return true ;
}

template< class V , class F >
void CLRendererNode< V , F >::deployAllGPUs()
{
    for( const CLRendererDeviceInfo &deviceInfo :
         nodeInfo_.getRendererDevicesInfo( ))
        deployGPU( deviceInfo.localGPUIndex );
}

template< class V , class F >
void CLRendererNode< V , F >::instructionTransmitted_SLOT(
        Instruction instruction )
{
    if( instruction.getMessage() == Instruction::Message::LoadVolume )
    {
        try{
            const SerializableVolume< V > &volume =
                    dynamic_cast< const SerializableVolume< V > &>( instruction.getData());
            quint32 gpuIndex = volume.getTargetGPUIndex();

            LOG_DEBUG("Serialized:%d/%d",volume.getSerializedSegmentsCount() ,
                      volume.getSegmentsCount());

            deviceMonitor_[ gpuIndex ].segmentsSent+=1 ;
            Q_EMIT loadVolumeProgress_SIGNAL( ip_ , gpuIndex ,
                                              &deviceMonitor_[ gpuIndex ]);
        } catch( const std::bad_cast &e ) {
            LOG_ERROR("Bad cast.");
        }


    }
}

template< class V , class F >
void CLRendererNode< V , F >::requestNodeInfo()
{
    assertThread_();
    LOG_DEBUG("Request Node Info:%s", ipStr_.c_str());
    _controlChannel->sendInstruction(  Instruction::Message::NodeInfo ,
                                       Instruction::MessageDirection::Request );

}



template< class V , class F >
void CLRendererNode< V , F >::renderNewFrame( )
{
    _controlChannel->sendInstruction( Instruction::Message::Render ,
                                      Instruction::MessageDirection::Request ,
                                      Instruction::DataType::Transformation ,
                                      &globalTransformations_ );

}


template< class V , class F >
const CLRendererNodeInfo & CLRendererNode< V , F >::getNodeInfo() const
{
    return nodeInfo_ ;
}


template< class V , class F >
const QMap<quint32, Coordinates3D> &
CLRendererNode< V , F >::getFramesCenters() const
{
    return framesCenters_ ;
}

template< class V , class F >
bool CLRendererNode< V , F >::allDevicesDeployed() const
{
    return deployedGPUs_.size() == nodeInfo_.getRendererDevicesInfo().size();
}

template< class V , class F >
bool CLRendererNode< V , F >::nodeReady() const
{
    return nodeReady_ ;
}


template< class V , class F >
void CLRendererNode< V , F >::instructionReceived_SLOT( Instruction instruction )
{
    try{
        switch( instruction.getMessage())
        {
            case Instruction::Message::NodeInfo :
            {
                nodeInfo_ = dynamic_cast< const CLRendererNodeInfo& >( instruction.getData());
                Q_EMIT this->nodeInfoReady_SIGNAL( ip_ );
                LOG_DEBUG("Node info received from [%s:%s]." ,
                          nodeInfo_.getName().toStdString().c_str() ,
                          ipStr_.c_str());

            } break;

            case Instruction::Message::NodeReady :
            {
                nodeReady_ = true ;
                Q_EMIT this->nodeReady_SIGNAL( ip_ );
            } break;

            case Instruction::Message::DeployGPU :
            {
                auto gpuIndex = dynamic_cast< const SerializableNumber< quint32 >& >(
                                    instruction.getData());

                LOG_DEBUG("GPU [%d] deployed at [%s]" , gpuIndex.getNumber() ,
                          ipStr_.c_str( ));
                deployedGPUs_ << gpuIndex.getNumber() ;
                renderedFrames_[ gpuIndex.getNumber() ] =
                        new clparen::CLData::CLImage2D< F >( frameDimensions_ ,
                                                             frameChannelOrder_ );
                Q_EMIT this->deviceDeployed_SIGNAL( ip_ , gpuIndex.getNumber() );
            } break ;

            case Instruction::Message::Error :
            {
                LOG_DEBUG("Error message coming from [%s]." , ipStr_.c_str());
            } break;
        }
    } catch( const std::bad_cast &e ) {
        LOG_ERROR("Bad cast!");
    }


}



template< class V , class F >
QSet< quint32 > CLRendererNode< V , F >::getDeployedGPUs() const
{
    return deployedGPUs_;
}

template< class V , class F >
void CLRendererNode< V , F >::switchRenderingKernel( clparen::CLKernel::RenderingMode mode )
{
    _controlChannel->sendInstruction(
                Instruction::Message::SwitchRenderingKernel ,
                Instruction::MessageDirection::Request ,
                Instruction::DataType::UInterger32 ,
                SerializableNumber< quint32 >( static_cast< quint32 >( mode )));

}


}
}



#include "CLRendererNode.ipp"
