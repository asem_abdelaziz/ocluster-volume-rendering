#ifndef CLRENDERERDEVICEINFO_H
#define CLRENDERERDEVICEINFO_H

#include <QString>

#include "Serializable.h"

// forward declaration.
class CLRendererNodeInfo;

struct CLRendererDeviceInfo : public Serializable
{
    CLRendererDeviceInfo();
    CLRendererDeviceInfo( const CLRendererDeviceInfo &other );
    quint32 localGPUIndex;
    QString vendor;
    QString name;
    quint64 globalMemory;
    mutable bool deployed;
    mutable bool volumeLoaded;

    SharedPointerType toSharedPointer() const;
protected:
    friend class CLRendererNodeInfo;
    StreamType &_serialize( StreamType &stream ) const override;
    StreamType &_deserialize( StreamType &stream ) override;
    std::string _getSerializableName() const override;

private:

};



#endif // CLRENDERERDEVICEINFO_H
