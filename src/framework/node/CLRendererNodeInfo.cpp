#include "CLRendererNodeInfo.h"


CLRendererNodeInfo::CLRendererNodeInfo()
{

}

CLRendererNodeInfo::CLRendererNodeInfo( const CLRendererNodeInfo &other )
    : username_( other.username_ ),
      rendererDevices_( other.rendererDevices_ )
{

}

void CLRendererNodeInfo::addRendererDeviceInfo( const quint32 localGPUIndex,
                                                const QString vendor,
                                                const QString name,
                                                const quint64 globalMemory )
{
    CLRendererDeviceInfo deviceInfo ;
    deviceInfo.localGPUIndex = localGPUIndex ;
    deviceInfo.vendor = vendor ;
    deviceInfo.name = name ;
    deviceInfo.globalMemory = globalMemory ;

    rendererDevices_[ localGPUIndex ] =  deviceInfo ;

}

const QMap< quint32 , CLRendererDeviceInfo >
&CLRendererNodeInfo::getRendererDevicesInfo() const
{
    return rendererDevices_ ;
}

const QString CLRendererNodeInfo::getName() const
{
    return username_ ;
}

void CLRendererNodeInfo::setName( const QString name )
{
    username_ = name ;
}

void CLRendererNodeInfo::setVolumeLoaded( bool volumeLoaded,
                                          const quint32 gpuIndex)
{
    rendererDevices_[ gpuIndex ].volumeLoaded = volumeLoaded ;
}

bool CLRendererNodeInfo::volumeLoaded(const quint32 gpuIndex) const
{
    return rendererDevices_[ gpuIndex ].volumeLoaded ;
}

Serializable::SharedPointerType CLRendererNodeInfo::toSharedPointer() const
{
    return std::make_shared< CLRendererNodeInfo >( *this );
}

Serializable::StreamType & CLRendererNodeInfo::_serialize( StreamType &stream ) const
{
    stream << username_ ;
    stream << static_cast<quint32>( rendererDevices_.size( ));
    for( const CLRendererDeviceInfo &deviceInfo : rendererDevices_ )
        deviceInfo._serialize( stream );
    return stream ;
}

Serializable::StreamType & CLRendererNodeInfo::_deserialize( StreamType &stream )
{
    quint32 listSize = 0 ;
    stream >> username_ ;
    stream >> listSize ;
    for( quint32 i = 0 ; i < listSize ; i++ )
    {
        CLRendererDeviceInfo info;
        info._deserialize( stream );
        rendererDevices_[ info.localGPUIndex ] = info ;
    }
    return stream;
}

std::string CLRendererNodeInfo::_getSerializableName() const
{
    return "CLRendererNodeInfo";
}

