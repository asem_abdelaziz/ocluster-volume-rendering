#include "CLDeviceMonitor.h"

bool CLDeviceMonitor::metaTypeRegistered = false ;

CLDeviceMonitor::CLDeviceMonitor()
    : volumeSize( 0 ) ,
      segmentsCount( 0 ) ,
      segmentsSent( 0 ) ,
      renderingAverageTime( -1 )
{
    if( !metaTypeRegistered )
    {
        qRegisterMetaType<CLDeviceMonitor>("CLDeviceMonitor");
        metaTypeRegistered = true ;
    }
}
