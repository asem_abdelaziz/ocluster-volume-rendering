#include "RendererNode.h"
#include "Logger.h"


namespace ocluster
{
namespace Node
{

RendererNode::RendererNode(
        const quintptr socketDescriptor ,
        const SerializableTransformation &transformations,
        const clparen::CLData::FRAME_CHANNEL_ORDER frameChannelOrder ,
        QObject *parent )
    : globalTransformations_( transformations ) ,
      frameDimensions_( FRAME_WIDTH , FRAME_HEIGHT ) ,
      frameChannelOrder_( frameChannelOrder ),
      nodeReady_( false ),
      RemoteObject( socketDescriptor , parent )
{

}



}
}
