#ifndef RENDERERNODE_H
#define RENDERERNODE_H


#include <QHostAddress>
#include <QThreadPool>
#include <QQueue>

#include "clparen/clparen.h"

#include "RemoteObject.h"
#include "CLDeviceMonitor.h"




#define FRAME_WIDTH 512
#define FRAME_HEIGHT 512




namespace ocluster
{

// Forward Declaration (friend class)
class CLusterServer;


namespace Node
{



class RendererNode : public RemoteObject
{
    Q_OBJECT

    Q_DISABLE_COPY( RendererNode )


public:
    explicit RendererNode(
            const quintptr socketDescriptor ,
            const SerializableTransformation &transformations ,
            const clparen::CLData::FRAME_CHANNEL_ORDER frameChannelOrder ,
            QObject *parent = 0 );


    /**
     * @brief requestNodeInfo
     */
    virtual void requestNodeInfo( ) = 0;


    /**
     * @brief renderNewFrame
     */
    virtual void renderNewFrame( ) = 0;


    /**
     * @brief deployGPU
     * @param gpuIndex
     */
    virtual bool deployGPU( const quint32 gpuIndex ) = 0;


    /**
     * @brief deployAllGPUs
     */
    virtual void deployAllGPUs( ) = 0 ;


    /**
     * @brief getNodeInfo
     * @return
     */
    virtual const CLRendererNodeInfo &getNodeInfo() const = 0;



    /**
     * @brief getFramesCenters
     * @return
     */
    virtual const QMap< quint32 , Coordinates3D > &getFramesCenters() const = 0;


    /**
     * @brief allDevicesDeployed
     * @return
     */
    virtual bool allDevicesDeployed() const = 0;


    /**
     * @brief nodeReady
     * @return
     */
    virtual bool nodeReady() const = 0;


    /**
     * @brief getDeployedGPUs
     * @return
     */
    virtual QSet< quint32 > getDeployedGPUs() const = 0;


    /**
     * @brief switchRenderingKernel
     * @param mode
     */
    virtual void switchRenderingKernel( clparen::CLKernel::RenderingMode mode ) = 0;

Q_SIGNALS:

    /**
     * @brief nodeInfoReady_SIGNAL
     * @param ip
     */
    void nodeInfoReady_SIGNAL( quint32 ip );


    /**
     * @brief gpuDeployed_SIGNAL
     * @param ip
     * @param gpuIndex
     */
    void deviceDeployed_SIGNAL( quint32 ip , quint32 gpuIndex );


    /**
     * @brief volumeLoadedToDevice_SIGNAL
     * @param ip
     * @param gpuIndex
     */
    void volumeLoadedToDevice_SIGNAL( quint32 ip , quint32 gpuIndex );

    /**
     * @brief frameReady_SIGNAL
     * @param ip
     */
    void frameReady_SIGNAL( quint32 ip , quint32 gpuIndex );


    /**
     * @brief nodeReady_SIGNAL
     * @param ip
     */
    void nodeReady_SIGNAL( quint32 ip );

    /**
     * @brief loadVolumeProgress
     * @param ip
     * @param gpuIndex
     * @param monitor
     */
    void loadVolumeProgress_SIGNAL( quint32 ip , quint32 gpuIndex ,
                                    CLDeviceMonitor *monitor );



protected:
    friend class ocluster::CLusterServer;


protected:

    /**
     * @brief nodeInfo_
     */
    CLRendererNodeInfo nodeInfo_ ;


    /**
     * @brief frameDimensions_
     */
    const Dimensions2D frameDimensions_ ;



    /**
     * @brief globalTransformations_
     */
    const SerializableTransformation &globalTransformations_ ;


    /**
     * @brief deployedGPUs_
     */
    QSet< quint32 > deployedGPUs_ ;


    /**
     * @brief deviceMonitor_
     */
    QMap< quint32 , CLDeviceMonitor > deviceMonitor_ ;

    /**
     * @brief nodeReady_
     */
    bool nodeReady_ ;


    /**
     * @brief framesCenters_
     * key: GPU index.
     */
    QMap< quint32 , Coordinates3D > framesCenters_ ;

    /**
     * @brief frameChannelOrder
     */
    const clparen::CLData::FRAME_CHANNEL_ORDER frameChannelOrder_;

};

}
}
#endif // RENDERERNODE_H
