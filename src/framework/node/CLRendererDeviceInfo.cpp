#include "CLRendererDeviceInfo.h"


CLRendererDeviceInfo::CLRendererDeviceInfo()
{

    deployed = false ;
    volumeLoaded = false ;
}

CLRendererDeviceInfo::CLRendererDeviceInfo(
        const CLRendererDeviceInfo &other )
    : deployed( other.deployed ),
      volumeLoaded( other.volumeLoaded ),
      localGPUIndex( other.localGPUIndex ),
      vendor( other.vendor ),
      name( other.name ),
      globalMemory( other.globalMemory )
{

}

Serializable::SharedPointerType CLRendererDeviceInfo::toSharedPointer() const
{
    return std::make_shared< CLRendererDeviceInfo >( *this );
}

Serializable::StreamType
&CLRendererDeviceInfo::_serialize( StreamType &stream ) const
{
    stream  << localGPUIndex
            << vendor
            << name
            << globalMemory ;
    return stream ;
}

Serializable::StreamType
&CLRendererDeviceInfo::_deserialize( StreamType &stream )
{
    stream  >> localGPUIndex
            >> vendor
            >> name
            >> globalMemory ;
    return stream ;
}

std::string CLRendererDeviceInfo::_getSerializableName() const
{
    return "CLRendererDeviceInfo";
}
