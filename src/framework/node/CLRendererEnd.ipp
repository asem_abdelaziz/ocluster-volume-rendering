#ifndef CLRENDEREREND_IPP
#define CLRENDEREREND_IPP

namespace ocluster
{
namespace Node
{

//template class CLRendererEnd< uint8_t , uint8_t >;
//template class CLRendererEnd< uint8_t , uint16_t >;
//template class CLRendererEnd< uint8_t , uint32_t >;
//template class CLRendererEnd< uint8_t , half  >;
template class CLRendererEnd< uchar , float > ;


}
}

#endif // CLRENDEREREND_IPP
