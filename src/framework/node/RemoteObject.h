#ifndef REMOTEOBJECT_H
#define REMOTEOBJECT_H

// std
#include <memory>

// Qt
#include <QObject>
#include <QTcpSocket>
#include <QThread>
#include <QHostAddress>

// local
#include "Logger.h"
#include "MetaSerialization.hh"
#include "TcpChannel.h"

namespace ocluster
{
namespace Node
{


class RemoteObject : public QObject
{
    Q_OBJECT

    Q_DISABLE_COPY( RemoteObject )

public:
    explicit RemoteObject( const quintptr socketDescriptor ,
                           QObject *parent = 0);


    explicit RemoteObject( const QHostAddress serverAddress ,
                           const int serverPort ,
                           QObject *parent = 0 );

    /**
     * @brief socket
     * @return
     */
    const QTcpSocket &getControlSocket( ) const;

    /**
     * @brief getFrameSocket
     * @return
     */
    const QTcpSocket &getFrameSocket( ) const;


    /**
     * @brief ip
     * @return
     */
    quint32 ip() const ;


    /**
     * @brief ipStr
     * @return
     */
    std::string ipStr() const;

Q_SIGNALS:
    /**
     * @brief nodeDisconnected_SIGNAL
     * @param ip
     */
    void nodeDisconnected_SIGNAL( quint32 ip );


public Q_SLOTS:

    /**
     * @brief nodeDisconnected_SLOT
     */
    virtual void nodeDisconnected_SLOT( );


    /**
     * @brief instructionSent_SLOT
     */
    virtual void instructionTransmitted_SLOT( Instruction ) = 0;


    /**
     * @brief instructionReceived_SLOT
     */
    virtual void instructionReceived_SLOT( Instruction ) = 0;

    /**
     * @brief incomingFrame_SLOT
     */
    virtual void incomingFrame_SLOT( );

    /**
     * @brief frameReceived_SLOT
     * @param frame
     */
    virtual void frameReceived_SLOT( Instruction );

protected:

    /**
     * @brief setFrameSocket_
     * @param frameSocket
     */
    virtual void setFrameSocket_( QTcpSocket *socket );

    /**
     * @brief assertMasterThread_
     */
    void assertThread_();

protected:

    /**
     * @brief _controlChannel
     */
    std::unique_ptr< TcpChannel > _controlChannel;

    /**
     * @brief _frameChannel
     */
    std::unique_ptr< TcpChannel > _frameChannel;

    /**
     * @brief transmitterThreadPool_
     */
    QThreadPool *networkThreadPool_ ;

    /**
     * @brief ip_
     */
    quint32 ip_ ;

    /**
     * @brief ipStr_
     */
    std::string ipStr_ ;

};


}
}
#endif // REMOTEOBJECT_H
