#ifndef CLRENDERERNODE_H
#define CLRENDERERNODE_H

#include "RendererNode.h"

namespace ocluster
{
namespace Node
{


template< class V , class F >
class CLRendererNode : public RendererNode
{

public:
    CLRendererNode( const quintptr socketDescriptor ,
                    const SerializableTransformation &transformations ,
                    const clparen::CLData::FRAME_CHANNEL_ORDER frameChannelOrder ,
                    QObject *parent = 0 );


    /**
     * @brief getFrames
     * @return
     */
    const QMap< quint32 , clparen::CLData::CLImage2D< F >* > &getFrames() const ;


    /**
     * @brief loadVolumeToGPU
     * @param volume
     * @param gpuIndex
     */
    void loadVolumeToDevice(
            std::shared_ptr< SerializableVolume< V >> serializableVol ,
            const quint32 gpuIndex );


    void frameReceived_SLOT( Instruction ) Q_DECL_OVERRIDE;

    void instructionTransmitted_SLOT( Instruction ) Q_DECL_OVERRIDE;

    void instructionReceived_SLOT( Instruction ) Q_DECL_OVERRIDE;



    bool deployGPU( const quint32 gpuIndex ) Q_DECL_OVERRIDE;

    void deployAllGPUs( ) Q_DECL_OVERRIDE;

    void requestNodeInfo( ) Q_DECL_OVERRIDE;

    void renderNewFrame( ) Q_DECL_OVERRIDE;

    const CLRendererNodeInfo &getNodeInfo() const Q_DECL_OVERRIDE;

    const QMap< quint32 , Coordinates3D > &getFramesCenters() const Q_DECL_OVERRIDE;

    bool allDevicesDeployed() const Q_DECL_OVERRIDE;

    bool nodeReady() const Q_DECL_OVERRIDE;

    QSet< quint32 > getDeployedGPUs() const Q_DECL_OVERRIDE;

    void switchRenderingKernel( clparen::CLKernel::RenderingMode mode ) Q_DECL_OVERRIDE;

private:
    /**
     * @brief renderedFrames_
     * key: GPU index.
     */
    QMap< quint32 , clparen::CLData::CLImage2D< F >* > renderedFrames_ ;

};



}
}


#endif // CLRENDERERNODE_H
