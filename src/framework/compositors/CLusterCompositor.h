#ifndef CLUSTERCOMPOSITOR_H
#define CLUSTERCOMPOSITOR_H

#include <QObject>
#include <QMutex>
#include <QMutexLocker>

#include "CLRendererNode.h"
#include "clparen/clparen.h"


namespace ocluster
{

class CLusterCompositor : public clparen::Compositor::CLAbstractCompositor
{    
    Q_OBJECT

    Q_DISABLE_COPY( CLusterCompositor )

public:
    /**
     * @brief CLusterCompositor
     * @param gpuIndex
     * @param frameWidth
     * @param frameHeight
     */
    CLusterCompositor( const quint32 gpuIndex = 0 ,
                       const uint frameWidth = FRAME_WIDTH ,
                       const uint frameHeight = FRAME_HEIGHT ,
                       const std::string kernelDirectory = DEFAULT_KERNELS_DIRECTORY ,
                       QObject *parent = 0 );


    ~CLusterCompositor( );

    /**
     * @brief allocateFrames
     * @param node
     */
    virtual void allocateFrames( Node::RendererNode *node ) final ;


    /**
     * @brief collectFrame
     * @param sourceFrame
     * @param block
     */
    virtual
    void uploadFrame( const clparen::CLData::CLImage2D< float > *sourceFrame ,
                       const cl_bool block ) final ;

    /**
     * @brief composite
     */
    virtual void composite( ) final;

    /**
     * @brief loadFinalFrame
     */
    virtual void loadFinalFrame( )  final;

    /**
     * @brief getFinalFrame
     * @return
     */
    virtual const clparen::CLData::CLFrameVariant &getFinalFrame( ) const final ;



    /**
     * @brief framesCount
     * @return
     */
    uint framesCount( ) const ;

    /**
     * @brief getCompositedFramesCount
     * @return
     */
    uint getFramesInCompositorCount( ) const;


    /**
     * @brief getGPUIndex
     * @return
     */
    quint32 getGPUIndex( ) const;

    /**
     * @brief readOutReady
     * @return
     */
    bool readOutReady( ) const ;


    bool isRenderingModeSupported(
            clparen::CLKernel::RenderingMode mode ) Q_DECL_OVERRIDE;
Q_SIGNALS:

    /**
     * @brief compositingReady_SIGNAL
     */
    void compositingReady_SIGNAL( );

    /**
     * @brief compositingFinished_SIGNAL
     */
    void compositingFinished_SIGNAL( );



protected :

    /**
     * @brief initializeBuffers_
     */
    void initializeBuffers_( ) ;

    /**
     * @brief initializeKernel_
     */
    void initializeKernel_( ) ;

    /**
     * @brief updateKernelsArguments_
     */
    void updateKernelsArguments_();
protected:

    /**
     * @brief finalFrame_
     */
    clparen::CLData::CLImage2D< float > *finalFrame_ ;

    /**
     * @brief finalFrameReadout_
     */
    clparen::CLData::CLImage2D< float > *finalFrameReadout_ ;

    //empty

    /**
     * @brief imagesArray_
     */
    clparen::CLData::CLImage2DArray< float > *imagesArray_ ;

    /**
     * @brief depthIndex_
     */
    clparen::CLData::CLBuffer< uint > *depthIndex_ ;

    /**
     * @brief framesCount_
     */
    uint framesCount_ ;

    /**
     * @brief framesInCompositor_
     */
    uint framesInCompositor_ ;

    /**
     * @brief framesInCompositorLock_
     * Serialize incrementing of framesInCompositor.
     */
    QMutex framesInCompositorLock_;

    /**
     * @brief framesInices_
     */
    QMap< const clparen::CLData::CLImage2D< float >* , uint > framesIndices_ ;

    ////////// TO DELETE ///////////////////////////////////////////////////////////////////////
    virtual void allocateFrame( clparen::Renderer::CLAbstractRenderer* ) final {}           //////
    virtual void collectFrame( clparen::Renderer::CLAbstractRenderer*,const cl_bool) final{}//////
    ////////////////////////////////////////////////////////////////////////////////////////////

};

}

#endif // CLUSTERCOMPOSITOR_H
