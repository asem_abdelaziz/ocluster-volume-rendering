#include "CLusterCompositor.h"
#include "Logger.h"

#define LOCAL_SIZE_X 16
#define LOCAL_SIZE_Y 16

namespace ocluster
{

CLusterCompositor::CLusterCompositor( const quint32 gpuIndex ,
                                      const uint frameWidth  ,
                                      const uint frameHeight  ,
                                      const std::string kernelDirectory  ,
                                      QObject *parent )
    :
      CLAbstractCompositor( gpuIndex , frameWidth , frameHeight ,
                            kernelDirectory , parent   )
{

    framesCount_ = 0 ;
    framesInCompositor_ = 0 ;
    depthIndex_ = nullptr;

    initializeBuffers_( );
    initializeKernel_( );

}

CLusterCompositor::~CLusterCompositor( )
{

}

void CLusterCompositor::allocateFrames( Node::RendererNode *node )
{

    Node::CLRendererNode< uchar , float > *clNode =
            dynamic_cast< Node::CLRendererNode< uchar , float > *>( node );

    const QMap< quint32 , clparen::CLData::CLImage2D< float >* > &frames =
            clNode->getFrames();

    for( const quint32 gpuIndex  : frames.keys( ))
    {
        if( framesIndices_.keys().contains( frames[ gpuIndex ] ))
            continue;

        framesIndices_[ frames[ gpuIndex ]] = framesCount_++ ;
    }



    imagesArray_->resize( framesCount_ ,
                          context_ );

    depthIndex_->resize( framesCount_ );


    updateKernelsArguments_();

}

void CLusterCompositor::uploadFrame(
        const clparen::CLData::CLImage2D< float > *sourceFrame ,
        const cl_bool block )
{

    if( !framesIndices_.keys().contains( sourceFrame ))
        LOG_ERROR("Frame< %p > is not registered!" , sourceFrame );

    imagesArray_->setFrameData( framesIndices_[ sourceFrame ] ,
                                sourceFrame->getHostData( ));

    imagesArray_->loadFrameDataToDevice( framesIndices_[ sourceFrame ] ,
                                         commandQueue_ ,
                                         block );

    QMutexLocker lock( &framesInCompositorLock_ );
    if( ++framesInCompositor_ == framesCount_ )
        Q_EMIT this->compositingReady_SIGNAL( );
}

void CLusterCompositor::composite()
{
    QMutexLocker lock( &framesInCompositorLock_ );
    if( framesInCompositor_ != framesCount_)
        LOG_ERROR("Not all frames loaded to device yet!") ;


    LOG_DEBUG("Compositing..");
    //    qStableSort( renderers_.begin() , renderers_.end() ,
    //                 CLAbstractRenderer::lessThan );

    QVector< uint > depthIndex ;

    //    for( const CLImage2D< float > *frame : framesIndices_.keys( ))
    //        depthIndex << framesIndices_[ frame ];

    for( uint i = 0 ; i < depthIndex_->size() ; i++ )
        depthIndex << i ;

    depthIndex_->setHostData( depthIndex );
    depthIndex_->writeDeviceData( commandQueue_ , CL_TRUE );

    const size_t localSize[ ] = { LOCAL_SIZE_X , LOCAL_SIZE_Y  } ;
    const size_t globalSize[ ] = { frameDimensions_.x ,
                                   frameDimensions_.y };

    cl_int clErrorCode =
            clEnqueueNDRangeKernel( commandQueue_ ,
                                    activeCompositingKernel_->getKernelObject() ,
                                    2 ,
                                    NULL ,
                                    globalSize ,
                                    localSize ,
                                    0 ,
                                    NULL ,
                                    NULL ) ;


    if( clErrorCode != CL_SUCCESS )
    {
        oclHWDL::Error::checkCLError( clErrorCode );
        LOG_ERROR("OpenCL Error!");
    }

    clFinish( commandQueue_ );


    framesInCompositor_ = 0 ;
    readOutReady_ = true ;

    Q_EMIT this->compositingFinished_SIGNAL();
}

void CLusterCompositor::loadFinalFrame()
{
    finalFrameReadout_->readOtherDeviceData( commandQueue_ ,
                                             *finalFrame_ ,
                                             CL_TRUE );
    readOutReady_ = false ;
}

const clparen::CLData::CLFrameVariant &CLusterCompositor::getFinalFrame() const
{
    this->finalFrameVariant_.
            setValue(( clparen::CLData::CLImage2D< float > *) finalFrameReadout_ );
    return this->finalFrameVariant_ ;
}

uint CLusterCompositor::framesCount() const
{
    return framesCount_ ;
}

uint CLusterCompositor::getFramesInCompositorCount() const
{
    return framesInCompositor_ ;
}

quint32 CLusterCompositor::getGPUIndex() const
{
    return gpuIndex_ ;
}

bool CLusterCompositor::readOutReady() const
{
    return readOutReady_ ;
}

bool CLusterCompositor::isRenderingModeSupported(
        clparen::CLKernel::RenderingMode mode )
{
    if( finalFrame_ == 0 )
        return false ;

    return compositingKernels_[ mode ]->getChannelOrderSupport()
            == finalFrame_->channelOrder() ;
}

void CLusterCompositor::initializeBuffers_()
{
    LOG_DEBUG("Initializing Buffers ...");


    finalFrame_ = new clparen::CLData::CLImage2D< float >(
                frameDimensions_ ,
                clparen::CLData::FRAME_CHANNEL_ORDER::ORDER_INTENSITY );

    finalFrameReadout_ =
            new clparen::CLData::CLImage2D< float >(
                frameDimensions_ ,
                clparen::CLData::FRAME_CHANNEL_ORDER::ORDER_INTENSITY );

    finalFrame_->createDeviceData( context_ );

    imagesArray_ =
            new clparen::CLData::CLImage2DArray< float >(
                frameDimensions_.x ,
                frameDimensions_.y ,
                0 ,
                clparen::CLData::FRAME_CHANNEL_ORDER::ORDER_INTENSITY );

    depthIndex_ = new clparen::CLData::CLBuffer< uint >( 1 );

    depthIndex_->createDeviceData( context_ );

    LOG_DEBUG("[DONE] Initializing Buffers ...");
}

void CLusterCompositor::initializeKernel_()
{
    LOG_DEBUG( "Initializing an OpenCL Kernel ... " );

    activeCompositingKernel_ =
            compositingKernels_[
            clparen::CLKernel::RenderingMode::RENDERING_MODE_Xray ];

    for( clparen::CLKernel::CLCompositingKernel* compositingKernel :
         compositingKernels_.values( ))
    {
        // Assuming that every thing is going in the right direction.
        cl_int clErrorCode = CL_SUCCESS;

        compositingKernel->setFinalFrame( finalFrame_->getDeviceData( ));

        oclHWDL::Error::checkCLError( clErrorCode );
    }

    LOG_DEBUG( "[DONE] Initializing an OpenCL Kernel ... " );
}


void CLusterCompositor::updateKernelsArguments_()
{
    for( clparen::CLKernel::CLCompositingKernel* compositingKernel :
         compositingKernels_.values( ))
    {
        // Assuming that every thing is going in the right direction.
        cl_int clErrorCode = CL_SUCCESS;

        compositingKernel->setDepthIndex( depthIndex_->getDeviceData( ));

        oclHWDL::Error::checkCLError( clErrorCode );

        if( imagesArray_->inDevice())
            compositingKernel->setFrame( imagesArray_->getDeviceData( ));
    }
}


}
