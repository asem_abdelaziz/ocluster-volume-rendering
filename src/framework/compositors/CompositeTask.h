#ifndef COMPOSITETASK_H
#define COMPOSITETASK_H

#include <QRunnable>
#include <CLusterCompositor.h>

namespace ocluster
{

class CompositeTask : public QRunnable
{
    Q_DISABLE_COPY( CompositeTask )

public:
    CompositeTask( CLusterCompositor &clusterCompositor );

protected:
    void run() override;

private:
    CLusterCompositor &clusterCompositor_ ;
};

}
#endif // COMPOSITETASK_H
