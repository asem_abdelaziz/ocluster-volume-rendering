#include "CompositeTask.h"

namespace ocluster
{

CompositeTask::CompositeTask( CLusterCompositor &clusterCompositor )
    : clusterCompositor_( clusterCompositor )
{
    setAutoDelete( false );
}

void CompositeTask::run()
{
    clusterCompositor_.composite();
    clusterCompositor_.loadFinalFrame();
}

}
