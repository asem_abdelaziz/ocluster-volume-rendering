#include "CLusterServer.h"
#include "Logger.h"

namespace ocluster
{

CLusterServer::CLusterServer(
        QMap<quint32, Node::RendererNode *> &clRendererNodes,
        Parallel::CLusterRenderer &clusterRenderer ,
        QObject *parent )
    : clRendererNodes_( clRendererNodes ),
      clusterRenderer_( clusterRenderer ) ,
      QTcpServer( parent )
{
    status_ = 0 ;

}

void CLusterServer::initializeServer()
{

    if( isServerInitialized( ))
    {
        LOG_WARNING("Server is already initialized!");
        return;
    }
    LOG_DEBUG("Initializing the server: Listening to [%s] at port [%d].",
              QHostAddress( QHostAddress::Any ).
              toString().toStdString().c_str() ,
              CONTROL_PORT );

    if ( this->listen( QHostAddress::Any ,  CONTROL_PORT ) &&
         framesServer_.listen( QHostAddress::Any , FRAMES_PORT ))

        status_ |= Status::ServerInitialized ;


    connect( &framesServer_ , SIGNAL( newConnection( )) ,
             this , SLOT( newFrameConnection_SLOT( )));




    networkManagerSSH_ = new NetworkManagerSSH( DEFAULT_USERNAME ,
                                                DEFAULT_PASSWORD ,
                                                DEFAULT_SSH_PORT ,
                                                DEFAULT_MAX_THREADS_COUNT );

    connect( networkManagerSSH_ , SIGNAL(remoteNodesVerified_SIGNAL( )) ,
             this , SLOT(remoteNodesVerifiedSSH_SLOT( )));

    connect( networkManagerSSH_ , SIGNAL(remoteNodesFetched_SIGNAL()) ,
             this , SLOT(remoteNodesFetchedSSH_SLOT( )));

}

void CLusterServer::scanInterface( const QString interfaceLabel )
{
    if( !isServerInitialized( ))
    {
        LOG_WARNING("Server is not initialized!");
        return;
    }
    networkManagerSSH_->scanInterface( interfaceLabel );
}

void CLusterServer::scanRange( const quint32 ipFirst,
                               const quint32 ipLast)
{
    networkManagerSSH_->scanRange( ipFirst , ipLast );
}

void CLusterServer::blindScanRenderingMachines()
{
    if( !isServerInitialized( ))
    {
        LOG_WARNING("Server is not initialized!");
        return;
    }

    // Scan network for any machines supporting
    // SSH protocol and having user:vizuser.
    networkManagerSSH_->surveyRemoteNodes( );
}

void CLusterServer::fetchRenderingMachines( const QList< quint32 > ipAddress ,
                                            const QString username  ,
                                            const QString password  )
{
    if( !isServerInitialized( ))
    {
        LOG_WARNING("Server is not initialized!");
        return;
    }

    // Initialization.2: Initiate SSH connection with all verified
    // SSH supporting nodes.
    const std::string usernameStd = username.toStdString();
    const std::string passwordStd = password.toStdString();
    networkManagerSSH_->fetchRemoteNodes( ipAddress , usernameStd , passwordStd );
}

void CLusterServer::connectOnlineMachinesToServer(
        const std::string applicationName ,
        const QList< quint32 > ipAddress )
{
    if( !isServerInitialized( ))
    {
        LOG_WARNING("Server is not initialized!");
        return;
    }

    for( const quint32 ip :
         ( ipAddress.isEmpty( ))? onlineMachines_.toList() : ipAddress )
    {
        const QHostAddress serverInterface =
                NetworkManagerSSH::getCorrespondingInterface( QHostAddress( ip ));


        std::string command = applicationName  +
                + " " + serverInterface.toString().toStdString()
                + " " + QString::number( CONTROL_PORT ).toStdString();

        if( networkManagerSSH_->executeRemoteCommand( ip , command ) == false )

            LOG_WARNING("Failed to execute command on ip [%s].",
                        QHostAddress( ip ).toString().toStdString().c_str( ));
    }

}



void CLusterServer::dropRendererNode( Node::RendererNode *rendererNode )
{
    quint32 ip = rendererNode->ip();

    dropRendererNode( ip );
}

void CLusterServer::dropRendererNode( quint32 ip )
{
    LOG_DEBUG("Dropping node: %s",
              QHostAddress( ip ).toString().toStdString().c_str( ));

    clRendererNodes_.remove( ip );


}

void CLusterServer::initializeRendering()
{
    clusterRenderer_.initializeRendering();
}

const QSet<quint32> &CLusterServer::getOnlineMachines() const
{
    return onlineMachines_ ;
}

const QMap<quint32, Node::RendererNode *> &CLusterServer::getCLRenderers() const
{
    return clRendererNodes_ ;
}

bool CLusterServer::isServerInitialized() const
{
    return status_ & Status::ServerInitialized ;
}

bool CLusterServer::machinesReady() const
{
    return  isServerInitialized() &&
            ( status_ & Status::MachinesOnline ) &&
            ( status_ & Status::MachinesConnectedToServer ) &&
            ( status_ & Status::MachinesIdentified ) ;
}

bool CLusterServer::isRenderingInitialized() const
{
    return machinesReady() && ( status_ & Status::RenderingInitialized );
}

bool CLusterServer::inRenderingLoop() const
{
    return isRenderingInitialized() && ( status_ & Status::InRenderingLoop );
}

Parallel::CLusterRenderer &CLusterServer::getCLusterRenderer()
{
    return clusterRenderer_ ;
}


void CLusterServer::nodeInfoReady_SLOT( quint32 ip )
{
    Q_EMIT this->rendererNodeInfoReady_SIGNAL( ip );
}

void CLusterServer::nodeDisconnected_SLOT( quint32 ip )
{
    // Either implement routine to try reconnection,
    // Or just drop it. Drop it for now.

    dropRendererNode( ip );

    Q_EMIT this->rendererNodeDisconnected_SIGNAL( ip );
}

void CLusterServer::remoteNodesVerifiedSSH_SLOT()
{

    QSet< quint32 > verifiedNodes =
            networkManagerSSH_->getVerfiedRemoteNodes( );


    LOG_DEBUG("Remote nodes verified count:%d" , verifiedNodes.size( ));


    fetchRenderingMachines( verifiedNodes.toList( ));

}

void CLusterServer::remoteNodesFetchedSSH_SLOT()
{

    QMap< quint32 , RemoteNodeSSH* > fetchedNodesSSH =
            networkManagerSSH_->getAttachedNodes();

    LOG_DEBUG("Remote nodes fetched count:%d", fetchedNodesSSH.size());


    for( const quint32 ip : fetchedNodesSSH.keys( ))
        onlineMachines_ << ip ;

    status_ |= Status::MachinesOnline ;

    Q_EMIT this->newOnlineMachines_SIGNAL();

}

void CLusterServer::newFrameConnection_SLOT()
{

    QTcpSocket *frameSocket = framesServer_.nextPendingConnection();

    quint32 peerAddress = frameSocket->peerAddress().toIPv4Address();

    clRendererNodes_[ peerAddress ]->setFrameSocket_( frameSocket );
}

void CLusterServer::incomingConnection( qintptr socketDescriptor )
{

    const SerializableTransformation &globalTransformation =
            clusterRenderer_.getGlobalTransformation();

    Node::RendererNode *node =
            clusterRenderer_.instanceOfCLRenderer( socketDescriptor ,
                                                   globalTransformation ) ;

    quint32 ip = node->getControlSocket().peerAddress().toIPv4Address();

    clRendererNodes_[ ip ] = node ;

    LOG_DEBUG("New connection registered: %s",
              QHostAddress( ip ).toString().toStdString().c_str( ));


    connect( node , SIGNAL( nodeInfoReady_SIGNAL( quint32 )) ,
             this , SLOT( nodeInfoReady_SLOT( quint32 )));

    // Require node data.
    node->requestNodeInfo( );


    Q_EMIT this->rendererNodeConnected_SIGNAL( ip );

}


}
