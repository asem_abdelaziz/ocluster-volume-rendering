#include "CLusterRenderer.h"
#include "Logger.h"

namespace ocluster
{
namespace Parallel
{

CLusterRenderer::CLusterRenderer(
        const QMap<quint32, Node::RendererNode *>  &clRendererNodes ,
        const clparen::CLData::FRAME_CHANNEL_ORDER frameChannelOrder,
        QObject *parent )
    : clRendererNodes_( clRendererNodes ) ,
      frameChannelOrder_( frameChannelOrder ),
      QObject( parent )
{
    status_ = 0 ;


    // Translation
    transformation_.translation.x = INITIAL_VOLUME_CENTER_X;
    transformation_.translation.y = INITIAL_VOLUME_CENTER_X;
    transformation_.translation.z = INITIAL_VOLUME_CENTER_Z;

    // Rotation
    transformation_.rotation.x = INITIAL_VOLUME_ROTATION_X;
    transformation_.rotation.y = INITIAL_VOLUME_ROTATION_Y;
    transformation_.rotation.z = INITIAL_VOLUME_ROTATION_Z;

    //Scale
    transformation_.scale.x = INITIAL_VOLUME_SCALE_X;
    transformation_.scale.y = INITIAL_VOLUME_SCALE_Y;
    transformation_.scale.z = INITIAL_VOLUME_SCALE_Z;

    //transfer parameters
    transformation_.transferFunctionOffset = INITIAL_TRANSFER_OFFSET;
    transformation_.transferFunctionScale  = INITIAL_TRANSFER_SCALE;

    transformation_.maxSteps = 500 ;
    transformation_.stepSize = 0.02 ;
    transformation_.apexAngle = 0.1f ;

    compositorThreadPool_.setMaxThreadCount( 2 );
    compositorThreadPool_.setExpiryTimeout( -1 );
    collectingThreadPool_.setExpiryTimeout( -1 );
}

CLusterRenderer::~CLusterRenderer()
{

}

void CLusterRenderer::initializeRendering( QMap< quint32 , quint32 > toDeploy ,
                                           bool deployAllDevices )
{
    if( deployAllDevices )
        for( const quint32 ip : clRendererNodes_.uniqueKeys( ))
            for( const quint32 index :
                 clRendererNodes_[ ip ]->getNodeInfo()
                 .getRendererDevicesInfo().keys( ))
                toDeploy_.insertMulti( ip , index );
    else
        toDeploy_ = toDeploy ;

    if( initializingState() || clusterReady( ))
    {
        LOG_WARNING("Cluster is being initialized or already initialized");
        return ;
    }

    status_ |= Status::InitializingRendering ;


    for( const quint32 ip : toDeploy_.uniqueKeys( ))
    {
        connect( clRendererNodes_[ ip ] ,
                 SIGNAL(deviceDeployed_SIGNAL( quint32 , quint32 )) ,
                 this , SLOT(deviceDeployed_SLOT( quint32 , quint32 )));

        connect( clRendererNodes_[ ip ] ,
                 SIGNAL(frameReady_SIGNAL( quint32 , quint32 )) ,
                 this , SLOT(frameReady_SLOT( quint32 , quint32 )));

        connect( clRendererNodes_[ ip ] , SIGNAL(nodeReady_SIGNAL( quint32 )) ,
                 this , SLOT(nodeReady_SLOT( quint32 )));
    }

    QMap< quint32 , quint32 >::const_iterator it = toDeploy_.begin();
    while( it != toDeploy_.end())
    {
        clRendererNodes_[ it.key() ]->deployGPU( it.value() );
        it++;
    }

}


bool CLusterRenderer::deployDevice( quint32 ip , quint32 deviceIndex )
{
    return clRendererNodes_[ ip ]->deployGPU( deviceIndex );
}



void CLusterRenderer::distributeVolume(
        const QMap<quint32, quint32> &deployedDevices ,
        bool deepCopy )
{
    distributeVolumeMemoryWeighted( deployedDevices, deepCopy );
}


const SerializableTransformation &
CLusterRenderer::getGlobalTransformation() const
{
    return serializableTransformation_ ;
}

bool CLusterRenderer::clusterReady() const
{
    return status_ & Status::ClusterReady ;
}

bool CLusterRenderer::inRendering() const
{
    return clusterReady() && ( status_ & Status::InRendering );
}

bool CLusterRenderer::initializingState() const
{
    return status_ & Status::InitializingRendering ;
}

bool CLusterRenderer::pendingTransformation() const
{
    return status_ & Status::PendingTransformation ;
}

bool CLusterRenderer::readyForNewFrame() const
{
    return status_ & Status::ReadyForNewFrame ;
}

const QMap< quint32 , quint32 > &CLusterRenderer::getDeployedDevices() const
{
    return deployedDevices_ ;
}

void CLusterRenderer::deviceDeployed_SLOT( quint32 ip , quint32 gpuIndex )
{
    deployedDevices_.insertMulti( ip , gpuIndex );

    Q_EMIT deviceDeployed_SIGNAL( ip , gpuIndex );

    // If requested devices are deployed, distribute volume.
    QMap< quint32 , quint32 >::const_iterator toDeployIt =
            toDeploy_.begin();

    while( toDeployIt != toDeploy_.end( ))
    {
        if( deployedDevices_.contains( toDeployIt.key( )))
        {
            if( deployedDevices_.values( toDeployIt.key( ))
                    .contains( toDeployIt.value( )) == false )
                return;

        }
        else
            return;

        toDeployIt++;
    }

    Q_EMIT devicesDeployed_SIGNAL( true );

    LOG_DEBUG("Requested Devices Deployed");

    // disconnect unused signal/slot to reduce overheads.
    for( const quint32 ip : clRendererNodes_.keys( ))
        disconnect( clRendererNodes_[ ip ] ,
                    SIGNAL(deviceDeployed_SIGNAL( quint32 , quint32 )) ,
                    this ,
                    SLOT(deviceDeployed_SLOT( quint32 , quint32 )));

}



void CLusterRenderer::nodeReady_SLOT( quint32 ip )
{
    disconnect( clRendererNodes_[ ip ] , SIGNAL(nodeReady_SIGNAL( quint32 )) ,
                this , SLOT(nodeReady_SLOT( quint32 )));

    collectingThreadPool_.setMaxThreadCount( deployedDevices_.size( ));

    for( const quint32 ip : clRendererNodes_.keys( ))
        if( !clRendererNodes_[ ip ]->nodeReady( ))
            return;

    LOG_INFO("[DONE] Subvolumes are distributed to all deployed GPUs");

    status_ &= ~ static_cast< int >( Status::InitializingRendering );
    status_ |= Status::ClusterReady ;
    status_ |= Status::ReadyForNewFrame;
    Q_EMIT this->clusterReady_SIGNAL( clusterReady( ));
}



void CLusterRenderer::updateRotationX_SLOT( int angle )
{
    transformation_.rotation.x = angle;
    applyTransformation_();
}

void CLusterRenderer::updateRotationY_SLOT( int angle )
{
    transformation_.rotation.y = angle;
    applyTransformation_();
}

void CLusterRenderer::updateRotationZ_SLOT( int angle )
{
    transformation_.rotation.z = angle;
    applyTransformation_();
}

void CLusterRenderer::updateTranslationX_SLOT( int distance )
{
    transformation_.translation.x = distance;
    applyTransformation_();
}

void CLusterRenderer::updateTranslationY_SLOT( int distance )
{
    transformation_.translation.y = distance;
    applyTransformation_();
}

void CLusterRenderer::updateTranslationZ_SLOT( int distance )
{
    transformation_.translation.z = distance;
    applyTransformation_();
}

void CLusterRenderer::updateScaleX_SLOT( int scale )
{
    transformation_.scale.x = scale;
    applyTransformation_();
}

void CLusterRenderer::updateScaleY_SLOT( int scale )
{
    transformation_.scale.y = scale;
    applyTransformation_();
}

void CLusterRenderer::updateScaleZ_SLOT( int scale )
{
    transformation_.scale.z = scale;
    applyTransformation_();
}

void CLusterRenderer::updateScaleXYZ_SLOT( int scale )
{
    transformation_.scale.x = scale;
    transformation_.scale.y = scale;
    transformation_.scale.z = scale;
    applyTransformation_();
}

void CLusterRenderer::updateImageBrightness_SLOT( float brightness )
{
    transformation_.brightness = brightness;
    applyTransformation_();
}

void CLusterRenderer::updateVolumeDensity_SLOT( float density )
{
    transformation_.volumeDensity = density;
    applyTransformation_();
}

void CLusterRenderer::updateIsoValue_SLOT( float isoValue )
{
    LOG_INFO("new Iso value %f",isoValue);
    transformation_.isoValue = isoValue;
    applyTransformation_();

}

void CLusterRenderer::activateRenderingKernel_SLOT(
        clparen::CLKernel::RenderingMode type )
{
    clusterCompositor_.switchCompositingKernel( type );
    for( const quint32 ip : clRendererNodes_.uniqueKeys( ))
        clRendererNodes_[ ip ]->switchRenderingKernel( type );


    applyTransformation_(  );
}


}
}
