#ifndef CLUSTERSORTLAST_IPP
#define CLUSTERSORTLAST_IPP

namespace ocluster
{
namespace Parallel
{

//template class CLusterSortLast< uint8_t , uint8_t >;
//template class CLusterSortLast< uint8_t , uint16_t >;
//template class CLusterSortLast< uint8_t , uint32_t >;
//template class CLusterSortLast< uint8_t , half  >;
template class CLusterSortLast< uint8_t , float > ;


}
}

#endif // CLUSTERSORTLAST_IPP
