#include "CLusterSortLast.h"


namespace ocluster
{
namespace Parallel
{



template< class V , class F >
CLusterSortLast< V , F >::CLusterSortLast(
        const QMap< quint32 , Node::RendererNode* > &clRendererNodes ,
        const clparen::CLData::FRAME_CHANNEL_ORDER frameChannelOrder,
        QObject *parent )
    : CLusterRenderer( clRendererNodes , frameChannelOrder , parent )
{

}

template< class V , class F >
void CLusterSortLast< V , F >::loadBaseVolume(
        const std::string volumePath , const bool memoryMapVolume)
{
    LOG_DEBUG("Loading Volume Path:%s", volumePath.c_str());
    baseVolume_.reset( new Volume< V >( volumePath , memoryMapVolume )) ;
    LOG_DEBUG("[DONE] Loading Volume Path:%s", volumePath.c_str());
}

template< class V , class F >
void CLusterSortLast< V , F >::startRendering()
{

    if( inRendering( ))
    {
        LOG_WARNING("Cluster already in rendering!");
        return ;
    }

    LOG_DEBUG("Start rendering..");
    status_ |= Status::InRendering ;

    for( const quint32 ip : clRendererNodes_.uniqueKeys( ))
        clusterCompositor_.allocateFrames( clRendererNodes_[ ip ]);

    compositeTask_ = new CompositeTask( clusterCompositor_ );

    connect( &clusterCompositor_ , SIGNAL(compositingReady_SIGNAL( )) ,
             this , SLOT(compositingReady_SLOT( )));

    connect( &clusterCompositor_ , SIGNAL(compositingFinished_SIGNAL( )) ,
             this , SLOT(compositingFinished_SLOT( )));

    applyTransformation_( );

}

template< class V , class F >
void CLusterSortLast< V , F >::frameReady_SLOT( quint32 ip , quint32 gpuIndex )
{
    //    LOG_DEBUG("Frame Ready");

    Node::CLRendererNode< V , F > *node =
            dynamic_cast< Node::CLRendererNode< V , F> * >( clRendererNodes_[ ip ]);

    clparen::CLData::CLImage2D< F > *frame =
            node->getFrames()[ gpuIndex ];

    if( deployedDevices_.count() > 1 )
        QtConcurrent::run(  &collectingThreadPool_ ,
                            &clusterCompositor_ , &CLusterCompositor::uploadFrame ,
                            frame , true );
    else
    {
        status_ |= Status::ReadyForNewFrame ;

        if( pendingTransformation( ))
            applyTransformation_();

        Q_EMIT this->finalFrameReady_SIGNAL( &frame->getFramePixmap( ));
    }
}

template< class V , class F >
void CLusterSortLast< V , F >::compositingFinished_SLOT()
{
    LOG_DEBUG("[DONE] Compositing");
    clparen::CLData::CLImage2D< F > *finalFrame =
            clusterCompositor_.getFinalFrame().
            value< clparen::CLData::CLImage2D< F >* >();

    status_ |= Status::ReadyForNewFrame ;

    if( pendingTransformation( ))
        applyTransformation_();

    Q_EMIT this->finalFrameReady_SIGNAL( &finalFrame->getFramePixmap( ));
}

template< class V , class F >
void CLusterSortLast< V , F >::compositingReady_SLOT()
{
    compositorThreadPool_.start( compositeTask_ );
}

template< class V , class F >
Dimensions3D CLusterSortLast< V , F >::getVolumeDimensions() const
{
    return baseVolume_->getDimensions();
}

template< class V , class F >
Node::RendererNode *CLusterSortLast< V , F >::instanceOfCLRenderer(
        const quintptr socketDescriptor,
        const SerializableTransformation &transformations,
        QObject *parent ) const
{
    return new Node::CLRendererNode< V , F >( socketDescriptor ,
                                              transformations ,
                                              frameChannelOrder_ ,
                                              parent );
}

template< class V , class F >
std::shared_ptr< SerializableVolume< V >>
CLusterSortLast< V , F >::cloneBrick_(
        const BrickParameters< V > &brickParameters)
{
    return std::make_shared< SerializableVolume< V >>( brickParameters , true );
}

template< class V , class F >
void CLusterSortLast< V , F >::applyTransformation_()
{
    if( !clusterReady())
    {
        LOG_WARNING("Cluster is not ready yet!");
        return ;
    }

    if( !inRendering())
    {
        startRendering(  );
        return ;
    }

    if( !readyForNewFrame( ))
    {
        status_ |= Status::PendingTransformation  ;
    }
    else
    {
        status_ &= ~ static_cast< int >( Status::ReadyForNewFrame );
        status_ &= ~ static_cast< int >( Status::PendingTransformation ) ;

        LOG_DEBUG("Broadcast Transformation");
        serializableTransformation_ = transformation_ ;
        for( const quint32 ip : clRendererNodes_.uniqueKeys( ))
        {
            clRendererNodes_[ ip ]->renderNewFrame();
        }
    }
}

template< class V , class F >
Node::CLRendererNode< V, F >
*CLusterSortLast< V , F >::clRendererNode_( Node::RendererNode *node)
{
    return dynamic_cast< Node::CLRendererNode< V, F > *>( node );
}


template< class V , class F >
void CLusterSortLast< V , F >::distributeVolumeMemoryWeighted(
        const QMap< quint32 , quint32 > &deployedDevices  ,
        bool deepCopy )
{
    QVector< uint > memoryScores ;
    for( const quint32 ip : deployedDevices.uniqueKeys( ))
        for( const quint32 index : deployedDevices.values( ip ))
            memoryScores.append( clRendererNodes_[ ip ]
                                 ->getNodeInfo()
                                 .getRendererDevicesInfo()[ index ]
                                 .globalMemory / ( 1024 * 1024 ));


    QVector< BrickParameters< V >> bricks =
            baseVolume_->weightedBricking1D( memoryScores );

    QVector< std::shared_ptr< SerializableVolume< V >>> serializableVolumes ;

    if( deepCopy )
    {
        QVector< QFuture< std::shared_ptr< SerializableVolume< V >>>> futureVector;

        // Concurrent cloning.
        for( const BrickParameters< V > &brick : bricks )
            futureVector << QtConcurrent::run(
                                this , &CLusterSortLast::cloneBrick_ , brick );


        for( auto &future : futureVector )
            serializableVolumes << future.result() ;

        baseVolume_.reset() ;
    }
    else
        for( const BrickParameters< V > &brick : bricks )
            serializableVolumes.append(
                        std::make_shared< SerializableVolume< V >>( brick ));

    Q_EMIT doneBricking_SIGNAL();

    int brickIndex = 0 ;
    for( const quint32 ip : deployedDevices.uniqueKeys( ))
        for( const quint32 index : deployedDevices.values( ip ))
        {
            serializableVolumes[ brickIndex ]->setTargetGPUIndex( index );

            Node::CLRendererNode< V , F > *node =
                    clRendererNode_( clRendererNodes_[ ip ]);

            node->loadVolumeToDevice( serializableVolumes[ brickIndex ] ,
                                      index );
            brickIndex++;
        }
}


}
}


#include "CLusterSortLast.ipp"
