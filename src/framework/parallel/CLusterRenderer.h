#ifndef CLUSTERRENDERER_H
#define CLUSTERRENDERER_H

#include <QObject>
#include <QTcpServer>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>

#include "RendererNode.h"
#include "CLusterCompositor.h"
#include "SerializableTransformation.h"
#include "SerializableVolume.h"
#include "CompositeTask.h"
#include "clparen/clparen.h"

//#define VOLUME_PATH \
//    "/projects/volume-datasets/skull-scales/skull-2048-2048-1024/output-voulme"


#define VOLUME_PATH \
    "/projects/volume-datasets/skull/skull"


//#define VOLUME_PATH \
//    "/projects/volume-datasets/skull-scales/skull-1024-1024-1024/output-voulme"

#define INITIAL_VOLUME_CENTER_X 0.0
#define INITIAL_VOLUME_CENTER_Y 0.0
#define INITIAL_VOLUME_CENTER_Z 0.0
#define INITIAL_VOLUME_ROTATION_X 0.0
#define INITIAL_VOLUME_ROTATION_Y 0.0
#define INITIAL_VOLUME_ROTATION_Z 0.0
#define INITIAL_VOLUME_SCALE_X 1.0
#define INITIAL_VOLUME_SCALE_Y 1.0
#define INITIAL_VOLUME_SCALE_Z 1.0
#define INITIAL_TRANSFER_SCALE    1.0
#define INITIAL_TRANSFER_OFFSET   0.0



namespace ocluster
{
namespace Parallel
{

/**
 * @brief The CLusterRenderer class
 * Server that controls the renderer nodes.
 */
class CLusterRenderer : public QObject
{
    Q_OBJECT

    Q_DISABLE_COPY( CLusterRenderer )


public:
    enum Status : int { InitializingRendering = 1 ,
                        ClusterReady = 2 ,
                        InRendering = 4 ,
                        PendingTransformation = 8 ,
                        ReadyForNewFrame = 16 } ;

    CLusterRenderer( const QMap< quint32 , Node::RendererNode* > &clRendererNodes ,
                     const clparen::CLData::FRAME_CHANNEL_ORDER frameChannelOrder ,
                     QObject *parent = 0 );

    ~CLusterRenderer( );


    /**
     * @brief loadBaseVolume
     * @param volume
     */
    virtual void loadBaseVolume( const std::string volumePath ,
                                 const bool memoryMapVolume = true ) = 0 ;

    /**
     * @brief getVolumeDimensions
     * @return
     */
    virtual Dimensions3D getVolumeDimensions() const = 0;

    /**
     * @brief distributeVolumeMemoryWeighted
     * distribute base volume among nodes.
     */
    virtual void distributeVolumeMemoryWeighted(
            const QMap<quint32, quint32> &deployedDevices ,
            bool deepCopy = false ) = 0 ;

    /**
     * @brief startRendering
     */
    virtual void startRendering(  ) = 0 ;


    /**
     * @brief instanceOfCLRenderer
     * @param socketDescriptor
     * @param transformations
     * @param parent
     * @return
     */
    virtual Node::RendererNode* instanceOfCLRenderer(
            const quintptr socketDescriptor ,
            const SerializableTransformation &transformations ,
            QObject *parent = 0) const = 0 ;

    /**
     * @brief initializeRendering
     */
    virtual void initializeRendering(
            QMap< quint32 , quint32 > toDeploy = QMap< quint32 , quint32 >() ,
            bool deployAllDevices = true ) ;

    /**
     * @brief deployDevice
     * @param ip
     * @param address
     * @return
     */
    virtual bool deployDevice( quint32 ip , quint32 deviceIndex );

    /**
     * @brief distributeVolume
     * @param deployedDevices
     * @param deepCopy
     */
    void distributeVolume( const QMap<quint32, quint32> &deployedDevices ,
                           bool deepCopy = false  );



    /**
     * @brief getGlobalTransformation
     * @return
     */
    const SerializableTransformation &getGlobalTransformation() const ;

    /**
     * @brief clusterReady
     * @return
     */
    bool clusterReady() const ;

    /**
     * @brief inRendering
     * @return
     */
    bool inRendering() const ;

    /**
     * @brief initializingState
     * @return
     */
    bool initializingState() const;

    /**
     * @brief pendingTransformation
     * @return
     */
    bool pendingTransformation() const ;

    /**
     * @brief readyForNewFrame
     * @return
     */
    bool readyForNewFrame() const ;

    /**
     * @brief getDeployedDevices
     * @return
     */
    const QMap<quint32, quint32> &getDeployedDevices() const;

Q_SIGNALS:
    /**
     * @brief finalFrameReady_SIGNAL
     * @param finalFrame
     */
    void finalFrameReady_SIGNAL( QPixmap *finalFrame ) ;

    /**
     * @brief clusterReady_SIGNAL
     */
    void clusterReady_SIGNAL( bool ready );

    /**
     * @brief deviceDeployed_SIGNAL
     * @param ip
     * @param gpuIndex
     */
    void deviceDeployed_SIGNAL( quint32 ip , quint32 gpuIndex );

    /**
     * @brief devicesDeployed_SIGNAL
     */
    void devicesDeployed_SIGNAL( bool deployed );

    /**
     * @brief doneBricking_SIGNAL
     */
    void doneBricking_SIGNAL( );


public Q_SLOTS:
    /**
     * @brief frameReady_SLOT
     * @param ip
     * @param gpuIndex
     */
    virtual void frameReady_SLOT( quint32 ip , quint32 gpuIndex ) = 0 ;

    /**
     * @brief compositingReady_SLOT
     */
    virtual void compositingReady_SLOT( ) = 0 ;

    /**
     * @brief compositingFinished_SLOT
     */
    virtual void compositingFinished_SLOT( ) = 0 ;

    /**
     * @brief deviceDeployed_SLOT
     * @param ip
     * @param gpuIndex
     */
    virtual void deviceDeployed_SLOT( quint32 ip , quint32 gpuIndex );


    /**
     * @brief nodeReady_SLOT
     * @param ip
     */
    virtual void nodeReady_SLOT( quint32 ip ) ;


    /**
     * @brief updateRotationX_SLOT
     * @param angle
     */
    void updateRotationX_SLOT( int angle );

    /**
     * @brief updateRotationY_SLOT
     * @param angle
     */
    void updateRotationY_SLOT( int angle );

    /**
     * @brief updateRotationZ_SLOT
     * @param angle
     */
    void updateRotationZ_SLOT( int angle );

    /**
     * @brief updateTranslationX_SLOT
     * @param distance
     */
    void updateTranslationX_SLOT( int distance );

    /**
     * @brief updateTranslationY_SLOT
     * @param distance
     */
    void updateTranslationY_SLOT( int distance );
    /**
     * @brief updateTranslationZ_SLOT
     * @param distance
     */
    void updateTranslationZ_SLOT( int distance );

    /**
     * @brief updateScaleX_SLOT
     * @param distance
     */
    void updateScaleX_SLOT( int scale );
    /**
     * @brief updateScaleY_SLOT
     * @param distance
     */
    void updateScaleY_SLOT( int scale );
    /**
     * @brief updateScaleZ_SLOT
     * @param distance
     */
    void updateScaleZ_SLOT( int scale );
    /**
     * @brief updateScaleXYZ_SLOT
     * @param distance
     */
    void updateScaleXYZ_SLOT( int scale );
    /**
     * @brief updateImageBrightness_SLOT
     * @param brithness
     */
    void updateImageBrightness_SLOT( float brightness );

    /**
     * @brief updateVolumeDensity_SLOT
     * @param density
     */
    void updateVolumeDensity_SLOT( float density );

    /**
     * @brief updateIsoValue_SLOT
     * @param isoValue
     */
    void updateIsoValue_SLOT(float isoValue );

    /**
     * @brief activateRenderingKernel_SLOT
     * @param type
     */
    void activateRenderingKernel_SLOT( clparen::CLKernel::RenderingMode type );



protected:
    /**
     * @brief applyTransformation_
     */
    virtual void applyTransformation_( ) = 0;

protected:


    /**
     * @brief clRendererNodes_
     */
    const QMap< quint32 , Node::RendererNode* > &clRendererNodes_ ;

    /**
     * @brief clusterCompositor_
     */
    CLusterCompositor clusterCompositor_;

    /**
     * @brief transformation_
     */
    Transformation transformation_ ;

    /**
     * @brief serializableTransformation_
     */
    SerializableTransformation serializableTransformation_ ;

    /**
     * @brief lastTransformationDemand_
     */
    Transformation lastTransformationDemand_ ;


    /**
     * @brief compositeTask_
     */
    CompositeTask *compositeTask_ ;

    /**
     * @brief collectingThreadPool_
     */
    QThreadPool collectingThreadPool_ ;

    /**
     * @brief compositorThreadPool_
     */
    QThreadPool compositorThreadPool_ ;

    /**
     * @brief status_
     */
    int status_ ;


    /**
     * @brief deployedDevices_
     */
    QMap< quint32 , quint32 > deployedDevices_ ;

    /**
     * @brief toDeploy_
     */
    QMap< quint32 , quint32 > toDeploy_ ;

    /**
     * @brief frameChannelOrder_
     */
    const clparen::CLData::FRAME_CHANNEL_ORDER frameChannelOrder_ ;

};

}
}
#endif // CLUSTERRENDERER_H
