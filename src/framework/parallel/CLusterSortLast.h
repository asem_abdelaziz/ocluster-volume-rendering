#ifndef CLUSTERSORTLAST_H
#define CLUSTERSORTLAST_H

#include "CLusterRenderer.h"

namespace ocluster
{
namespace Parallel
{


template < class V , class F >
class CLusterSortLast : public CLusterRenderer
{
public:
    CLusterSortLast( const QMap< quint32 , Node::RendererNode* > &clRendererNodes ,
                     const clparen::CLData::FRAME_CHANNEL_ORDER frameChannelOrder ,
                     QObject *parent = 0 );

    /**
     * @brief getBaseVolume
     * @return
     */
    Volume< V > *getBaseVolume() const;


    void loadBaseVolume( const std::string volumePath ,
                         const bool memoryMapVolume = true ) Q_DECL_OVERRIDE;

    void startRendering( ) Q_DECL_OVERRIDE;

    void frameReady_SLOT( quint32 ip , quint32 gpuIndex ) Q_DECL_OVERRIDE;

    void compositingFinished_SLOT( ) Q_DECL_OVERRIDE;

    void compositingReady_SLOT( ) Q_DECL_OVERRIDE;

    Dimensions3D getVolumeDimensions( ) const Q_DECL_OVERRIDE;

    Node::RendererNode *instanceOfCLRenderer(
            const quintptr socketDescriptor ,
            const SerializableTransformation &transformations,
            QObject *parent ) const Q_DECL_OVERRIDE;


protected:
    /**
     * @brief cloneBrick_
     * @param brickParameters
     * @return
     */
    std::shared_ptr< SerializableVolume< V >> cloneBrick_(
            const BrickParameters< V > &brickParameters );


    void distributeVolumeMemoryWeighted(
            const QMap< quint32 , quint32 > &deployedDevices  ,
            bool deepCopy ) Q_DECL_OVERRIDE ;

    void applyTransformation_() Q_DECL_OVERRIDE;
private:
    Node::CLRendererNode< V , F > *clRendererNode_( Node::RendererNode *node );

private:
    /**
     * @brief baseVolume_
     */
    std::unique_ptr< Volume< V >> baseVolume_ ;

};


}
}
#endif // CLUSTERSORTLAST_H
