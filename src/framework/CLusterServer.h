#ifndef CLUSTERDRIVER_H
#define CLUSTERDRIVER_H

#include "CLusterRenderer.h"
#include "CLRendererNode.h"
#include "NetworkManagerSSH.h"
#include "clparen/clparen.h"


#define CONTROL_PORT 1993
#define FRAMES_PORT CONTROL_PORT + 1
#define DEFAULT_SSH_PORT 22
#define DEFAULT_USERNAME "vizuser"
#define DEFAULT_PASSWORD "alpha@viz"
#define DEFAULT_MAX_THREADS_COUNT 256

namespace ocluster
{

/**
 * @brief The CLusterDriver class
 * To auto-control the workflow of the CLusterRenderer.
 */
class CLusterServer : public QTcpServer
{
    Q_OBJECT

    Q_DISABLE_COPY( CLusterServer )

    /**
     * @brief The Status enum
     */
    enum Status : quint32 { ServerInitialized = 1,
                            MachinesOnline = 2 ,
                            MachinesConnectedToServer = 4 ,
                            MachinesIdentified = 8 ,
                            RenderingInitialized = 16 ,
                            InRenderingLoop = 32 } ;

public:
    /**
     * @brief CLusterServer
     * @param parent
     */
    CLusterServer( QMap< quint32 , Node::RendererNode* > &clRendererNodes,
                   Parallel::CLusterRenderer &clusterRenderer ,
                   QObject *parent = 0 ) ;



    /**
     * @brief initializeCluster
     */
    virtual void initializeServer( );


    /**
     * @brief scanInterface
     * @param interfaceLabel
     */
    void scanInterface( const QString interfaceLabel );


    /**
     * @brief scanRange
     * @param ipFirst
     * @param ipLast
     */
    void scanRange( const quint32 ipFirst , const quint32 ipLast );

    /**
     * @brief blindScanRenderingMachines
     */
    void blindScanRenderingMachines( );


    /**
     * @brief fetchRenderingMachines
     * @param ipAddress
     */
    void fetchRenderingMachines( const QList< quint32 > ipAddress ,
                                 const QString username = DEFAULT_USERNAME ,
                                 const QString password = DEFAULT_PASSWORD );


    /**
     * @brief connectOnlineMachinesToServer
     * @param ipAddress: if empty, connect all online machines.
     */
    void connectOnlineMachinesToServer( const std::string applicationName ,
                                        const QList< quint32 > ipAddress = QList< quint32 >( ));


    /**
     * @brief dropRendererNode
     * Drop and disconnect renderer node.
     * @param rendererNode
     */
    virtual void dropRendererNode( Node::RendererNode *rendererNode ) ;


    /**
     * @brief dropRendererNode
     * Drop and disconnect renderer node.
     * @param rendererNode
     */
    virtual void dropRendererNode( quint32 ip ) ;


    /**
     * @brief initializeRendering
     */
    virtual void initializeRendering( ) ;


    /**
     * @brief getOnlineMachines
     * @return
     */
    const QSet< quint32 > &getOnlineMachines() const ;


    /**
     * @brief getCLRenderers
     * @return
     */
    const QMap< quint32 , Node::RendererNode* > &getCLRenderers() const ;

    /**
     * @brief isServerIntialized
     * @return
     */
    bool isServerInitialized() const ;

    /**
     * @brief machinesReady
     * @return
     */
    bool machinesReady() const ;

    /**
     * @brief isRenderingInitialized
     * @return
     */
    bool isRenderingInitialized() const ;

    /**
     * @brief inRenderingLoop
     * @return
     */
    bool inRenderingLoop() const ;

    /**
     * @brief getClusterRenderer
     * @return
     */
    Parallel::CLusterRenderer &getCLusterRenderer();

Q_SIGNALS:
    /**
     * @brief rendererNodeConnected_SIGNAL
     * @param node
     */
    void rendererNodeConnected_SIGNAL( quint32 ip );

    /**
     * @brief rendererNodeDisconnected_SIGNAL
     * @param node
     */
    void rendererNodeDisconnected_SIGNAL( quint32 ip );


    /**
     * @brief rendererNodeInfoReady_SIGNAL
     * @param node
     */
    void rendererNodeInfoReady_SIGNAL( quint32 ip );


    /**
     * @brief newOnlineMachines_SIGNAL
     */
    void newOnlineMachines_SIGNAL( );

public Q_SLOTS:

    /**
     * @brief rendererNodeInfoReady_SLOT
     * @param node
     */
    virtual void nodeInfoReady_SLOT( quint32 ip );

    /**
     * @brief rendererNodeDisconnected_SLOT
     */
    virtual void nodeDisconnected_SLOT( quint32 ip ) ;


    /**
     * @brief remoteNodesVerifiedSSH_SLOT
     */
    virtual void remoteNodesVerifiedSSH_SLOT( );


    /**
     * @brief remoteNodesFetchedSSH_SLOT
     */
    virtual void remoteNodesFetchedSSH_SLOT( ) ;


    /**
     * @brief newFrameConnection_SLOT
     */
    virtual void newFrameConnection_SLOT(  );

protected:
    /**
     * @brief incomingConnection
     * @param socketDescriptor
     */
    virtual void incomingConnection( qintptr socketDescriptor ) Q_DECL_OVERRIDE ;



private:
    /**
     * @brief clRendererNodes_
     */
    QMap< quint32 , Node::RendererNode* > &clRendererNodes_ ;

    /**
     * @brief clusterRenderer_
     */
    Parallel::CLusterRenderer &clusterRenderer_;

    /**
     * @brief framesServer_
     */
    QTcpServer framesServer_ ;

    /**
     * @brief networkManagerSSH_
     */
    NetworkManagerSSH *networkManagerSSH_ ;

    /**
     * @brief threadPool_
     */
    QThreadPool threadPool_ ;

    /**
     * @brief onlineMachines_
     */
    QSet< quint32 > onlineMachines_ ;

    /**
     * @brief status_
     */
    int status_ ;

};

}

#endif // CLUSTERDRIVER_H
