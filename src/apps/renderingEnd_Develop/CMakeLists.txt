# Application name
set( RENDERING_END_DEVELOP_APP "renderingEnd_Develop" )

# Application source/header/ui files
set( RENDERING_END_DEVELOP_APP_SOURCES renderingEnd_Develop.cpp )
set( RENDERING_END_DEVELOP_APP_HEADERS renderingEnd_Develop.h)

# Generate the executables
add_executable( ${RENDERING_END_DEVELOP_APP}
    ${RENDERING_END_DEVELOP_APP_SOURCES}
    ${RENDERING_END_DEVELOP_APP_HEADERS}
    )

# Link the application against the GLDPVR library
target_link_libraries( ${RENDERING_END_DEVELOP_APP} ${OCLUSTER_LIB} )


# Make sure to use the required Qt modules
qt5_use_modules( ${RENDERING_END_DEVELOP_APP} Core Network Gui )


# Install the executable (generate with 'make install' in the bin directory)
install( TARGETS ${RENDERING_END_DEVELOP_APP} DESTINATION bin )




# Generate the run script.
# Add the shell script to run the application.
set( RENDERING_END_RUNSCRIPT ${RENDERING_END_DEVELOP_APP}.sh )
set( RUNSCRIPT_DIR  $ENV{HOME}/.${RENDERING_END_DEVELOP_APP} )

# Create hidden directory /home/<user>/.<app_name>
install(DIRECTORY DESTINATION ${RUNSCRIPT_DIR})

set( APP_EXECUTABLE ${CMAKE_INSTALL_PREFIX}/bin/${RENDERING_END_DEVELOP_APP} )

# Now the shell script wraps the absolute path of the executable.
configure_file( ${APPLICATION_RUNSCRIPT_RAW} ${RENDERING_END_RUNSCRIPT} @ONLY)

# Install the runscript shell at /home/<user>/.<app_name>
install( PROGRAMS ${CMAKE_CURRENT_BINARY_DIR}/${RENDERING_END_RUNSCRIPT} DESTINATION ${RUNSCRIPT_DIR} )

set( RENDERING_END_RUNSCRIPT_PATH ${RUNSCRIPT_DIR}/${RENDERING_END_RUNSCRIPT} CACHE INTERNAL "")
