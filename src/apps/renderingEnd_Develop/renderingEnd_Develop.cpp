#include "renderingEnd_Develop.h"
#include <QCoreApplication>
#include "CLRendererEnd.h"
#include "MetaSerialization.hh"
#include "QDateTime"

using namespace ocluster;

int main( int argc , char **argv )
{
    QCoreApplication a( argc , argv );
    CORE_SERIALIZATION_INIT();

//    const QString datetime = QDateTime::currentDateTime().toString();
//    const QString fileName = QString("%1/%2.txt")
//                             .arg( qApp->applicationDirPath() )
//                             .arg( datetime );

//    const std::string fileNameStd = fileName.toStdString();
//    FILE *file = fopen(fileNameStd.c_str(), "a");
//    Logger::setFileDescriptor( file );
    const QString serverAddress = argv[1];
    const QHostAddress qServerAddress( serverAddress );
    const int serverPort = QString( argv[2] ).toInt();

    Node::CLRendererEnd< uchar , float > rendererEnd( qServerAddress , serverPort );

    return a.exec();
}
