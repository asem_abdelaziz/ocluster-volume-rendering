#ifndef RENDERINGWINDOW_H
#define RENDERINGWINDOW_H

#include <QMainWindow>
#include "CLusterServer.h"
#include "QVector2D"
namespace Ui {
class RenderingWindow;
}

class RenderingWindow : public QMainWindow
{
    Q_OBJECT

public:

    RenderingWindow( ocluster::CLusterServer &clusterServer ,
                     QWidget *parent = 0 );

    ~RenderingWindow();


public Q_SLOTS:
    void finalFrameReady_SLOT( QPixmap *finalFrame );

    void switchRenderingKernel_SLOT( );

    void newDensity_SLOT( int value );

    void newBrightness_SLOT( int value );

    void newXYZScaling_SLOT( int value );

    void newZScaling_SLOT( int value );

    void newYScaling_SLOT( int value );

    void newXScaling_SLOT( int value );

    void newZTranslation_SLOT( int value );

    void newYTranslation_SLOT( int value );

    void newXTranslation_SLOT( int value );

    void newZRotation_SLOT( int value );

    void newYRotation_SLOT( int value );

    void newXRotation_SLOT( int value );

    void newIsoValue_SLOT( int value );

    void initializeConnections_SLOT( );

    void mousePressed_SLOT( QVector2D mousePressedPosition);

    void mouseMoved_SLOT( QVector2D newPosition);

    void mouseWheelMoved_SLOT(QWheelEvent* event);


private:
    void initializeConnections_( );

    void newMouseXRotation_( );

    void newMouseYRotation_( );

    void newMouseZTranslation_(int value );


private:
    Ui::RenderingWindow *ui;

    ocluster::CLusterServer &clusterServer_ ;

    QVector2D lastMousePosition_;

    QVector2D mousePositionDifference_;

    float mouseXRotationAngle_;

    float mouseYRotationAngle_;

};

#endif // RENDERINGWINDOW_H
