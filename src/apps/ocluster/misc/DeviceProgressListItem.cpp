#include "DeviceProgressListItem.h"
#include "Logger.h"
DeviceProgressListItem::DeviceProgressListItem( const QPixmap &icon,
                                                const quint32 ip,
                                                const QString username,
                                                const CLRendererDeviceInfo deviceInfo,
                                                QWidget *parent )
    : DeviceListItem( icon , ip , username , deviceInfo , parent )
{

    setAttribute( Qt::WA_DeleteOnClose );
    clearLayout_();

    progressBar_ = new QProgressBar(  );
    progressBar_->setMinimum( 0 );
    progressBar_->setMaximum( 0 );
    progressBar_->setValue( 0 );

//    layout_.reset( new QHBoxLayout( this ));
    gpuIcon_.reset( new QLabel(  ));
    description_.reset( new QLabel(   ));

    gpuIcon_->setPixmap( icon_ );

    QString description(
                QHostAddress( ip_ ).toString().leftJustified( 15 , ' ' , true )
                + QString(tr("\nuser:%1\n%2(%3)\nMem: %4 (GB)")
                          .arg( username )
                          .arg( deviceInfo.name )
                          .arg( QString::number( deviceInfo.localGPUIndex ))
                          .arg( QString::number( deviceInfo
                                                 .globalMemory / B2GB ))));

    description_->setText( description );
    description_->setAlignment( Qt::AlignLeft );

    layout_->addWidget( gpuIcon_.data( ));
    layout_->addWidget( description_.data( ));
    layout_->addWidget( progressBar_ );

}

void DeviceProgressListItem::configProgressBar( quint64 steps )
{
    progressBar_->setMaximum( static_cast< int >( steps ));
}

void DeviceProgressListItem::setProgressBar( quint64 steps )
{
    progressBar_->setValue( static_cast< int >( steps ));

    LOG_DEBUG("Progress[%d]:%d/%d" , deviceInfo_.localGPUIndex ,
              progressBar_->value() , progressBar_->maximum());

}


