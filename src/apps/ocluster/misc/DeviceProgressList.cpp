#include "DeviceProgressList.h"

DeviceProgressList::DeviceProgressList( QWidget *centeralWidget )
    : DeviceList( centeralWidget )
{


}

bool DeviceProgressList::addDevice( const quint32 ip,
                                    const QString username,
                                    const CLRendererDeviceInfo deviceInfo )
{
    QMutexLocker lock( &mutex_ );


    if( contains( ip , deviceInfo.localGPUIndex ))
        return false ;


    QListWidgetItem *item =
            new QListWidgetItem( this );

    DeviceProgressListItem *device
            = new DeviceProgressListItem( vendorIcon_( deviceInfo.vendor ) ,
                                          ip , username , deviceInfo );

    item->setSizeHint( device->getIcon().size() + QSize( 20 , 30 ));

    addItem( item );
    setItemWidget( item , device );

    devices_.insertMulti( ip , device );

    return true ;
}


bool DeviceProgressList::contains( const quint32 ip , const quint32 index ) const
{
    QMutexLocker lock( &mutex_ );
    const QList< DeviceProgressListItem* > devicesByIp =
            devices_.values( ip ) ;

    if( !devicesByIp.isEmpty( ))
        for( const DeviceProgressListItem *device : devicesByIp )
            if( device->getGPUIndex() == index )
                return true ;
    return false;
}

bool DeviceProgressList::isEmpty() const
{
    QMutexLocker lock( &mutex_ );
    return devices_.isEmpty();
}

void DeviceProgressList::configProgress_SLOT( quint32 ip ,
                                              quint32 index ,
                                              quint64 segments )
{
    QMutexLocker lock( &mutex_ );

    DeviceProgressListItem* device =
            getDeviceProgressItem_( ip , index );

    if( device )
        device->configProgressBar( segments );

}

void DeviceProgressList::updateProgress_SLOT( quint32 ip ,
                                              quint32 index ,
                                              quint64 segments )
{
    QMutexLocker lock( &mutex_ );

    DeviceProgressListItem* device =
            getDeviceProgressItem_( ip , index );

    if( device )
        device->setProgressBar( segments );
}

DeviceProgressListItem
*DeviceProgressList::getDeviceProgressItem_( quint32 ip,
                                             quint32 index)
{
    QMutexLocker lock( &mutex_ );
    const QList< DeviceProgressListItem* > devicesByIp =
            devices_.values( ip ) ;

    if( !devicesByIp.isEmpty( ))
        for( DeviceProgressListItem *device : devicesByIp )
            if( device->getGPUIndex() == index )
                return device ;

    return nullptr ;
}

