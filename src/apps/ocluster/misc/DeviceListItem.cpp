#include "DeviceListItem.h"


DeviceListItem::DeviceListItem( const QPixmap &icon ,
                                const quint32 ip ,
                                const QString username ,
                                const CLRendererDeviceInfo deviceInfo ,
                                QWidget *parent )
    : QWidget( parent ) ,
      deviceInfo_( deviceInfo ) ,
      icon_( icon.scaled( 55 , 55 , Qt::KeepAspectRatio )) ,
      gpuIndex_( deviceInfo.localGPUIndex ) ,
      ip_( ip ) ,
      username_( username )
{
    setAttribute( Qt::WA_DeleteOnClose );

    //    stackedLayout_ = new QStackedLayout( this );

    layout_.reset( new QHBoxLayout( this  ));
    gpuIcon_.reset( new QLabel(  ));
    description_.reset( new QLabel(   ));

    gpuIcon_->setPixmap( icon_ );

    QString description(
                QHostAddress( ip_ ).toString().leftJustified( 15 , ' ' , true )
                + QString(tr("\nuser:%1\n%2(%3)\nMem: %4 (GB)")
                          .arg( username )
                          .arg( deviceInfo.name )
                          .arg( QString::number( deviceInfo.localGPUIndex ))
                          .arg( QString::number( deviceInfo
                                                 .globalMemory / B2GB ))));

    description_->setText( description );
    description_->setAlignment( Qt::AlignLeft );

    layout_->addWidget( gpuIcon_.data( ));
    layout_->addWidget( description_.data( ));

}

//QHBoxLayout *DeviceListItem::getLayout()
//{
//    return layout_.data();
//}

const QPixmap &DeviceListItem::getIcon() const
{
    return icon_ ;
}

const CLRendererDeviceInfo &DeviceListItem::getDeviceInfo() const
{
    return deviceInfo_ ;
}

quint32 DeviceListItem::getIp() const
{
    return ip_;
}

quint32 DeviceListItem::getGPUIndex() const
{
    return gpuIndex_;
}

QString DeviceListItem::getUsername() const
{
    return username_;
}

QLabel *DeviceListItem::getDescription() const
{
    return description_.data( );
}

void DeviceListItem::clearLayout_()
{
    QLayoutItem *item;
    while ((item = layout_->takeAt(0)))
    {
//        delete item->widget();
        delete item;
    }
}
