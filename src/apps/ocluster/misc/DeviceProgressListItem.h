#ifndef DEVICEPROGRESSLISTITEM_H
#define DEVICEPROGRESSLISTITEM_H

#include "DeviceListItem.h"
#include <QProgressBar>

class DeviceProgressListItem : public DeviceListItem
{
    Q_OBJECT
public:
    DeviceProgressListItem( const QPixmap &icon ,
                            const quint32 ip , const QString username,
                            const CLRendererDeviceInfo deviceInfo ,
                            QWidget *parent = 0 );


    void configProgressBar( quint64 steps );

    void setProgressBar( quint64 steps );


Q_SIGNALS:

private:
    QProgressBar *progressBar_ ;

};

#endif // DEVICEPROGRESSLISTITEM_H
