﻿#include "DeviceList.h"

DeviceList::DeviceList( QWidget *centeralWidget )
    : QListWidget( centeralWidget ) ,
      mutex_( QMutex::RecursionMode::Recursive )
{

    setMinimumSize(150, 150);
    //    setAcceptDrops(true);


    connect( this , SIGNAL(doubleClicked( QModelIndex )) ,
             this , SLOT( doubleClickedDevice_SLOT( QModelIndex )));

    QPixmap nvidiaIcon( "://images/nvidia.jpg" );
    QPixmap atiIcon( "://images/ati.jpg" );
    QPixmap intelIcon( "://images/intel.jpg" );
    QPixmap otherIcon( "://images/other.jpg" );



    vendorsIcons_[ Vendor::Nvidia ] =
            nvidiaIcon  .scaled( 50 , 50 , Qt::KeepAspectRatio ) ;
    vendorsIcons_[ Vendor::ATI ]    =
            atiIcon     .scaled( 50 , 50 , Qt::KeepAspectRatio ) ;
    vendorsIcons_[ Vendor::Intel ] =
            intelIcon   .scaled( 50 , 50 , Qt::KeepAspectRatio ) ;
    vendorsIcons_[ Vendor::Other ] =
            otherIcon   .scaled( 50 , 50 , Qt::KeepAspectRatio ) ;

}


bool DeviceList::addDevice( const quint32 ip ,
                            const QString username ,
                            const CLRendererDeviceInfo deviceInfo )
{

    QMutexLocker lock( &mutex_ );

    if( contains( ip , deviceInfo.localGPUIndex ))
        return false ;

    if( isEmpty( ))
        Q_EMIT this->listEmpty_SIGNAL( false );






    QListWidgetItem *item =
            new QListWidgetItem( this );
    DeviceListItem *device =
            new DeviceListItem( vendorIcon_( deviceInfo.vendor ) ,
                                ip , username ,
                                deviceInfo );

    item->setSizeHint( device->getIcon().size() + QSize( 20 , 30 ));

    addItem( item );
    setItemWidget( item , device );

    devices_.insertMulti( ip , device );
    map_[ item ] = device ;

    return true ;
}

void DeviceList::clearDevices()
{
    QMutexLocker lock( &mutex_ );

    clear();
    map_.clear();
    devices_.clear();

    Q_EMIT this->listEmpty_SIGNAL( true );
}

const QMap< quint32 , DeviceListItem *> &DeviceList::getDevices() const
{
    QMutexLocker lock( &mutex_ );

    return devices_ ;
}

bool DeviceList::dropDevice( const quint32 ip, const quint32 index )
{
    QMutexLocker lock( &mutex_ );

    QMap< quint32 , DeviceListItem* >::iterator it = devices_.find( ip );
    while ( it != devices_.end() && it.key() == ip )
    {
        if( it.value()->getDeviceInfo().localGPUIndex == index )
        {
            QListWidgetItem *item = map_.key( it.value( ));
            removeItemWidget( item );
            map_.remove( item );
            devices_.erase( it );
            delete item ;
            Q_EMIT listEmpty_SIGNAL( map_.isEmpty( ));
            return true ;
        }
        ++it;
    }
    return false ;

}

bool DeviceList::contains( const quint32 ip , const quint32 index ) const
{
    QMutexLocker lock( &mutex_ );

    QMap< quint32 , DeviceListItem* >::const_iterator it = devices_.find( ip );
    while ( it != devices_.end() && it.key() == ip )
    {
        if( it.value()->getDeviceInfo().localGPUIndex == index )
            return true ;
        ++it;
    }
    return false ;
}

bool DeviceList::isEmpty() const
{
    QMutexLocker lock( &mutex_ );

    return map_.isEmpty() ;
}

QPixmap DeviceList::vendorIcon_( const QString deviceVendor ) const
{
    DeviceList::Vendor vendor ;

    if( deviceVendor == "NVIDIA Corporation" )
        vendor = DeviceList::Vendor::Nvidia;

    else if( deviceVendor == "Advanced Micro Devices, Inc." )
        vendor = DeviceList::Vendor::ATI;

    else if( deviceVendor == "Intel(R) Corporation"  ||
             deviceVendor == "GenuineIntel" )
        vendor = DeviceList::Vendor::Intel;

    else
        vendor = DeviceList::Vendor::Other ;

    return vendorsIcons_[ vendor ];
}

void DeviceList::doubleClickedDevice_SLOT( QModelIndex index )
{
    Q_EMIT doubleClickedDevice_SIGNAL( map_[ itemFromIndex( index )]);
}
