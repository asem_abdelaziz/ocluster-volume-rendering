#ifndef DEVICELISTITEM_H
#define DEVICELISTITEM_H

#include "CLRendererDeviceInfo.h"
#include <QHostAddress>
#include <QScopedPointer>
#include <QIcon>
#include <QWidget>
#include <QHBoxLayout>
#include <QLabel>
#include <QStackedLayout>

#define B2GB (1024*1024*1024.f)

class DeviceListItem : public QWidget
{
public:
    DeviceListItem( const QPixmap &icon ,
                    const quint32 ip , const QString username,
                    const CLRendererDeviceInfo deviceInfo ,
                    QWidget *parent = 0 );

    //    QHBoxLayout *getLayout();

    const QPixmap &getIcon() const;

    const CLRendererDeviceInfo &getDeviceInfo() const ;

    quint32 getIp() const;

    quint32 getGPUIndex() const;


    QString getUsername() const;


    QLabel *getDescription() const;

protected:
    void clearLayout_( );


protected:

    //    QStackedLayout *stackedLayout_ ;
    QScopedPointer< QHBoxLayout > layout_ ;
    QScopedPointer< QLabel >gpuIcon_ ;
    QScopedPointer< QLabel >description_ ;

    const quint32 ip_ ;
    const quint32 gpuIndex_ ;
    const QPixmap icon_ ;
    const CLRendererDeviceInfo deviceInfo_ ;
    const QString username_ ;

};

#endif // DEVICELISTITEM_H




