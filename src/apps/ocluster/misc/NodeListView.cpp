#include "NodeListView.h"
#include "Logger.h"

NodeListView::NodeListView( QWidget *centeralWidget )
    : QListWidget( centeralWidget ) ,
      nodeIcon_( QPixmap( "://images/terminal5-ssh.jpg" )
                 .scaled( 35 , 35 , Qt::KeepAspectRatio ))
{

    setMinimumSize(150, 100);

    connect( this , SIGNAL(doubleClicked( QModelIndex )) ,
             this , SLOT( doubleClickedNode_SLOT( QModelIndex )));

}

void NodeListView::addNode( const QString name, const quint32 ip )
{
    if( nodes_.keys().contains( ip ))
        return ;

    if( nodes_.isEmpty( ))
        Q_EMIT this->listEmpty_SIGNAL( false );

    QListWidgetItem *item = new QListWidgetItem( this );

    NodeListItemWidget *node =
            new NodeListItemWidget( ip , nodeIcon_ , name );

    node->setAttribute(Qt::WA_DeleteOnClose);
    item->setSizeHint( node->getPicture().size() + QSize( 40 , 20 ) );

    this->addItem( item ) ;
    this->setItemWidget( item , node ) ;
    nodes_[ ip ] = node ;
    map_[ item ] = node ;
}

void NodeListView::clearNodesItem()
{
    clear();
    nodes_.clear();
    map_.clear();

    Q_EMIT this->listEmpty_SIGNAL( true );
}

const QMap<quint32, NodeListItemWidget *> &NodeListView::getNodes() const
{
    return nodes_ ;

}

void NodeListView::dropNode( NodeListItemWidget *node )
{
    quint32 ip = node->getIp();
    QListWidgetItem* item = map_.key( node );


    removeItemWidget( item );
    nodes_.remove( ip );
    map_.remove( item );

    if( nodes_.isEmpty( ))
        Q_EMIT listEmpty_SIGNAL( true );

    delete item;
}

void NodeListView::updateList( const QList<quint32> addressList ,
                               const QString sameName )
{
    clearNodesItem();

    for( const quint32 ip : addressList )
        addNode( sameName , ip );
}

void NodeListView::doubleClickedNode_SLOT( QModelIndex index )
{

    Q_EMIT doubleClickedNode_SIGNAL( map_[ itemFromIndex( index )]);
}
