#ifndef NODELISTVIEW_H
#define NODELISTVIEW_H

#include <QListWidget>
#include <QMap>
#include <QListWidgetItem>
#include "NodeListItemWidget.h"

class NodeListView : public QListWidget
{
    Q_OBJECT
public:
    NodeListView( QWidget *centeralWidget );

    void addNode( const QString name , const quint32 ip );

    void clearNodesItem( );

    const QMap<quint32, NodeListItemWidget *> &getNodes() const;

    void dropNode( NodeListItemWidget* node );

    void updateList( const QList< quint32 > addressList,
                     const QString sameName );

Q_SIGNALS :
    void listEmpty_SIGNAL( bool populated );

    void doubleClickedNode_SIGNAL( NodeListItemWidget* );

private Q_SLOTS:
    void doubleClickedNode_SLOT( QModelIndex index );

private:
    QMap< quint32 , NodeListItemWidget* > nodes_ ;

    QMap< QListWidgetItem* , NodeListItemWidget* > map_ ;

    const QPixmap nodeIcon_ ;
};

#endif // NODELISTVIEW_H

