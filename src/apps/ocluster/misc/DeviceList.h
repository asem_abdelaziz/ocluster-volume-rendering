#ifndef DEVICELIST_H
#define DEVICELIST_H

#include <QListWidget>
#include <QMap>
#include <QListWidgetItem>
#include "DeviceListItem.h"
#include <QMutex>
#include <QMutexLocker>

class DeviceList : public QListWidget
{
    Q_OBJECT


    enum Vendor
    {
        Nvidia ,
        ATI ,
        Intel ,
        Other
    };

public:
    DeviceList( QWidget *centeralWidget );

    virtual bool addDevice( const quint32 ip ,
                            const QString username ,
                            const CLRendererDeviceInfo device );

    virtual void clearDevices( ) ;

    const QMap< quint32 , DeviceListItem* > &getDevices( ) const ;

    virtual bool dropDevice( const quint32 ip , const quint32 index ) ;

    virtual bool contains( const quint32 ip , const quint32 index ) const ;

    virtual bool isEmpty() const ;

Q_SIGNALS:
    void listEmpty_SIGNAL( bool empty );

    void doubleClickedDevice_SIGNAL( DeviceListItem* );

protected:
    QPixmap vendorIcon_( const QString vendor ) const ;

private Q_SLOTS:
    void doubleClickedDevice_SLOT( QModelIndex index );


protected:
    mutable QMutex mutex_ ;

private:
    QMap< quint32 , DeviceListItem* > devices_ ;
    QMap< QListWidgetItem* , DeviceListItem* > map_ ;
    QMap< Vendor , QPixmap > vendorsIcons_ ;
};

#endif // DEVICELIST_H
