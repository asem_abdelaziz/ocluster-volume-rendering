#ifndef DEVICEPROGRESSLIST_H
#define DEVICEPROGRESSLIST_H

#include "DeviceProgressListItem.h"
#include "DeviceList.h"

class DeviceProgressList : public DeviceList
{
    Q_OBJECT
public:
    DeviceProgressList( QWidget *centeralWidget );


    virtual bool addDevice( const quint32 ip ,
                    const QString username ,
                    const CLRendererDeviceInfo device ) final;


    virtual bool contains( const quint32 ip , const quint32 index ) const final ;

    virtual bool isEmpty() const final;

public Q_SLOTS:
    void configProgress_SLOT( quint32 ip , quint32 index , quint64 segments );

    void updateProgress_SLOT( quint32 ip , quint32 index , quint64 segments );

private:
    DeviceProgressListItem* getDeviceProgressItem_( quint32 ip , quint32 index );

private:
    QMap< quint32 , DeviceProgressListItem* > devices_ ;
};

#endif // DEVICEPROGRESSLIST_H
