#include "NodeListItemWidget.h"

NodeListItemWidget::NodeListItemWidget( const quint32 ip ,
                                        const QPixmap picture ,
                                        const QString name ,
                                        QWidget *parent )
    : QWidget( parent ) ,
      ip_( ip ) ,
      picture_( picture ) ,
      name_( name )
{
        layout_ = new QHBoxLayout(  this );
        nodeInfo_ = new QVBoxLayout(  );
        nodeIcon_ = new QLabel( this );
        nameLabel_ = new QLabel( this );
        ipLabel_ = new QLabel( this );

        nodeIcon_->setPixmap( picture_ );

        nameLabel_->setText( QString( tr( "name:" )) +  name_ );
        ipLabel_->setText(QString( QHostAddress( ip_ ).toString( )));
        nodeInfo_->addWidget( nameLabel_ );
        nodeInfo_->addWidget( ipLabel_ );

        layout_->addWidget( nodeIcon_ );
        layout_->addLayout( nodeInfo_ );

}

NodeListItemWidget::~NodeListItemWidget()
{


}

QHBoxLayout *NodeListItemWidget::getLayout()
{
    return layout_;
}

const QPixmap &NodeListItemWidget::getPicture() const
{
    return picture_ ;
}

const QString &NodeListItemWidget::getName() const
{
    return name_ ;
}

quint32 NodeListItemWidget::getIp() const
{
    return ip_ ;
}
