#ifndef NODELISTITEMWIDGET_H
#define NODELISTITEMWIDGET_H

#include <QWidget>
#include <QIcon>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QHostAddress>
#include <memory>
#include <QPainter>
#include <QStyleOption>

class NodeListItemWidget : public QWidget
{
//    Q_OBJECT
public:
    explicit NodeListItemWidget( const quint32 ip ,
                                 const QPixmap picture ,
                                 const QString name  = QString(tr("-")) ,
                                 QWidget *parent = 0 );

    ~NodeListItemWidget( );

    QHBoxLayout *getLayout( );

    const QPixmap &getPicture( ) const ;

    const QString &getName( ) const ;

    quint32 getIp( ) const ;


private:

    QHBoxLayout *layout_ ;

    QVBoxLayout *nodeInfo_ ;

    QLabel *nameLabel_ ;

    QLabel *ipLabel_ ;

    QLabel *nodeIcon_ ;

    const QString name_ ;

    const QPixmap picture_ ;

    const quint32 ip_ ;

};

#endif // NODELISTITEMWIDGET_H




