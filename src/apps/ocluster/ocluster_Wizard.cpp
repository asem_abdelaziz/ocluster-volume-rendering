#include <QApplication>
#include <QDebug>
#include "RenderingWindow.h"
#include "CLusterWizard.h"
#include "CLusterSortLast.h"
#include <QObject>

using namespace ocluster;
using namespace ocluster::Parallel;
using namespace clparen::CLData ;
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);    

    CORE_SERIALIZATION_INIT();
    QMap< quint32 , Node::RendererNode* > clRenderers;
    CLusterSortLast< uchar , float > clusterRenderer(
                clRenderers ,
                FRAME_CHANNEL_ORDER::ORDER_INTENSITY);

    CLusterServer cs( clRenderers , clusterRenderer );

    RenderingWindow mw( cs );

    CLusterWizard *cw = new CLusterWizard( cs );

    QObject::connect( cw , SIGNAL(accepted()) ,
                      &mw , SLOT(show( ))) ;

    QObject::connect( cw , SIGNAL(accepted()) ,
                      & mw , SLOT(initializeConnections_SLOT())) ;

    QObject::connect( cw , SIGNAL(accepted( )) ,
                      cw , SLOT(deleteLater( )));

    cw->show();



    return a.exec();
}
