#include "ScanPage.h"

ScanPage::ScanPage( CLusterServer &clusterServer ,
                    QVariantMap &sharedData ,
                    QWidget *parent )
    : CLusterWizardPage( clusterServer , sharedData , parent )
{

    layout_ = new QHBoxLayout( this );

    detectedNodesList_ = new NodeListView( this );
    selectedNodesList_ = new NodeListView( this );

    selectAllButton_ = new QPushButton( this );
    deselectAllButton_ = new QPushButton( this );

    startScanButton_ = new QPushButton("Scan");

    connect( startScanButton_ , SIGNAL(released()) ,
             this , SLOT(startScan_SLOT( )));

    connect( detectedNodesList_ ,
             SIGNAL( doubleClickedNode_SIGNAL( NodeListItemWidget* )) ,
             this , SLOT( selectNode_SLOT( NodeListItemWidget* )));

    connect( selectedNodesList_ ,
             SIGNAL( doubleClickedNode_SIGNAL( NodeListItemWidget* )) ,
             this , SLOT( deselectNode_SLOT( NodeListItemWidget* )));

    connect( selectAllButton_ , SIGNAL(released()) ,
             this , SLOT(selectAllButtonClicked_SLOT( )));

    connect( deselectAllButton_ , SIGNAL(released()) ,
             this , SLOT(deselectAllButtonClicked_SLOT()));
}

void ScanPage::selectAllButtonClicked_SLOT()
{
    QMap< quint32 , NodeListItemWidget* > detectedNodes =
            detectedNodesList_->getNodes() ;

    for( const quint32 ip : detectedNodes.keys( ))
        selectedNodesList_->addNode( detectedNodes[ ip ]->getName() , ip );

    detectedNodesList_->clearNodesItem( );

    Q_EMIT completeChanged();
}

void ScanPage::deselectAllButtonClicked_SLOT()
{
    QMap< quint32 , NodeListItemWidget* > selectedNodes =
            selectedNodesList_->getNodes() ;
    for( const quint32 ip : selectedNodes.keys( ))
        detectedNodesList_->addNode( selectedNodes[ ip ]->getName() , ip );

    selectedNodesList_->clearNodesItem( );

    Q_EMIT completeChanged();
}

void ScanPage::deselectNode_SLOT( NodeListItemWidget *node )
{


    quint32 ip = node->getIp();
    const QString name = node->getName( );
    selectedNodesList_->dropNode( node );
    detectedNodesList_->addNode( name , ip );

    Q_EMIT completeChanged();

}

void ScanPage::selectNode_SLOT( NodeListItemWidget *node )
{
    quint32 ip = node->getIp();
    const QString name = node->getName( );
    detectedNodesList_->dropNode( node );
    selectedNodesList_->addNode( name , ip );

    Q_EMIT completeChanged();
}


bool ScanPage::isComplete() const
{
    return !selectedNodesList_->getNodes().isEmpty();
}


QVBoxLayout *ScanPage::createNodesList_( NodeListView *list ,
                                                  const QString listLabel ,
                                                  QPushButton *button,
                                                  const QString buttonLabel)
{
    QVBoxLayout *listLayout =  new QVBoxLayout();


    button->setText( buttonLabel );
    button->setEnabled( false );

    listLayout->addWidget( new QLabel( listLabel ));
    listLayout->addWidget( list );
    listLayout->addWidget( button );


    connect( list , SIGNAL(listEmpty_SIGNAL( bool )) ,
             button , SLOT(setDisabled( bool )));

    return listLayout;


}
