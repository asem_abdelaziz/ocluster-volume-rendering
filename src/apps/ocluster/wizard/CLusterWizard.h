#ifndef CLUSTERWIZARD_H
#define CLUSTERWIZARD_H
#include <QWizard>

#include "CLusterServer.h"

#include "CLusterWizardPage.h"
#include "LoadVolumePage.h"
#include "NetworkDiscoverPage.h"
#include "AutomaticScanPage.h"
#include "ManualScanPage.h"
#include "DeviceDeploymentPage.h"
#include "DistributeVolumePage.h"
#include "StartRenderingPage.h"


class CLusterWizard : public QWizard
{
    Q_OBJECT

public:
    enum WizardPages{ PAGE_LoadVolume ,
                      PAGE_NetworkDiscover ,
                      PAGE_AutomaticScan ,
                      PAGE_ManualScan ,
                      PAGE_DeviceDeployment ,
                      PAGE_DistributeVolume ,
                      PAGE_StartRendering };

public:
    CLusterWizard( CLusterServer &clusterServer ,
                   QWidget *parent = 0 );


    ~CLusterWizard( );

    int nextId() const ;


private:
    QMap< WizardPages , QWizardPage* > createPages_( CLusterServer &clusterServer );


private:
    const QMap< WizardPages , QWizardPage* > pages_ ;

    QVariantMap sharedData_ ;

    CLusterServer &clusterServer_ ;


};



#endif // CLUSTERWIZARD_H
