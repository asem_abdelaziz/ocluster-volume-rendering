#ifndef AUTOMATICSCANPAGE_H
#define AUTOMATICSCANPAGE_H
#include "ScanPage.h"

class ManualScanPage ;

class AutomaticScanPage : public ScanPage
{
    Q_OBJECT

public:
    AutomaticScanPage( CLusterServer &clusterServer ,
                       QVariantMap &sharedData ,
                       QWidget *parent = 0 );

    ~AutomaticScanPage();

    void initializePage() override ;

    void startScan_SLOT() override ;

    void remoteNodesVerified_SLOT()  override;

    bool validatePage() override;

public Q_SLOTS:
    void interfaceChoice_SLOT();

protected:
    QVBoxLayout *createScanInterface_( ) ;


private:
    QString username_ ;
    QString password_ ;

    QList< QString > selectedInterfaces_ ;
    QGroupBox *checkBoxesGroup_ ;
    QMap< QString , QCheckBox* > checkBoxes_ ;
};

#endif // AUTOMATICSCANPAGE_H
