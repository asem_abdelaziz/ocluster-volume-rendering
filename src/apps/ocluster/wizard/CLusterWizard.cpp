#include "CLusterWizard.h"

CLusterWizard::CLusterWizard( CLusterServer &clusterServer,
                              QWidget *parent )
    : clusterServer_( clusterServer ) ,
      pages_( createPages_( clusterServer )) ,
      QWizard( parent )
{
    setWizardStyle( WizardStyle::ClassicStyle );

    for( const WizardPages pageId : pages_.keys( ))
        setPage( pageId , pages_[ pageId ]);


}

CLusterWizard::~CLusterWizard()
{
    LOG_DEBUG("Deconstructor");
}

int CLusterWizard::nextId() const
{
    switch( currentId( ))
    {
        case WizardPages::PAGE_LoadVolume :
            return WizardPages::PAGE_NetworkDiscover ;

        case WizardPages::PAGE_NetworkDiscover :
            if( field("manualScan").toBool())
                return WizardPages::PAGE_ManualScan ;
            else
                return WizardPages::PAGE_AutomaticScan ;


        case WizardPages::PAGE_AutomaticScan :
            // fallback to next AutomaticScan label.

        case WizardPages::PAGE_ManualScan :
            return WizardPages::PAGE_DeviceDeployment;


        case WizardPages::PAGE_DeviceDeployment :
            return WizardPages::PAGE_DistributeVolume;

        case WizardPages::PAGE_DistributeVolume :
            return WizardPages::PAGE_StartRendering;

        case WizardPages::PAGE_StartRendering :
            // fallback.

        default :
            return -1 ;

    }
}




QMap< CLusterWizard::WizardPages, QWizardPage *>
CLusterWizard::createPages_( CLusterServer &clusterServer )
{
    QMap< WizardPages , QWizardPage* > pageMap ;

    pageMap[ WizardPages::PAGE_LoadVolume  ]
            = new LoadVolumePage( clusterServer , sharedData_ , this );

    pageMap[ WizardPages::PAGE_NetworkDiscover  ]
            = new NetworkDiscoverPage( clusterServer , sharedData_ , this );

    pageMap[ WizardPages::PAGE_AutomaticScan  ]
            = new AutomaticScanPage( clusterServer , sharedData_ , this );

    pageMap[ WizardPages::PAGE_ManualScan  ]
            = new ManualScanPage( clusterServer , sharedData_ , this );

    pageMap[ WizardPages::PAGE_DeviceDeployment  ]
            = new DeviceDeploymentPage( clusterServer , sharedData_ , this );

    pageMap[ WizardPages::PAGE_DistributeVolume  ]
            = new DistributeVolumePage( clusterServer , sharedData_ , this );

    pageMap[ WizardPages::PAGE_StartRendering  ]
            = new StartRenderingPage( clusterServer , sharedData_ , this );

    return pageMap ;
}

