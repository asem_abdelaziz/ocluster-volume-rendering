#ifndef STARTRENDERINGPAGE_H
#define STARTRENDERINGPAGE_H
#include "CLusterWizardPage.h"



class StartRenderingPage : public CLusterWizardPage
{

public:
    StartRenderingPage( CLusterServer &clusterServer ,
                        QVariantMap &sharedData ,
                        QWidget *parent = 0 );


};

#endif // STARTRENDERINGPAGE_H
