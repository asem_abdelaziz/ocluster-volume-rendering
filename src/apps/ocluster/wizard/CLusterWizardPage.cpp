#include "CLusterWizardPage.h"


CLusterWizardPage::CLusterWizardPage( CLusterServer &clusterServer ,
                                      QVariantMap &sharedData,
                                      QWidget *parent)
    : QWizardPage( parent ) ,
      clusterServer_( clusterServer ),
      sharedData_( sharedData )
{

}
