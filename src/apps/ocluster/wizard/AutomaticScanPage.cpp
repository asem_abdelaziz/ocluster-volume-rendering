#include "AutomaticScanPage.h"

AutomaticScanPage::AutomaticScanPage( CLusterServer &cs ,
                                      QVariantMap &sharedData ,
                                      QWidget *parent )
    : ScanPage( cs , sharedData , parent )
{
    setTitle(tr("Automatic Scan"));
    setSubTitle(tr("Provide the username and password of the registered "
                   "accounts at all machines in the cluster."));


    setCommitPage( true );

    layout_->addLayout( createScanInterface_( ));

    layout_->addLayout( ScanPage::createNodesList_( detectedNodesList_ ,
                                                    tr("Detected Nodes") ,
                                                    selectAllButton_  ,
                                                    tr("Select All" )));

    layout_->addLayout( ScanPage::createNodesList_( selectedNodesList_ ,
                                                    tr("Selected Nodes") ,
                                                    deselectAllButton_  ,
                                                    tr("Deselect All" )));
    setLayout( layout_ );

}


AutomaticScanPage::~AutomaticScanPage()
{

}

void AutomaticScanPage::initializePage( )
{
    username_ = field("username").toString();
    password_ = field("password").toString();

    LOG_DEBUG("Username:%s" ,
              username_.toStdString().c_str());

    LOG_DEBUG("Password:%s" ,
              password_.toStdString().c_str());

    //    QList< quint32 > discoveredIPs;
    //    discoveredIPs << 1 << 0 << 21 << 3 ;

    //    sharedData_["discoveredIPs"] = QVariant::fromValue( discoveredIPs );

    const std::string usernameStd = username_.toStdString();
    const std::string passwordStd = password_.toStdString();

    networkManagerSSH_.reset( new NetworkManagerSSH( usernameStd ,
                                                     passwordStd ,
                                                     DEFAULT_SSH_PORT ,
                                                     MAXIMUM_TASKS ));

    connect( networkManagerSSH_.data( ) , SIGNAL(remoteNodesVerified_SIGNAL( )) ,
             this , SLOT(remoteNodesVerified_SLOT( )));
}


void AutomaticScanPage::startScan_SLOT()
{
    checkBoxesGroup_->setEnabled( false );

    networkManagerSSH_->scanInterface( selectedInterfaces_.takeFirst( ));
}

void AutomaticScanPage::remoteNodesVerified_SLOT()
{
    if( verifiedNodes_ != networkManagerSSH_->getVerfiedRemoteNodes( ))
    {
        verifiedNodes_ = networkManagerSSH_->getVerfiedRemoteNodes();
        detectedNodesList_->updateList( verifiedNodes_.toList() ,
                                        username_ );

    }

    if( !selectedInterfaces_.isEmpty( ))
        networkManagerSSH_->scanInterface( selectedInterfaces_.takeFirst( ));

}

bool  AutomaticScanPage::validatePage()
{
    QMap< quint32 , QString > nodesUsername;
    QMap< quint32 , QString > nodesPassword;

    const QMap< quint32 , NodeListItemWidget* > &selectedNodes =
            selectedNodesList_->getNodes();
    for( const quint32 ip : selectedNodes.keys( ))
    {
        nodesUsername[ ip ] = username_ ;
        nodesPassword[ ip ] = password_ ;
    }

    sharedData_["selectedNodesUsername"] = QVariant::fromValue( nodesUsername );
    sharedData_["selectedNodesPassword"] = QVariant::fromValue( nodesPassword );

    return true;
}

void AutomaticScanPage::interfaceChoice_SLOT()
{
    selectedInterfaces_.clear();
    for( const QString choice : checkBoxes_.keys( ))
        if( checkBoxes_[ choice ]->isChecked( ))
            selectedInterfaces_.append( choice );

    startScanButton_->setDisabled( selectedInterfaces_.isEmpty( ));

}

QVBoxLayout *AutomaticScanPage::createScanInterface_()
{
    checkBoxesGroup_ =
            new QGroupBox(tr("Select Interface(s) to scan"));
    QVBoxLayout *layout =
            new QVBoxLayout();
    QVBoxLayout *checkBoxContainer =
            new QVBoxLayout();

    QMap< QString , quint32 > ifaces = SSHScanner::interfacesIPv4Address();

    for( const QString ifaceLabel : ifaces.keys( ))
        checkBoxes_[ ifaceLabel ] =
                new QCheckBox( ifaceLabel + ":" +
                               QHostAddress( ifaces[ ifaceLabel ]).toString());

    for( QCheckBox *checkBox : checkBoxes_.values( ))
    {
        checkBoxContainer->addWidget( checkBox );
        connect( checkBox , SIGNAL(toggled(bool)) ,
                 this , SLOT(interfaceChoice_SLOT( )));
    }

    startScanButton_->setDisabled( true );
    checkBoxContainer->addWidget( startScanButton_ );
    checkBoxesGroup_->setLayout( checkBoxContainer );
    layout->addWidget( checkBoxesGroup_ );

    return layout;
}
