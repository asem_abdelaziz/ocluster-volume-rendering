#ifndef DISTRIBUTEVOLUMEPAGE_H
#define DISTRIBUTEVOLUMEPAGE_H
#include "CLusterWizardPage.h"
#include "DeviceProgressList.h"
#include "CLDeviceMonitor.h"
#include <QCheckBox>
#include <QPushButton>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>

class DistributeVolumePage : public CLusterWizardPage
{
Q_OBJECT

public:
    DistributeVolumePage( CLusterServer &clusterServer ,
                          QVariantMap &sharedData ,
                          QWidget *parent = 0 );

    void initializePage() override ;

    bool isComplete() const override ;


public Q_SLOTS :
    void deviceDeployed_SLOT( quint32 ip , quint32 index );

    void loadVolumeProgress_SLOT( quint32 ip ,  quint32 index ,
                                  CLDeviceMonitor *monitor );

    void devicesDeployed_SLOT();

    void startDistribution_SLOT();

private:

private:
    QVBoxLayout *layout_ ;
    DeviceProgressList *deployedDevices_ ;
    QMap< quint32 , QString > usernames_ ;
    QCheckBox *copyBricksBeforeSerialization_ ;
    QPushButton *startDistribution_ ;

};

#endif // DISTRIBUTEVOLUMEPAGE_H
