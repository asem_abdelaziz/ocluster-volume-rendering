#include "DeviceDeploymentPage.h"
#include <QDebug>

DeviceDeploymentPage::DeviceDeploymentPage( CLusterServer &cs ,
                                            QVariantMap &sharedData ,
                                            QWidget *parent )
    : CLusterWizardPage( cs , sharedData , parent )
{
    setTitle(tr("Device Deployment"));
    setSubTitle(tr("Select the devices to deploy in rendering process."));



    layout_ = new QHBoxLayout( this );

    availableDevices_ =
            new DeviceList( this );

    selectedDevices_ =
            new DeviceList( this );

    selectAllButton_ =
            new QPushButton(tr( "Select All" ));

    deselectAllButton_ =
            new QPushButton(tr( "Deselect All" ));

    connect( availableDevices_ ,
             SIGNAL(doubleClickedDevice_SIGNAL( DeviceListItem* )) ,
             this , SLOT(selectDevice_SLOT( DeviceListItem* )));

    connect( selectedDevices_ ,
             SIGNAL( doubleClickedDevice_SIGNAL( DeviceListItem* )) ,
             this , SLOT( deselectDevice_SLOT( DeviceListItem* )));

    connect( selectAllButton_ , SIGNAL(released()) ,
             this , SLOT(selectAll_SLOT( )));

    connect( deselectAllButton_ , SIGNAL(released()) ,
             this , SLOT(deselectAll_SLOT( )));

    connect( &clusterServer_ , SIGNAL(rendererNodeInfoReady_SIGNAL( quint32 )),
             this , SLOT( nodeConnected_SLOT( quint32 )));

    setCommitPage( true );
    setButtonText( QWizard::CommitButton , tr("Deploy"));


    layout_->addLayout( createDeviceList_( availableDevices_ ,
                                           tr("Availabel Devices") ,
                                           selectAllButton_  ,
                                           tr("Select All" )));

    layout_->addLayout( createDeviceList_( selectedDevices_ ,
                                           tr("Selected Devices") ,
                                           deselectAllButton_  ,
                                           tr("Deselect All" )));
    setLayout( layout_ );
}

void DeviceDeploymentPage::initializePage()
{
    usernames_ =
            sharedData_["selectedNodesUsername"]
            .value< QMap< quint32 , QString >>() ;

    const QMap< quint32 , QString > passwords =
            sharedData_["selectedNodesPassword"]
            .value< QMap< quint32 , QString >>() ;


    clusterServer_.initializeServer();

    for( const quint32 ip : usernames_.keys( ))
    {
        const QString username = usernames_[ ip ];
        const QString password = passwords[ ip ];

        LOG_DEBUG("[IP:%s][user:%s][pass:%s]" ,
                  QHostAddress( ip ).toString().toStdString().c_str( ) ,
                  username.toStdString().c_str() ,
                  password.toStdString().c_str( ));

        QList< quint32 > ipList = QList< quint32 >() << ip ;
        clusterServer_.fetchRenderingMachines( ipList ,
                                               username ,
                                               password );
    }

    clusterServer_.connectOnlineMachinesToServer( CLIENT_APPLICATION_NAME , usernames_.uniqueKeys( ));
}

bool DeviceDeploymentPage::isComplete() const
{
    return !selectedDevices_->isEmpty();
}

bool DeviceDeploymentPage::validatePage()
{

    const QMap< quint32 , DeviceListItem* > toDeploy =
            selectedDevices_->getDevices( );

    QMap< quint32 , quint32 > toDeployDevices;

    QMap< quint32 , DeviceListItem* >::const_iterator it = toDeploy.begin();

    while( it != toDeploy.end( ))
    {
        toDeployDevices.insertMulti( it.key() ,
                                     it.value()
                                     ->getDeviceInfo().localGPUIndex );
        it++;
    }

    sharedData_["toDeployDevices"] = QVariant::fromValue( toDeployDevices );

    return true ;
}

void DeviceDeploymentPage::selectAll_SLOT()
{
    const QMap< quint32 , DeviceListItem* > availableDevices =
            availableDevices_->getDevices() ;


    QMap< quint32 , DeviceListItem* >::const_iterator it =
            availableDevices.begin();
    while( it != availableDevices.end( ))
    {
        qDebug() << QHostAddress( it.key());
        LOG_DEBUG("Select[IP:%s][name:%s]" ,
                  QHostAddress( it.key()).toString().toStdString().c_str() ,
                  it.value()->getUsername().toStdString().c_str());
        selectedDevices_->addDevice( it.key() , it.value()->getUsername(),
                                     it.value()->getDeviceInfo( ));
        it++;
    }
    LOG_DEBUG("[DONE] Select All");

    availableDevices_->clearDevices();

    Q_EMIT completeChanged();
}

void DeviceDeploymentPage::deselectAll_SLOT()
{
    const QMap< quint32 , DeviceListItem* > &availableDevices =
            selectedDevices_->getDevices() ;

    QMap< quint32 , DeviceListItem* >::const_iterator it =
            availableDevices.begin();
    while( it != availableDevices.end( ))
    {
        availableDevices_->addDevice( it.key() , it.value()->getUsername() ,
                                      it.value()->getDeviceInfo( ));
        it++;
    }

    selectedDevices_->clearDevices();

    Q_EMIT completeChanged();

}

void DeviceDeploymentPage::deselectDevice_SLOT( DeviceListItem *device )
{
    const quint32 ip = device->getIp();
    const quint32 index = device->getGPUIndex();

    availableDevices_->addDevice( ip , device->getUsername() ,
                                  device->getDeviceInfo( )) ;


    selectedDevices_->dropDevice( ip , index );

    Q_EMIT completeChanged();
}

void DeviceDeploymentPage::selectDevice_SLOT( DeviceListItem *device )
{
    const quint32 ip = device->getIp();
    const quint32 index = device->getGPUIndex();

    selectedDevices_->addDevice( ip , device->getUsername() ,
                                 device->getDeviceInfo( )) ;


    availableDevices_->dropDevice( ip , index );

    Q_EMIT completeChanged();
}

void DeviceDeploymentPage::nodeConnected_SLOT( quint32 ip )
{

    const CLRendererNodeInfo &nodeInfo =
            clusterServer_.getCLRenderers()[ ip ]->getNodeInfo();

    for( const CLRendererDeviceInfo &deviceInfo :
         nodeInfo.getRendererDevicesInfo( ))
        availableDevices_->addDevice( ip , usernames_[ ip ] ,
                                      deviceInfo );


}

QVBoxLayout *DeviceDeploymentPage::createDeviceList_( DeviceList *list ,
                                                      const QString listLabel ,
                                                      QPushButton *button ,
                                                      const QString buttonLabel )
{
    QVBoxLayout *listLayout =  new QVBoxLayout();


    button->setText( buttonLabel );
    button->setEnabled( false );

    listLayout->addWidget( new QLabel( listLabel ));
    listLayout->addWidget( list );
    listLayout->addWidget( button );


    connect( list , SIGNAL(listEmpty_SIGNAL( bool )) ,
             button , SLOT(setDisabled( bool )));

    return listLayout;
}

