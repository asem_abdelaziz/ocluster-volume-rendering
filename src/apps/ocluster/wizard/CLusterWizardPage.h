#ifndef CLUSTERWIZARDPAGE_H
#define CLUSTERWIZARDPAGE_H

#include <QWizardPage>
#include <CLusterServer.h>
#include <QVariantMap>

#include "Logger.h"

// Forward declaration: to be a friend class.
class CLusterWizrd ;

using namespace ocluster;

class CLusterWizardPage : public QWizardPage
{
    Q_OBJECT

public:
    CLusterWizardPage( CLusterServer &clusterServer ,
                       QVariantMap &sharedData ,
                       QWidget *parent = 0  );


protected :
    CLusterServer &clusterServer_ ;

    QVariantMap &sharedData_ ;

private:
    friend class CLusterWizrd ;

};




#endif // CLUSTERWIZARDPAGE_H
