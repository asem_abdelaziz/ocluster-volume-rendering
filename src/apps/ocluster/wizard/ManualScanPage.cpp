#include "ManualScanPage.h"

ManualScanPage::ManualScanPage( CLusterServer &cs ,
                                QVariantMap &sharedData ,
                                QWidget *parent )
    : ScanPage( cs , sharedData , parent )
{
    setTitle(tr("Manual Scan"));
    setSubTitle(tr("Fetch remote nodes manually by specifying address,"
                   " username, and password."));


    setCommitPage( true );
    networkManagerSSH_.reset( new NetworkManagerSSH( "" ,
                                                     "" ,
                                                     DEFAULT_SSH_PORT ,
                                                     MAXIMUM_TASKS ));

    connect( networkManagerSSH_.data( ) , SIGNAL(remoteNodesVerified_SIGNAL( )) ,
             this , SLOT(remoteNodesVerified_SLOT( )));


    layout_->addLayout( createScanInterface_( ));

    layout_->addLayout( ScanPage::createNodesList_( detectedNodesList_ ,
                                                    tr("Detected Nodes") ,
                                                    selectAllButton_  ,
                                                    tr("Select All" )));

    layout_->addLayout( ScanPage::createNodesList_( selectedNodesList_ ,
                                                    tr("Selected Nodes") ,
                                                    deselectAllButton_  ,
                                                    tr("Deselect All" )));
    setLayout( layout_ );

}

void ManualScanPage::startScan_SLOT()
{
    const quint32 ip =
            QHostAddress( addressInput_->text()).toIPv4Address();

    nodesNames_[ ip ] = usernameInput_->text() ;
    nodesPassowrds_[ ip ] = passwordInput_->text();

    networkManagerSSH_->verifyRemoteNode( ip , nodesNames_[ ip ].toStdString() ,
                                          nodesPassowrds_[ ip ].toStdString( ));
}

void ManualScanPage::remoteNodesVerified_SLOT()
{
    if( verifiedNodes_ != networkManagerSSH_->getVerfiedRemoteNodes( ))
    {
        QSet< quint32 > delta =
                networkManagerSSH_->getVerfiedRemoteNodes() - verifiedNodes_ ;

        for( quint32 newIP : delta )
            detectedNodesList_->addNode( nodesNames_[ newIP ] , newIP );
    }

    verifiedNodes_ = networkManagerSSH_->getVerfiedRemoteNodes() ;

}

void ManualScanPage::initializePage()
{

}

bool ManualScanPage::validatePage()
{
    QMap< quint32 , QString > nodesUsername;
    QMap< quint32 , QString > nodesPassword;

    const QMap< quint32 , NodeListItemWidget* > &selectedNodes =
            selectedNodesList_->getNodes();
    for( const quint32 ip : selectedNodes.keys( ))
    {
        nodesUsername[ ip ] = nodesNames_[ ip ] ;
        nodesPassword[ ip ] = nodesPassowrds_[ ip ] ;
    }

    sharedData_["selectedNodesUsername"] = QVariant::fromValue( nodesUsername );
    sharedData_["selectedNodesPassword"] = QVariant::fromValue( nodesPassword );

    return true;

}

void ManualScanPage::inputChanged_SLOT()
{
    Q_EMIT scanReady_SIGNAL(
                // check address field is not empty.
                !addressInput_->text().isEmpty() &&

                // check address field is valid IPV4
                QHostAddress( addressInput_->text()).protocol()
                == QAbstractSocket::IPv4Protocol   &&

                // check username field is not empty.
                !usernameInput_->text().isEmpty() &&

                // check password field is not empty.
                !passwordInput_->text().isEmpty( ));
}

QVBoxLayout *ManualScanPage::createScanInterface_()
{
    scanForm_ =
            new QGroupBox(tr("Enter remote node credentials."));

    QVBoxLayout *layout =
            new QVBoxLayout();

    QVBoxLayout *inputs =
            new QVBoxLayout();

    // Address
    addressInput_ =
            new QLineEdit(tr(""));
    addressInput_->setMaxLength(  15 /* IPV4: 4*3(fields) + 3(dots) */ );
    QLabel *addressLabel =
            new QLabel(tr("Address:"));
    addressLabel->setAlignment( Qt::AlignRight );
    QHBoxLayout *address =
            new QHBoxLayout();
    address->addWidget( addressLabel );
    address->addWidget( addressInput_ );
    inputs->addLayout( address );
    connect( addressInput_ , SIGNAL( textEdited( QString )) ,
             this , SLOT( inputChanged_SLOT( )));

    // Username
    usernameInput_ =
            new QLineEdit(tr("username"));
    QLabel *usernameLabel =
            new QLabel(tr("Username:"));
    usernameLabel->setAlignment( Qt::AlignRight );
    QHBoxLayout *username =
            new QHBoxLayout();
    username->addWidget( usernameLabel );
    username->addWidget( usernameInput_ );
    inputs->addLayout( username );
    connect( usernameInput_ , SIGNAL( textEdited( QString )) ,
             this , SLOT( inputChanged_SLOT( )));

    // Password
    passwordInput_ =
            new QLineEdit(tr("password"));
    passwordInput_->setEchoMode( QLineEdit::PasswordEchoOnEdit );
    QLabel *passwordLabel =
            new QLabel(tr("Password:"));
    passwordLabel->setAlignment( Qt::AlignRight );
    QHBoxLayout *password =
            new QHBoxLayout();
    password->addWidget( passwordLabel );
    password->addWidget( passwordInput_ );
    inputs->addLayout( password );
    connect( passwordInput_ , SIGNAL( textEdited( QString )) ,
             this , SLOT( inputChanged_SLOT( )));

    // Scan Button
    startScanButton_->setText(tr("Knock"));
    this->startScanButton_->setEnabled( false );
    inputs->addWidget( startScanButton_ );

    connect( this , SIGNAL( scanReady_SIGNAL( bool )) ,
             startScanButton_ , SLOT( setEnabled( bool )));

    scanForm_->setLayout( inputs );

    layout->addWidget( scanForm_ );


    ////////////////////
    inputChanged_SLOT();
    return layout ;
}
