#ifndef LOADVOLUMEPAGE_H
#define LOADVOLUMEPAGE_H
#include "CLusterWizardPage.h"
#include <QLineEdit>
#include <QLabel>
#include <QRadioButton>
#include <QCheckBox>
#include <QFileDialog>
#include <QFile>
#include <QVBoxLayout>
#include <QPushButton>

class LoadVolumePage : public CLusterWizardPage
{
    Q_OBJECT

public:
    LoadVolumePage( CLusterServer &clusterServer ,
                    QVariantMap &sharedData ,
                    QWidget *parent = 0 );



    bool isComplete() const override ;

    void initializePage() override ;

public Q_SLOTS:
    void loadVolume_SLOT();

private:
    QLabel *loadVolumeLabel_ ;
    QLineEdit *loadVolumePath_ ;
    QPushButton *loadVolumeButton_ ;
    QCheckBox *memoryMapCheckBox_ ;

    QLabel *volumeSizeGB_ ;
    QLabel *volumeDimensions_ ;

    bool volumeLoaded_ ;
};

#endif // LOADVOLUMEPAGE_H
