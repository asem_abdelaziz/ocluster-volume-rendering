#include "DistributeVolumePage.h"

DistributeVolumePage::DistributeVolumePage( CLusterServer &cs ,
                                            QVariantMap &sharedData ,
                                            QWidget *parent )
    : CLusterWizardPage( cs , sharedData , parent )
{
    setTitle(tr("Distribute the volume over the cluster"));
    setSubTitle(tr("Distribution of big volumes may last for"
                   " serveral minutes."));


    connect( & clusterServer_.getCLusterRenderer() ,
             SIGNAL( deviceDeployed_SIGNAL( quint32 , quint32 )) ,
             this , SLOT( deviceDeployed_SLOT( quint32 , quint32 )));

    connect( & clusterServer_.getCLusterRenderer() ,
             SIGNAL( clusterReady_SIGNAL( bool )) ,
             this , SIGNAL( completeChanged( )));


    deployedDevices_ = new DeviceProgressList( this );
    layout_ = new QVBoxLayout( );

    QHBoxLayout *distributionSetting = new QHBoxLayout();
    copyBricksBeforeSerialization_ = new QCheckBox(tr("Copy bricks before serialization (Less processing time "
                                                      "yet more memory occupation). "));
    startDistribution_ = new QPushButton(tr("Start distribution."));
    distributionSetting->addWidget( copyBricksBeforeSerialization_ );
    distributionSetting->addWidget( startDistribution_ );


    startDistribution_->setEnabled( false );
    copyBricksBeforeSerialization_->setEnabled( false );
    layout_->addLayout( distributionSetting );
    layout_->addWidget( deployedDevices_ );

    setLayout( layout_ );


}

void DistributeVolumePage::initializePage()
{

    const QMap< quint32 , quint32 > toDeployDevices =
            sharedData_["toDeployDevices"].value< QMap< quint32 , quint32 >>();


    QMap< quint32 , quint32 >::const_iterator it = toDeployDevices.begin();
    while( it != toDeployDevices.end( ))
    {
        LOG_DEBUG("Deploy[%s:%d]",
                  QHostAddress( it.key()).toString().toStdString().c_str() ,
                  it.value( ));
        it++;
    }

    clusterServer_.getCLusterRenderer()
            .initializeRendering( toDeployDevices , false  );

    usernames_ =
            sharedData_["selectedNodesUsername"]
            .value< QMap< quint32 , QString >>() ;


//    connect( &clusterServer_.getCLusterRenderer() ,
//             SIGNAL(devicesDeployed_SIGNAL(bool)) ,
//             this , SLOT(devicesDeployed_SLOT()));

    connect( &clusterServer_.getCLusterRenderer() ,
             SIGNAL(devicesDeployed_SIGNAL(bool)) ,
             startDistribution_ , SLOT(setEnabled(bool)));

    connect( &clusterServer_.getCLusterRenderer() ,
             SIGNAL(devicesDeployed_SIGNAL(bool)) ,
             copyBricksBeforeSerialization_ ,
             SLOT(setEnabled(bool)));



    connect( startDistribution_ , SIGNAL(released()) ,
             this , SLOT(startDistribution_SLOT()));

}

bool DistributeVolumePage::isComplete() const
{
    return clusterServer_.getCLusterRenderer().clusterReady();
}

void DistributeVolumePage::deviceDeployed_SLOT( quint32 ip , quint32 index )
{
    const CLRendererNodeInfo &nodeInfo =
            clusterServer_.getCLRenderers()[ ip ]->getNodeInfo();

    const CLRendererDeviceInfo &deviceInfo  =
            nodeInfo.getRendererDevicesInfo()[ index ];

    deployedDevices_->addDevice( ip , usernames_[ ip ] ,
                                 deviceInfo );


    connect( clusterServer_.getCLRenderers()[ ip ] ,
             SIGNAL(loadVolumeProgress_SIGNAL(quint32,quint32,CLDeviceMonitor*)),
             this ,
             SLOT(loadVolumeProgress_SLOT(quint32, quint32, CLDeviceMonitor* )),
             Qt::QueuedConnection );

}

void DistributeVolumePage::loadVolumeProgress_SLOT( quint32 ip, quint32 index,
                                                    CLDeviceMonitor *monitor )
{

    quint64 segmentsSent = monitor->segmentsSent ;

    if( segmentsSent == 0 )
        deployedDevices_->configProgress_SLOT( ip , index ,
                                               monitor->segmentsCount );
    else
        deployedDevices_->updateProgress_SLOT( ip , index ,
                                               monitor->segmentsSent );

}

void DistributeVolumePage::devicesDeployed_SLOT()
{


}

void DistributeVolumePage::startDistribution_SLOT()
{

    startDistribution_->setDisabled( true );
    copyBricksBeforeSerialization_->setDisabled( true );

//    QtConcurrent::run( &clusterServer_.getCLusterRenderer() ,
//                       &CLusterRenderer::distributeVolumeMemoryWeighted ,
//                       clusterServer_.getCLusterRenderer().getDeployedDevices() ,
//                       copyBricksBeforeSerialization_->isChecked( ));

    clusterServer_.getCLusterRenderer().
            distributeVolumeMemoryWeighted(
                clusterServer_.getCLusterRenderer().getDeployedDevices()  ,
                copyBricksBeforeSerialization_->isChecked( ));
}

