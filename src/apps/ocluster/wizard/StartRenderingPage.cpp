#include "StartRenderingPage.h"

StartRenderingPage::StartRenderingPage( CLusterServer &cs ,
                                        QVariantMap &sharedData ,
                                        QWidget *parent )
      : CLusterWizardPage( cs , sharedData , parent )
{
    setTitle(tr("Start rendering"));
    setSubTitle(tr("Now proceed to start rendering."));
}

