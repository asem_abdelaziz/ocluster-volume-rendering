#ifndef SCANPAGE_H
#define SCANPAGE_H

#include "CLusterServer.h"
#include "NetworkManagerSSH.h"
#include "CLusterWizardPage.h"
#include "NodeListView.h"

#include <QScopedPointer>
#include <QVBoxLayout>
#include <QPushButton>
#include <QGroupBox>
#include <QCheckBox>

class ScanPage : public CLusterWizardPage
{
    Q_OBJECT

public:
    ScanPage( CLusterServer &clusterServer ,
              QVariantMap &sharedData ,
              QWidget *parent = 0  );


public Q_SLOTS:
    void selectAllButtonClicked_SLOT( ) ;

    void deselectAllButtonClicked_SLOT( ) ;

    void deselectNode_SLOT( NodeListItemWidget* node ) ;

    void selectNode_SLOT( NodeListItemWidget* node ) ;

    virtual void remoteNodesVerified_SLOT( )  = 0 ;

    virtual bool isComplete() const override ;

    virtual void startScan_SLOT()  = 0 ;

protected:
    static QVBoxLayout *createNodesList_( NodeListView *list ,
                                          const QString listLabel ,
                                          QPushButton *button ,
                                          const QString buttonLabel );

protected:
    QHBoxLayout *layout_ ;

    NodeListView *detectedNodesList_;
    NodeListView *selectedNodesList_;

    QPushButton *selectAllButton_ ;
    QPushButton *deselectAllButton_ ;

    QScopedPointer< NetworkManagerSSH > networkManagerSSH_ ;
    QSet< quint32 > verifiedNodes_ ;
    QPushButton *startScanButton_;
};

#endif // SCANPAGE_H
