#ifndef MANUALSCANPAGE_H
#define MANUALSCANPAGE_H
#include "ScanPage.h"
#include <QMap>
#include <QLineEdit>

class ManualScanPage : public ScanPage
{
    Q_OBJECT
public:
    ManualScanPage( CLusterServer &clusterServer ,
                    QVariantMap &sharedData ,
                    QWidget *parent = 0 );


    void startScan_SLOT() override;

    void remoteNodesVerified_SLOT() override ;

    void initializePage() override ;

    bool validatePage() override ;
Q_SIGNALS:
    void scanReady_SIGNAL( bool );

private Q_SLOTS:
    void inputChanged_SLOT();

private:
    QVBoxLayout *createScanInterface_();


private:
    QMap< quint32 , QString > nodesNames_ ;
    QMap< quint32 , QString > nodesPassowrds_ ;

    QLineEdit *addressInput_ ;
    QLineEdit *usernameInput_ ;
    QLineEdit *passwordInput_ ;

    QGroupBox *scanForm_ ;

};

#endif // MANUALSCANPAGE_H
