#ifndef DEVICEDEPLOYMENTPAGE_H
#define DEVICEDEPLOYMENTPAGE_H
#include <CLusterWizardPage.h>
#include "DeviceList.h"
#include "ClientApplicationConfig.hh"
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>


// Forward declaration: to be a friend class.
class CLusterWizrd ;

class DeviceDeploymentPage : public CLusterWizardPage
{
    Q_OBJECT

public:
    DeviceDeploymentPage( CLusterServer &clusterServer ,
                          QVariantMap &sharedData ,
                          QWidget *parent = 0 );

    void initializePage() override ;

    bool isComplete() const override ;

    bool validatePage() override ;
public Q_SLOTS:

    void selectAll_SLOT( ) ;

    void deselectAll_SLOT( ) ;

    void deselectDevice_SLOT( DeviceListItem* device ) ;

    void selectDevice_SLOT( DeviceListItem* device ) ;

    void nodeConnected_SLOT( quint32 ip );

private:
    static QVBoxLayout *createDeviceList_( DeviceList *list ,
                                           const QString listLabel ,
                                           QPushButton *button ,
                                           const QString buttonLabel );

private:
    QHBoxLayout *layout_ ;

    DeviceList *availableDevices_ ;
    DeviceList *selectedDevices_ ;

    QPushButton *selectAllButton_ ;
    QPushButton *deselectAllButton_ ;

    QMap< quint32 , QString > usernames_ ;
};

#endif // DEVICEDEPLOYMENTPAGE_H
