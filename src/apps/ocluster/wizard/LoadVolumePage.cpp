#include "LoadVolumePage.h"
#include <QDebug>

LoadVolumePage::LoadVolumePage( CLusterServer &cs ,
                                QVariantMap &sharedData ,
                                QWidget *parent )
    : CLusterWizardPage( cs , sharedData , parent )
{
    setTitle(tr( "Load Volume" ));
    setSubTitle( tr( "Select volume file (.img)." ));


    setCommitPage( true );
    setButtonText( QWizard::CommitButton , tr("Next"));


    QHBoxLayout *loadVolume = new QHBoxLayout( );
    loadVolumeLabel_ = new QLabel( tr( "Select Volume File (.img):" ));
    loadVolumePath_ = new QLineEdit();
    loadVolumeButton_ = new QPushButton( tr( "Browse" ));
    loadVolume->addWidget( loadVolumeLabel_ );
    loadVolume->addWidget( loadVolumePath_ );
    loadVolume->addWidget( loadVolumeButton_ );

    memoryMapCheckBox_ = new QCheckBox(tr("Memory-map the volume."));

    QVBoxLayout *layout = new QVBoxLayout();

    volumeSizeGB_ = new QLabel();
    volumeDimensions_ = new QLabel();

    layout->addWidget( memoryMapCheckBox_ );
    layout->addLayout( loadVolume );
    layout->addWidget( volumeSizeGB_ );
    layout->addWidget( volumeDimensions_ );

    setLayout( layout );


    loadVolumePath_->setEnabled( false );

    connect( loadVolumeButton_ , SIGNAL(released( )) ,
             this , SLOT(loadVolume_SLOT( )));

    volumeLoaded_ = false ;
}

bool LoadVolumePage::isComplete() const
{
    return volumeLoaded_ ;
}

void LoadVolumePage::initializePage()
{

    Q_EMIT completeChanged();
}

void LoadVolumePage::loadVolume_SLOT()
{
    QFileDialog dialog;
    dialog.setFileMode( QFileDialog::ExistingFile );
    dialog.setNameFilter( tr( "Volume (*.img)" ));
    dialog.setViewMode( QFileDialog::Detail );
    dialog.setDirectory( "/" );

    QString filePath;
    float fileSizeGB;

    if( dialog.exec())
        filePath = dialog.selectedFiles().first();
    else
        return ;

    loadVolumePath_->setText( filePath );
    loadVolumeButton_->setEnabled( false );
    memoryMapCheckBox_->setEnabled( false );


    QFile file(filePath);
    if( file.open(QIODevice::ReadOnly))
        fileSizeGB =  file.size() /  1024 / 1024.f / 1024;


    volumeSizeGB_->setText(tr("Size (GB): %1").arg( fileSizeGB ));


    filePath.chop(QString(".img").length());


    clusterServer_.getCLusterRenderer().
            loadBaseVolume( filePath.toStdString( ) ,
                            memoryMapCheckBox_->isChecked( ));

    const Dimensions3D d =
            clusterServer_.getCLusterRenderer().getVolumeDimensions();
    volumeDimensions_->setText(tr("Dimensions: %1X%2X%3")
                               .arg( d.x ).arg( d.y ).arg( d.z ));


    volumeLoaded_ = true ;
    Q_EMIT completeChanged();
}

