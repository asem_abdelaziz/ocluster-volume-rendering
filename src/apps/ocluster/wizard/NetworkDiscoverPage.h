#ifndef NETWORKDISCOVERPAGE_H
#define NETWORKDISCOVERPAGE_H
#include "CLusterWizardPage.h"
#include <QRadioButton>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include <QButtonGroup>
#include <QLabel>
#include <QLineEdit>

class NetworkDiscoverPage : public CLusterWizardPage
{

public:
    NetworkDiscoverPage( CLusterServer &clusterServer ,
                         QVariantMap &sharedData ,
                         QWidget *parent = 0 );



    ~NetworkDiscoverPage();


    bool isComplete() const override;

private:


    // Automatic scan
    QRadioButton *automaticScan_ ;
    QLabel *automaticScanDescription_ ;

    // Automatic scan:username
    QLabel *usernameLabel_ ;
    QLineEdit *usernameInput_ ;
    QHBoxLayout *username_ ;

    // Automatic scan:password
    QLabel *passwordLabel_ ;
    QLineEdit *passwordInput_ ;
    QHBoxLayout *password_ ;

    // Manual scan
    QRadioButton *manualScan_ ;

    QLabel *manualScanDescription_ ;
};

#endif // NETWORKDISCOVERPAGE_H
