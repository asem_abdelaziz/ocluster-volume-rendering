#include "NetworkDiscoverPage.h"

NetworkDiscoverPage::NetworkDiscoverPage( CLusterServer &cs ,
                                          QVariantMap &sharedData ,
                                          QWidget *parent )
        : CLusterWizardPage( cs , sharedData , parent )
{
    setTitle(tr("Network scan options"));
    setSubTitle(tr("Choose whether to perform an automated (brute force) "
                   "network scan over the cluster with given credentials or"
                   " perform a manual node by node."));

    setButtonText( QWizard::NextButton , tr( "Scan" ));

    // Automatic scan
    automaticScan_ =
            new QRadioButton(tr("Automatic Scan"));

    automaticScanDescription_ =
            new QLabel(tr("Perform Automatic Scan."));

    // Automatic scan: Username input
    QHBoxLayout *username = new QHBoxLayout( );
    usernameLabel_ = new QLabel(tr("username:"));
    usernameInput_ = new QLineEdit( );
    username->addWidget( usernameLabel_ );
    username->addWidget( usernameInput_ );

    // Automatic scan: Password
    passwordLabel_ = new QLabel(tr("password:"));
    passwordInput_ = new QLineEdit( );
    passwordInput_->setEchoMode( QLineEdit::PasswordEchoOnEdit );
    QHBoxLayout *password = new QHBoxLayout(  );
    password->addWidget( passwordLabel_ );
    password->addWidget( passwordInput_ );

    // Manual scan
    manualScan_ =
            new QRadioButton(tr("Manual Scan"));

    manualScanDescription_ =
            new QLabel(tr("Perform Manual Scan."));


    QVBoxLayout *layout =
            new QVBoxLayout(  );

    layout->addWidget( automaticScan_ );
    layout->addWidget( automaticScanDescription_ );
    layout->addLayout( username );
    layout->addLayout( password );

    layout->addWidget( manualScan_ );
    layout->addWidget( manualScanDescription_ );

    setLayout( layout );


    registerField("automaticScan" , automaticScan_ );
    registerField("manualScan" , manualScan_ );
    registerField("username" , usernameInput_ );
    registerField("password" , passwordInput_ );

    connect( automaticScan_ , SIGNAL( toggled( bool )) ,
             usernameInput_ , SLOT( setEnabled( bool )));

    connect( automaticScan_ , SIGNAL( toggled( bool )) ,
             passwordInput_ , SLOT( setEnabled( bool )));

    connect( automaticScan_ , SIGNAL(toggled( bool )) ,
             this , SIGNAL(completeChanged( )));

    connect( manualScan_ , SIGNAL(toggled( bool )) ,
             this , SIGNAL(completeChanged( )));

    connect( usernameInput_ , SIGNAL(textChanged(QString)) ,
             this , SIGNAL(completeChanged( )));

    connect( passwordInput_ , SIGNAL(textChanged(QString)) ,
             this , SIGNAL(completeChanged( )));

    usernameInput_->setEnabled( false );
    passwordInput_->setEnabled( false );
}

NetworkDiscoverPage::~NetworkDiscoverPage()
{


}

bool NetworkDiscoverPage::isComplete() const
{
    if(  !automaticScan_->isChecked() &&
         !manualScan_->isChecked( ))
        return false ;

    if( automaticScan_->isChecked( ) &&
        ( usernameInput_->text().isEmpty() ||
          passwordInput_->text().isEmpty( )))
        return false ;

    return true ;

}

