# Configure ClienApplicationConfig.h file
set( CLIENT_APP "${RENDERING_END_RUNSCRIPT_PATH}" )
configure_file( ${CLIENT_APPLICATION_CONFIG_RAW} ${CMAKE_CURRENT_SOURCE_DIR}/${CLIENT_APPLICATION_CONFIG} @ONLY)


# Application name
set( OCLUSTER_APP ocluster_Wizard )

# Application source/header/ui files
set( OCLUSTER_APP_SOURCES ocluster_Wizard.cpp
    RenderingWindow.cpp
    MouseNavigator.cpp
    wizard/CLusterWizard.cpp
    wizard/LoadVolumePage.cpp
    wizard/NetworkDiscoverPage.cpp
    wizard/AutomaticScanPage.cpp
    wizard/ManualScanPage.cpp
    wizard/DeviceDeploymentPage.cpp
    wizard/DistributeVolumePage.cpp
    wizard/StartRenderingPage.cpp
    wizard/CLusterWizardPage.cpp
    wizard/ScanPage.cpp
    misc/NodeListItemWidget.cpp
    misc/NodeListView.cpp
    misc/DeviceList.cpp
    misc/DeviceListItem.cpp
    misc/DeviceProgressList.cpp
    misc/DeviceProgressListItem.cpp )


set( OCLUSTER_APP_HEADERS  RenderingWindow.h
    MouseNavigator.h
    wizard/CLusterWizard.h
    wizard/LoadVolumePage.h
    wizard/NetworkDiscoverPage.h
    wizard/AutomaticScanPage.h
    wizard/ManualScanPage.h
    wizard/DeviceDeploymentPage.h
    wizard/DistributeVolumePage.h
    wizard/StartRenderingPage.h
    wizard/CLusterWizardPage.h
    wizard/ScanPage.h
    misc/NodeListItemWidget.h
    misc/NodeListView.h
    misc/DeviceList.h
    misc/DeviceListItem.h
    misc/DeviceProgressList.h
    misc/DeviceProgressListItem.h
    ${CLIENT_APPLICATION_CONFIG})


set( OCLUSTER_APP_FORMS  RenderingWindow.ui )


set( OCLUSTER_APP_RESOURCES images.qrc )

qt5_wrap_ui( OCLUSTER_APP_FORMS_MOC ${OCLUSTER_VR_APP_FORMS} )


INCLUDE_DIRECTORIES( wizard )
INCLUDE_DIRECTORIES( misc )

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

# Generate the executables
add_executable( ${OCLUSTER_APP}
    ${OCLUSTER_APP_SOURCES}
    ${OCLUSTER_APP_HEADERS}
    ${OCLUSTER_APP_FORMS}
    ${OCLUSTER_APP_FORMS_MOC}
    ${OCLUSTER_APP_RESOURCES}
    )

# Link the application against the GLDPVR library
target_link_libraries( ${OCLUSTER_APP} ${OCLUSTER_LIB} )

# Make sure to use the required Qt modules
qt5_use_modules( ${OCLUSTER_APP} Core  Gui Widgets Network)

# Install the executable (generate with 'make install' in the bin directory)
install( TARGETS ${OCLUSTER_APP} DESTINATION bin )


