#include "RenderingWindow.h"
#include "ui_RenderingWindow.h"
#include "Logger.h"
#include "MouseNavigator.h"
RenderingWindow::RenderingWindow( ocluster::CLusterServer &clusterServer ,
                                  QWidget *parent )
    : QMainWindow( parent ),
      ui( new Ui::RenderingWindow ) ,
      clusterServer_( clusterServer )
{

    // UI setup
    ui->setupUi( this );
    //Enable xray kernel by default
    ui->xrayButton->setChecked(true);
    //disable transformations and transfer fun
    // disable iso value slider if iso surface kernel is not selected
    ui->isoValueSlider->setEnabled(false);


}


RenderingWindow::~RenderingWindow( )
{
    delete ui;
}

void RenderingWindow::initializeConnections_()
{

    //sliders
    connect( ui->xRotationSlider , SIGNAL( valueChanged( int )),
             this , SLOT( newXRotation_SLOT( int )));

    connect( ui->yRotationSlider , SIGNAL( valueChanged( int )),
             this , SLOT( newYRotation_SLOT( int )));

    connect( ui->zRotationSlider , SIGNAL( valueChanged( int )),
             this , SLOT( newZRotation_SLOT( int )));

    connect( ui->xTranslationSlider , SIGNAL( valueChanged( int )),
             this , SLOT( newXTranslation_SLOT( int )));

    connect( ui->yTranslationSlider , SIGNAL( valueChanged( int )),
             this , SLOT( newYTranslation_SLOT( int )));

    connect( ui->zTranslationSlider , SIGNAL( valueChanged( int )),
             this , SLOT( newZTranslation_SLOT( int )));

    connect( ui->xScalingSlider , SIGNAL( valueChanged( int )),
             this , SLOT( newXScaling_SLOT( int )));

    connect( ui->yScalingSlider , SIGNAL( valueChanged( int )),
             this , SLOT( newYScaling_SLOT( int )));

    connect( ui->zScalingSlider , SIGNAL( valueChanged( int )),
             this , SLOT( newZScaling_SLOT( int )));

    connect( ui->xyzScalingSlider , SIGNAL( valueChanged( int )),
             this , SLOT( newXYZScaling_SLOT( int )));

    connect( ui->brightnessSlider , SIGNAL( valueChanged( int )),
             this , SLOT( newBrightness_SLOT( int )));

    connect( ui->densitySlider , SIGNAL( valueChanged( int )),
             this , SLOT( newDensity_SLOT( int )));


    connect( ui->isoValueSlider , SIGNAL( valueChanged( int )),
             this , SLOT( newIsoValue_SLOT( int )));


    connect( & clusterServer_.getCLusterRenderer( ) ,
             SIGNAL( finalFrameReady_SIGNAL( QPixmap* )) ,
             this , SLOT( finalFrameReady_SLOT( QPixmap* )));

    // Rendering Types
    connect( ui->xrayButton, SIGNAL( toggled( bool )),
             this, SLOT( switchRenderingKernel_SLOT( )));

    connect( ui->maxIntensityProjectionButton ,
             SIGNAL( toggled( bool )) ,
             this, SLOT( switchRenderingKernel_SLOT( )));

    connect( ui->minIntensityProjectionButton ,
             SIGNAL( toggled( bool )) ,
             this , SLOT( switchRenderingKernel_SLOT( )));

    connect( ui->averageIntensityProjectionButton ,
             SIGNAL( toggled( bool )) ,
             this , SLOT( switchRenderingKernel_SLOT( )));

    connect( ui->isoSurfaceButton ,
             SIGNAL( toggled( bool )) ,
             this , SLOT( switchRenderingKernel_SLOT( )));


    connect (ui->frameContainerResult,
             SIGNAL( mousePressed( QVector2D )),
             this , SLOT (mousePressed_SLOT( QVector2D )));

    connect (ui->frameContainerResult ,
             SIGNAL( mouseMoved( QVector2D )),
             this , SLOT ( mouseMoved_SLOT(QVector2D)) );

    connect (ui->frameContainerResult ,
             SIGNAL( mouseWheelMoved(QWheelEvent*)) ,
             this , SLOT ( mouseWheelMoved_SLOT(QWheelEvent*) ));


    // Getting the initial values from the sliders
    newXTranslation_SLOT( ui->xTranslationSlider->value( ));
    newYTranslation_SLOT( ui->yTranslationSlider->value( ));
    newZTranslation_SLOT( ui->zTranslationSlider->value( ));
    newXRotation_SLOT( ui->xRotationSlider->value( ));
    newYRotation_SLOT( ui->yRotationSlider->value( ));
    newZRotation_SLOT( ui->zRotationSlider->value( ));
    newXScaling_SLOT( ui->xScalingSlider->value( ));
    newYScaling_SLOT( ui->yScalingSlider->value( ));
    newZScaling_SLOT( ui->zScalingSlider->value( ));
    newXYZScaling_SLOT( ui->xyzScalingSlider->value( ));
    newBrightness_SLOT( ui->brightnessSlider->value( ));
    newDensity_SLOT( ui->densitySlider->value( ));
}

void RenderingWindow::finalFrameReady_SLOT(
        QPixmap* finalFrame )
{
    ui->frameContainerResult->
            setPixmap( finalFrame->
                       scaled( ui->frameContainerResult->width( ) ,
                               ui->frameContainerResult->height( ) ,
                               Qt::KeepAspectRatio ));
}

void RenderingWindow::newXRotation_SLOT( int value )
{
    ui->xRotationValue->setText(QString::number( value ));

    clusterServer_.getCLusterRenderer().updateRotationX_SLOT( value );
}

void RenderingWindow::initializeConnections_SLOT()
{
    initializeConnections_();
    clusterServer_.getCLusterRenderer().startRendering();
}

void RenderingWindow::newYRotation_SLOT( int value )
{
    ui->yRotationValue->setText(QString::number( value ));

    clusterServer_.getCLusterRenderer().updateRotationY_SLOT( value );
}

void RenderingWindow::newZRotation_SLOT( int value )
{
    ui->zRotationValue->setText( QString::number( value ));

    clusterServer_.getCLusterRenderer().updateRotationZ_SLOT( value );

}

void RenderingWindow::newXTranslation_SLOT( int value )
{
    ui->xTranslationValue->setText( QString::number( value ));

    clusterServer_.getCLusterRenderer().updateTranslationX_SLOT( value );
}

void RenderingWindow::newYTranslation_SLOT( int value )
{
    ui->yTranslationValue->setText( QString::number( value ));

    clusterServer_.getCLusterRenderer().updateTranslationY_SLOT( value );
}

void RenderingWindow::newZTranslation_SLOT( int value )
{
    ui->zTranslationValue->setText( QString::number( value ));
    clusterServer_.getCLusterRenderer().updateTranslationZ_SLOT( value );

}

void RenderingWindow::newXScaling_SLOT( int value )
{
    ui->xScalingValue->setText( QString::number( value ));
    clusterServer_.getCLusterRenderer().updateScaleX_SLOT( value );
}

void RenderingWindow::newYScaling_SLOT( int value )
{
    ui->yScalingValue->setText( QString::number( value ));
    clusterServer_.getCLusterRenderer().updateScaleY_SLOT( value );
}

void RenderingWindow::newZScaling_SLOT( int value )
{
    ui->zScalingValue->setText( QString::number( value ));
    clusterServer_.getCLusterRenderer().updateScaleZ_SLOT( value );
}

void RenderingWindow::newXYZScaling_SLOT( int value )
{
    ui->xyzScalingValue->setText( QString::number( value ));
    clusterServer_.getCLusterRenderer().updateScaleXYZ_SLOT( value );
}



void RenderingWindow::newBrightness_SLOT( int value )
{
    ui->brightnessValue->setText( QString::number( value ));

    float brightness =  float( value ) / 100;
    clusterServer_.getCLusterRenderer().updateImageBrightness_SLOT( brightness );

}

void RenderingWindow::newDensity_SLOT( int value )
{
    ui->densityValue->setText( QString::number( value ));

    float density = float( value ) / ui->densitySlider->maximum();
    clusterServer_.getCLusterRenderer().updateVolumeDensity_SLOT( density );
}


void RenderingWindow::newIsoValue_SLOT(int value)
{
    float isoValue = value / 255.0;

    ui->isoValueLabel->setText( QString::number( value ));
    clusterServer_.getCLusterRenderer().updateIsoValue_SLOT( isoValue );

}


void RenderingWindow::mousePressed_SLOT(QVector2D mousePressedPosition)
{

    //LOG_DEBUG("Mouse pressed!" );
    lastMousePosition_ =  mousePressedPosition;

}

void RenderingWindow::mouseMoved_SLOT(QVector2D newMousePosition)
{

    //LOG_DEBUG("Mouse moved");

    mousePositionDifference_ = newMousePosition - lastMousePosition_;

    // Rotation axis is perpendicular to the mouse position difference
    // update x rotation angle
    mouseXRotationAngle_ = ui->xRotationSlider->value();
    mouseXRotationAngle_ += mousePositionDifference_.y();
    // update y rotation angle
    mouseYRotationAngle_ = ui->yRotationSlider->value();
    mouseYRotationAngle_ += mousePositionDifference_.x();

//    LOG_DEBUG("y diff :%f", mousePositionDifference_.y() );
//    LOG_DEBUG("x diff :%f", mousePositionDifference_.x() );
//    LOG_DEBUG("Rot x :%f",  mouseXRotationAngle_ );
//    LOG_DEBUG("Rot y :%f",  mouseYRotationAngle_ );

    if(mouseXRotationAngle_ >= 360)
        mouseXRotationAngle_ = 0;

    newMouseXRotation_( );

    if(mouseYRotationAngle_ >= 360)
        mouseYRotationAngle_ = 0;

    newMouseYRotation_( );

    // update last position
    lastMousePosition_ = newMousePosition;

}

void RenderingWindow::mouseWheelMoved_SLOT(QWheelEvent *event)
{
    //LOG_DEBUG("Mouse wheel moved");

    int translationZ = ui->zTranslationSlider->value();
    int numDegrees = event->delta() / 8;
    int numSteps = numDegrees / 15;
    translationZ += numSteps;

    newMouseZTranslation_(translationZ);
}

void RenderingWindow::newMouseXRotation_( )
{
    ui->xRotationSlider->setValue( mouseXRotationAngle_);
    clusterServer_.getCLusterRenderer().updateRotationX_SLOT( mouseXRotationAngle_ );
}


void RenderingWindow::newMouseYRotation_( )
{
    ui->yRotationSlider->setValue( mouseYRotationAngle_);
    clusterServer_.getCLusterRenderer().updateRotationY_SLOT( mouseYRotationAngle_ );

}

void RenderingWindow::newMouseZTranslation_(int value)
{
    //LOG_DEBUG("zTranslate %d " , value );
    ui->zTranslationSlider->setValue( value);
    clusterServer_.getCLusterRenderer().updateTranslationZ_SLOT( value );
}



void RenderingWindow::switchRenderingKernel_SLOT( )
{
    if( ui->xrayButton->isChecked( ))
        clusterServer_.getCLusterRenderer().
                activateRenderingKernel_SLOT(
                    clparen::CLKernel::RenderingMode::RENDERING_MODE_Xray );

    else if( ui->maxIntensityProjectionButton->isChecked( ))
        clusterServer_.getCLusterRenderer().
                activateRenderingKernel_SLOT(
                    clparen::CLKernel::RenderingMode::RENDERING_MODE_MaxIntensity );

    else if( ui->minIntensityProjectionButton->isChecked( ))
        clusterServer_.getCLusterRenderer().
                activateRenderingKernel_SLOT(
                    clparen::CLKernel::RenderingMode::RENDERING_MODE_MinIntensity );

    else if(ui->averageIntensityProjectionButton->isChecked( ))
        clusterServer_.getCLusterRenderer().
                activateRenderingKernel_SLOT(
                    clparen::CLKernel::RenderingMode::RENDERING_MODE_AverageIntensity );

    else if(ui->isoSurfaceButton->isChecked( ))
        clusterServer_.getCLusterRenderer().
                activateRenderingKernel_SLOT(
                    clparen::CLKernel::RenderingMode::RENDERING_MODE_IsoSurface );

    ui->isoValueSlider->setEnabled( ui->isoSurfaceButton->isChecked( ));

}
