# Application name
set( TEST_SSH_APP testSSH )

# Application source/header files
set( TEST_SSH_APP_SOURCES
testSSH.cpp
SignalReceiverTest.cpp
)
set( TEST_SSH_APP_HEADERS
SignalReceiverTest.h
)



# Generate the executables
add_executable( ${TEST_SSH_APP}
                ${TEST_SSH_APP_SOURCES}
                ${TEST_SSH_APP_HEADERS} )



# Link the application against the GLDPVR library
target_link_libraries( ${TEST_SSH_APP} ${OCLUSTER_LIB} )


# Make sure to use the required Qt modules
qt5_use_modules( ${TEST_SSH_APP}  Core Widgets OpenGL Gui Network)

# Install the executable (generate with 'make install' in the bin directory)
install( TARGETS ${TEST_SSH_APP} DESTINATION bin )
