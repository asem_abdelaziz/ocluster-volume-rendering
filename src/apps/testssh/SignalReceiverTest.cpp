#include "SignalReceiverTest.h"



SignalReveiverTest::SignalReveiverTest( NetworkManagerSSH &networkManager ,
                                        QString command )
    : nm_( networkManager ),
      command_( command )
{

}

void SignalReveiverTest::finishedSurvey()
{
    LOG_INFO("Survey Done");
    QList< quint32 > verifiedNodes = nm_.getVerfiedRemoteNodes().toList();

    for( quint32 ip : verifiedNodes )
    {
        LOG_INFO("verified: %s",
                 QHostAddress(ip).toString().toStdString().c_str());
    }

    nm_.fetchRemoteNodes( verifiedNodes );


}

void SignalReveiverTest::finishedFetching()
{
    QList< quint32 > responsiveNodes =
            nm_.executeRemoteCommandAll(  command_.toStdString( ));

    for( const quint32 ip : responsiveNodes )
        LOG_INFO("Machine of address %s : RESPONDED" ,
                 QHostAddress(ip).toString().toStdString().c_str( ));

    exit(EXIT_SUCCESS);
}
