#include <QCoreApplication>
#include "NetworkManagerSSH.h"
#include "SignalReceiverTest.h"



int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);


    QString command = QString(argv[1]);


    NetworkManagerSSH nm( "vizuser"  ,
                          "alpha@viz"  ,
                          22  ,
                          256 );
    SignalReveiverTest sr( nm , command );

    QObject::connect( &nm , SIGNAL(remoteNodesVerified_SIGNAL()) ,
                      &sr , SLOT(finishedSurvey()));

    QObject::connect( &nm , SIGNAL(remoteNodesFetched_SIGNAL()) ,
                      &sr , SLOT(finishedFetching()));

    nm.surveyRemoteNodes();


    return a.exec();
}

