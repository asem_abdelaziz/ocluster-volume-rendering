#ifndef SIGNALREVEIVERTEST_H
#define SIGNALREVEIVERTEST_H
#include <QObject>
#include "NetworkManagerSSH.h"
#include "Logger.h"

class SignalReveiverTest : public QObject
{
    Q_OBJECT
public:
    SignalReveiverTest( NetworkManagerSSH &networkManager ,
                        QString command );
public Q_SLOTS:
    void finishedSurvey();

    void finishedFetching();

private:
    NetworkManagerSSH &nm_ ;
    QString command_ ;
};


#endif // SIGNALREVEIVERTEST_H
