#include "RenderingWindow.h"
#include "ui_RenderingWindow.h"
#include "Logger.h"

RenderingWindow::RenderingWindow( CLusterServer &clusterServer ,
                                  QWidget *parent)
    : clusterServer_( clusterServer ),
      QMainWindow(parent),
      ui( new Ui::RenderingWindow )
{
    ui->setupUi(this);
    initializeSignalAndSlots_();
    clusterServer.getCLusterRenderer().startRendering();

}

RenderingWindow::~RenderingWindow()
{
    delete ui;
}

void RenderingWindow::initializeSignalAndSlots_()
{

    // Buttons
    connect( ui->connectAllMachinesButton , SIGNAL(released( )) ,
             this , SLOT(connectAllMachines_SLOT( )));

    connect( ui->fetchMachineButton , SIGNAL(released( )) ,
             this , SLOT(fetchMachine_SLOT( )));

    connect( ui->scanInterfaceButton , SIGNAL(released( )) ,
             this , SLOT(scanInterface_SLOT( )));

    connect( ui->startRenderingButton , SIGNAL(released( )),
             this , SLOT(startRendering_SLOT( )));

    connect( ui->scanRangeButton , SIGNAL(released( )) ,
             this , SLOT(scanRange_SLOT( )));

    // Server
    connect( &clusterServer_ , SIGNAL(newOnlineMachines_SIGNAL()) ,
             this , SLOT(newMachinesDetected_SLOT()));

    connect( &clusterServer_ , SIGNAL(rendererNodeInfoReady_SIGNAL( quint32 )),
             this , SLOT(updateMachineInfo_SLOT( quint32 )));

    connect( &clusterServer_ , SIGNAL(rendererNodeDisconnected_SIGNAL(quint32)),
             this , SLOT(machineDisconnected_SLOT( quint32 )));

    connect( &clusterServer_ , SIGNAL(rendererNodeConnected_SIGNAL(quint32)),
             this , SLOT(machineConnected_SLOT( quint32 )));


    //CLusterRenderer
    connect( & clusterServer_.getCLusterRenderer() ,
             SIGNAL(finalFrameReady_SIGNAL( clparen::CLData::CLImage2D<float>* )) ,
             this ,
             SLOT(finalFrameReady_SLOT( clparen::CLData::CLImage2D<float>* )));


    // Sliders
    connect( ui->xRotationSlider , SIGNAL( valueChanged( int )),
             this , SLOT( newXRotation_SLOT( int )));

    connect( ui->yRotationSlider , SIGNAL( valueChanged( int )),
             this , SLOT( newYRotation_SLOT( int )));

    connect( ui->zRotationSlider , SIGNAL( valueChanged( int )),
             this , SLOT( newZRotation_SLOT( int )));

    connect( ui->xTranslationSlider , SIGNAL( valueChanged( int )),
             this , SLOT( newXTranslation_SLOT( int )));

    connect( ui->yTranslationSlider , SIGNAL( valueChanged( int )),
             this , SLOT( newYTranslation_SLOT( int )));

    connect( ui->brightnessSlider , SIGNAL( valueChanged( int )),
             this , SLOT( newBrightness_SLOT( int )));

    connect( ui->densitySlider , SIGNAL( valueChanged( int )),
             this , SLOT( newDensity_SLOT( int )));


    // Connected Machines List
    connect( ui->connectedMachinesList , SIGNAL(itemClicked(QListWidgetItem*)),
             this , SLOT(connectedMachineSelected_SLOT( QListWidgetItem* )));
}

void RenderingWindow::fillAvailableMachinesList_()
{
    const QList< quint32 > &availableMachines =
            clusterServer_.getOnlineMachines().toList();

    ui->availableMachinesList->clear();

    for( const quint32 ip : availableMachines )
        ui->availableMachinesList->addItem( QHostAddress( ip ).toString( ));

}

void RenderingWindow::fillConnectedMachinesList_()
{
    const QMap< quint32 , Node::RendererNode* > &connectedMachines =
            clusterServer_.getCLRenderers();

    ui->connectedMachinesList->clear();

    for( const quint32 ip : connectedMachines.keys())
        ui->connectedMachinesList->addItem( QHostAddress( ip ).toString( ));

}

void RenderingWindow::fillNodeDescription_( const Node::RendererNode *node )
{
    QStringList description;

    const CLRendererNodeInfo &info = node->getNodeInfo();


    description << QString("name:") + info.getName();

    LOG_INFO("Name: [%s]" , info.getName().toStdString().c_str( ));

    const QMap< quint32 , CLRendererDeviceInfo > &devicesInfo =
            info.getRendererDevicesInfo();
    for( const CLRendererDeviceInfo &deviceInfo : devicesInfo.values( ))
    {
        const QString deviceName = deviceInfo.name;
        const QString deviceVendor = deviceInfo.vendor;
        const QString localIndex =
                QString::number( deviceInfo.localGPUIndex );
        const QString globalMemory =
                QString::number( deviceInfo.globalMemory /
                                 ( 1024.f * 1024 * 1024 )) +
                QString(" (GiB)");

        description << QString("local index:") + localIndex
                    << QString("device vendor:") + deviceVendor
                    << QString("device name:") + deviceName
                    << QString("global memory:") + globalMemory ;

        LOG_INFO("Device name: [%s]",
                 deviceName.toStdString().c_str());
        LOG_INFO("Vendor: [%s]" ,
                 deviceVendor.toStdString().c_str());
        LOG_INFO("Local index: [%d]" ,
                 deviceInfo.localGPUIndex );
        LOG_INFO("Global Memory: [%f]" ,
                 deviceInfo.globalMemory / ( 1024.0 * 1024.0 ));
    }


    ui->nodeInfo->insertItems( 0 , description );
}

void RenderingWindow::connectAllMachines_SLOT()
{
    clusterServer_.connectOnlineMachinesToServer( CLIENT_APPLICATION_NAME );
}

void RenderingWindow::fetchMachine_SLOT()
{
    const QString ipAddress = ui->machineIPText->text();

    const QList< quint32 > ipList =
            QList< quint32 >() << QHostAddress( ipAddress ).toIPv4Address( );

    clusterServer_.fetchRenderingMachines( ipList ,
                                           ui->remoteUsername->text( ) ,
                                           ui->remotePassowrd->text( ));

}

void RenderingWindow::scanInterface_SLOT()
{
    const QString interfaceLabel = ui->interfaceLabelText->text();
    clusterServer_.scanInterface( interfaceLabel );
}

void RenderingWindow::scanRange_SLOT()
{
    const QString ipFirstStr = ui->rangeFromLabelText->text();
    const QString ipLastStr = ui->rangeToLabelText->text();


    const quint32 ipFirst = QHostAddress( ipFirstStr ).toIPv4Address();
    const quint32 ipLast = QHostAddress( ipLastStr ).toIPv4Address();

    clusterServer_.scanRange( ipFirst , ipLast );
}

void RenderingWindow::newMachinesDetected_SLOT()
{
    fillAvailableMachinesList_();
}

void RenderingWindow::machineConnected_SLOT( quint32 ip )
{
    fillConnectedMachinesList_();
}

void RenderingWindow::machineDisconnected_SLOT( quint32 ip )
{
    fillAvailableMachinesList_();
    fillConnectedMachinesList_();
}

void RenderingWindow::updateMachineInfo_SLOT( quint32 ip )
{
    if( ip == selectedMachine_ )
    {
        ui->nodeInfo->clear();
        const Node::RendererNode* node = clusterServer_.getCLRenderers()[ ip ];
        fillNodeDescription_( node );
    }
}

void RenderingWindow::connectedMachineSelected_SLOT( QListWidgetItem* item )
{
    const QString address = item->text();

    const QMap< quint32 , Node::RendererNode* > connectedMachines =
            clusterServer_.getCLRenderers();

    for( const quint32 ip : connectedMachines.keys() )
        if( QHostAddress( ip ).toString() == address )
        {
            selectedMachine_ = ip ;
            ui->nodeInfo->clear();

            const Node::RendererNode* node = connectedMachines[ ip ];
            fillNodeDescription_( node );
        }
}

void RenderingWindow::newXRotation_SLOT( int value )
{
    ui->xRotationLabel->setText( QString::number( value ));
    clusterServer_.getCLusterRenderer().updateRotationX_SLOT( value );

}

void RenderingWindow::newYRotation_SLOT( int value )
{
    ui->yRotationLabel->setText( QString::number( value ));
    clusterServer_.getCLusterRenderer().updateRotationY_SLOT( value );


}

void RenderingWindow::newZRotation_SLOT( int value )
{
    ui->zRotationLabel->setText( QString::number( value ));
    clusterServer_.getCLusterRenderer().updateRotationZ_SLOT( value );

}

void RenderingWindow::newXTranslation_SLOT( int value )
{
    ui->xTranslationLabel->setText( QString::number( value ));
    clusterServer_.getCLusterRenderer().updateTranslationX_SLOT( value );

}

void RenderingWindow::newYTranslation_SLOT( int value )
{
    ui->yTranslationLabel->setText( QString::number( value ));
    clusterServer_.getCLusterRenderer().updateTranslationX_SLOT( value );

}

void RenderingWindow::newBrightness_SLOT( int value )
{
    ui->brightnessLabel->setText( QString::number( value ));
    clusterServer_.getCLusterRenderer().
            updateImageBrightness_SLOT( value / 100.f );

}

void RenderingWindow::newDensity_SLOT( int value )
{
    ui->densityLabel->setText( QString::number( value ));
    clusterServer_.getCLusterRenderer().
            updateVolumeDensity_SLOT( value / 100.f );
}

void RenderingWindow::startRendering_SLOT()
{
    clusterServer_.initializeRendering();
}

void RenderingWindow::finalFrameReady_SLOT( clparen::CLData::CLImage2D< float > *frame  )
{
    ui->finalFrame->
            setPixmap( frame->
                       getFramePixmap().
                       scaled( ui->finalFrame->width() ,
                               ui->finalFrame->height() ,
                               Qt::KeepAspectRatio ));
}

