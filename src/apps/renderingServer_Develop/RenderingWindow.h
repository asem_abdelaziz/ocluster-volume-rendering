#ifndef RENDERINGWINDOW_H
#define RENDERINGWINDOW_H

#include <QMainWindow>
#include "CLusterServer.h"
#include <QListWidgetItem>
#include "ClientApplicationConfig.hh"

using namespace ocluster;

namespace Ui {
class RenderingWindow;
}

class RenderingWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit RenderingWindow( CLusterServer &clusterServer ,
                              QWidget *parent = 0 );
    ~RenderingWindow();

public Q_SLOTS:
    void connectAllMachines_SLOT();

    void fetchMachine_SLOT();

    void scanInterface_SLOT();

    void scanRange_SLOT();

    void newMachinesDetected_SLOT();

    void machineConnected_SLOT( quint32 ip );

    void machineDisconnected_SLOT( quint32 ip );

    void updateMachineInfo_SLOT( quint32 ip );

    void connectedMachineSelected_SLOT( QListWidgetItem* item );

    void newXRotation_SLOT( int value );

    void newYRotation_SLOT( int value );

    void newZRotation_SLOT( int value );

    void newXTranslation_SLOT( int value );

    void newYTranslation_SLOT( int value );

    void newBrightness_SLOT( int value );

    void newDensity_SLOT( int value );

    void startRendering_SLOT( );

    void finalFrameReady_SLOT( clparen::CLData::CLImage2D< float > *frame );

private:
    void initializeSignalAndSlots_();

    void fillAvailableMachinesList_();

    void fillConnectedMachinesList_();

    void fillNodeDescription_( const Node::RendererNode *node );

private:
    CLusterServer &clusterServer_ ;

    quint32 selectedMachine_ ;

    Ui::RenderingWindow *ui;

};

#endif // RENDERINGWINDOW_H
