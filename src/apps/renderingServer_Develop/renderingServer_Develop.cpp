#include <QApplication>

#include "RenderingWindow.h"
#include "NetworkManagerSSH.h"
#include "CLusterServer.h"
#include "CLusterSortLast.h"

using namespace ocluster;
using namespace ocluster::Parallel;
using namespace clparen::CLData ;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QMap< quint32 , Node::RendererNode* > clRenderers;
    CLusterSortLast< uchar , float > clusterRenderer(
                clRenderers ,
                FRAME_CHANNEL_ORDER::ORDER_INTENSITY);

    CLusterServer s( clRenderers , clusterRenderer );

    s.initializeServer();

    s.getCLusterRenderer().loadBaseVolume( VOLUME_PATH );

    RenderingWindow w( s );
    w.show();

    return a.exec();
}
