#include "TcpChannel.h"

const uint8_t TcpChannel::_acknowledgeByte = 123 ;
const int TcpChannel::_acknowledgementTimeout = ACK_TIMEOUT ;
const TcpChannel::Protocols TcpChannel::_protocols = TcpChannel::_getProtocols();

TcpChannel::TcpChannel( QTcpSocket *socket, QObject *parent )
    :  QObject( parent ),
      _socketParentThread( QThread::currentThread())
{
    _socket.reset( socket );
    _socket->setParent( 0 );
    _constructConnections();
}

TcpChannel::TcpChannel( const quintptr socketDescriptor, QObject *parent )
    : QObject( parent ) ,
      _socketParentThread( QThread::currentThread())
{
    _socket.reset( new QTcpSocket());
    _socket->setSocketDescriptor( socketDescriptor );
    _socket->setReadBufferSize( 0 );
    _constructConnections();
}

TcpChannel::TcpChannel( const QHostAddress serverAddress, int serverPort, QObject *parent)
    : QObject( parent ) ,
      _socketParentThread( QThread::currentThread())
{
    _socket.reset( new QTcpSocket(  ));
    _socket->connectToHost( serverAddress ,
                            serverPort  );
    _constructConnections();
}

void TcpChannel::sendInstruction( const Instruction &instruction )
{
    if( _tasksQueue.size() > MAX_RUNNING_THREADS )
        // Possible dead-lock!
        _tasksQueue.pop_front();

    _tasksQueue.emplace_back(
                std::async( std::launch::async ,
                            &TcpChannel::_transmit , this ,
                            instruction ));
}

const QTcpSocket &TcpChannel::getSocket() const
{
    return *_socket;
}

void TcpChannel::incomingInstruction_SLOT()
{
    LOG_DEBUG("Incoming instruction..");
    if( _socket->bytesAvailable() > 0 )
    {
        if( _tasksQueue.size() > MAX_RUNNING_THREADS )
            _tasksQueue.pop_front();
        _tasksQueue
                .emplace_back(
                    std::async( std::launch::async ,
                                &TcpChannel::_receive , this ));
    }

}

void TcpChannel::socketDisconnected_SLOT()
{
    Q_EMIT disconnected_SIGNAL();
}

void TcpChannel::moveSocketToThread_SLOT( QThread *thread )
{
    _socket->moveToThread( thread );
}

void TcpChannel::checkSocket_SLOT()
{
    if( _socketMutex.try_lock( ))
    {
        _socketMutex.unlock();
        if( _socket->bytesAvailable( ) > 0 )
            incomingInstruction_SLOT();
    }
}

void TcpChannel::_configureDataStream( QDataStream &stream )
{
    stream.setVersion( QDataStream::Qt_4_0 );
}

void TcpChannel::_sendACK( )
{
    QByteArray block ;
    QDataStream stream( &block , QIODevice::WriteOnly )  ;
    stream.setVersion( QDataStream::Qt_4_0 );
    stream << _acknowledgeByte ;
    _socket->write( block );
    _socket->waitForBytesWritten( -1 );
}

bool TcpChannel::_waitACK( )
{
    if( _socket->waitForReadyRead( _acknowledgementTimeout ))
    {
        QDataStream incoming( _socket.get() ) ;
        incoming.setVersion( QDataStream::Qt_4_0 );
        uint8_t ackByte;
        incoming >> ackByte ;
        if( ackByte != _acknowledgeByte )
        {
            LOG_WARNING("Acknowledgement Failed: %d != %d !" ,
                        ackByte , _acknowledgeByte );
            return false;
        }
    }
    else
    {
        LOG_WARNING("Acknowledgement TIMEOUT:%d!" ,
                    _acknowledgementTimeout );
        return false;
    }
    return true ;
}


void TcpChannel::_transmissionFailure( const std::string errorMsg )
{
    LOG_DEBUG("Peer address: [%s]" ,
              _socket->peerAddress().toString().toStdString().c_str());


    _socket->moveToThread( _socketParentThread );
    if( _socket->thread( ) != _socketParentThread )
        LOG_WARNING("Cannot move socket to parent thread!");

    LOG_ERROR( errorMsg.c_str( ));
}

void TcpChannel::_receive()
{
    std::lock_guard<std::mutex> guard( _socketMutex );
    if( _socket->bytesAvailable() > 0 )
    {
        _pullSocket();
        QDataStream incomingStream( _socket.get() );
        _configureDataStream( incomingStream );

        Serializable *serializable;
        quint64 blockSize = 0;
        auto receive = [&,this]() -> bool {
            if( blockSize == 0 )
            {
                if( _socket->bytesAvailable() < (int) sizeof(quint64))
                    return false;
                incomingStream >> blockSize ;
            }
            if ( _socket->bytesAvailable() < blockSize )
                return false;
            blockSize = 0 ;
            incomingStream >> serializable;
            return true;
        };
        while( !receive()) _socket->waitForReadyRead();

        _instructionIn.reset( dynamic_cast< Instruction* >( serializable ));
        if( _instructionIn ) _acknowledge( *_instructionIn );
        else LOG_ERROR("Bad Cast!");

        _pushSocket();
        Q_EMIT instructionReceived_SIGNAL( *_instructionIn );
    }
}

void TcpChannel::_transmit( const Instruction instruction )
{
    _instructionOut.reset( new Instruction( instruction ));
    auto transmit = _protocols[ static_cast< int >( instruction.getScalability()) ].first;
    (this->*transmit)( instruction );
    Q_EMIT this->instructionTransmitted_SIGNAL( instruction );

}

void TcpChannel::_acknowledge( const Instruction &instruction )
{
    auto ack = _protocols[ static_cast< int >( instruction.getScalability()) ].second;
    (this->*ack)();
}

void TcpChannel::_heavyTransmission( const Instruction &instruction )
{
    // 1.Serialize
    std::unique_ptr< QByteArray > blockHeap( new QByteArray );
    // If data to send is volume data (large data), use
    // the QByteArray on the heap.
    _serialize( &instruction , blockHeap.get() );

    std::lock_guard<std::mutex> guard( _socketMutex );

    _pullSocket();

    if( ( _socket->write( *blockHeap )) < 0 )
        _transmissionFailure("Error sending the data!");
    _socket->waitForBytesWritten();

    LOG_DEBUG("Instruction Transmitted:%s",instruction.toString().c_str());

    _waitACK( );
    _sendACK( );
    _pushSocket();
}

void TcpChannel::_moderateTransmission( const Instruction &instruction )
{
    QByteArray byteArray;
    _serialize( &instruction , &byteArray );

    std::lock_guard<std::mutex> guard( _socketMutex );

    _pullSocket();

    if( ( _socket->write( byteArray )) < 0 )
        _transmissionFailure("Error sending the frame!");
    _socket->waitForBytesWritten();
    // Now wait for acknowledgemnt that receiver proprely deseialized
    // the instruction.
    if( !_waitACK( ))
        _transmissionFailure("Acknowledgement Failed!");

    _pushSocket();
}

void TcpChannel::_lightTransmission( const Instruction &instruction )
{
    // 1.Serialize
    QByteArray block ;
    _serialize( &instruction , &block );
    std::lock_guard<std::mutex> guard( _socketMutex );
    _pullSocket();
    if( ( _socket->write( block )) < 0 )
        _transmissionFailure("Error sending the data!");
    _socket->waitForBytesWritten();

    _pushSocket();
}

void TcpChannel::_heavyAcknowledgement()
{
    _sendACK();
    _waitACK();
}

void TcpChannel::_moderateAcknowledgement()
{
    _sendACK();
}

void TcpChannel::_lightAcknowledgement()
{

}

void TcpChannel::_serialize( const Serializable *instruction, QByteArray *block )
{
    QDataStream stream( block , QIODevice::WriteOnly );
    stream.setVersion( QDataStream::Qt_4_0 );
    stream << quint64( 0 ) ;
    instruction->serialize( stream )  ;
    stream.device()->seek(0) ;
    stream << (quint64) ( block->size( ) - sizeof( quint64 )) ;
}

TcpChannel::Protocols TcpChannel::_getProtocols()
{
    Protocols protocols;
    protocols[ static_cast< int >( Serializable::Scalability::KB ) ] =
            Protocol( &TcpChannel::_lightTransmission ,
                      &TcpChannel::_lightAcknowledgement );
    protocols[ static_cast< int >( Serializable::Scalability::MB ) ] =
            Protocol( &TcpChannel::_moderateTransmission ,
                      &TcpChannel::_moderateAcknowledgement );
    protocols[ static_cast< int >( Serializable::Scalability::GB ) ] =
            Protocol( &TcpChannel::_heavyTransmission ,
                      &TcpChannel::_heavyAcknowledgement );
    return protocols;
}

void TcpChannel::_pullSocket()
{
    //    LOG_DEBUG("[CurrentThread:%p],[ChannelThread:%p],[SocketThread:%p]",
    //              QThread::currentThread() , thread() , _socket->thread());
    //Block until receiver pushs the ownership of the socket to current thread.
    if( _socket->thread() != QThread::currentThread())
    {
        Q_EMIT moveSocketToThread_SIGNAL( QThread::currentThread( ));
        if( _socket->thread( ) != QThread::currentThread( ))
            _transmissionFailure("Cannot move socket to parent thread!");
    }
}

void TcpChannel::_pushSocket()
{
    //Push back the the socket ownership to its old parent.
    _socket->moveToThread( _socketParentThread );
    if( _socket->thread( ) != _socketParentThread )
        LOG_WARNING("Cannot move socket to parent thread!");
}

void TcpChannel::_constructConnections()
{
    connect( _socket.get() , SIGNAL(readyRead( )) ,
             this , SLOT(incomingInstruction_SLOT( )));
    connect( _socket.get() , SIGNAL(disconnected()) ,
             this , SLOT(socketDisconnected_SLOT()));

    connect( this , SIGNAL(moveSocketToThread_SIGNAL(QThread*)) ,
             this , SLOT(moveSocketToThread_SLOT(QThread*)) ,
             Qt::BlockingQueuedConnection );

    connect( this , SIGNAL(instructionReceived_SIGNAL(Instruction)) ,
             this , SLOT(checkSocket_SLOT()));
    connect( this , SIGNAL(instructionTransmitted_SIGNAL(Instruction)),
             this , SLOT(checkSocket_SLOT()));
}
