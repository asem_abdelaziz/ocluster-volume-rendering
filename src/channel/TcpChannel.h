#ifndef TCPCHANNEL_H
#define TCPCHANNEL_H

// std
#include <mutex>
#include <future>
#include <memory>
#include <array>
#include <deque>

// Qt
#include <QTcpSocket>
#include <QDataStream>
#include <QThread>
#include <QHostAddress>

// local
#include "Logger.h"
#include "Instruction.h"

#define MINUTE 60000 /* ms */
#define ACK_TIMEOUT 10 * MINUTE
#define MAX_RUNNING_THREADS 40

class TcpChannel : public QObject
{
    Q_OBJECT
public:
    TcpChannel( QTcpSocket *socket , QObject *parent = 0 );
    TcpChannel( const quintptr socketDescriptor , QObject *parent = 0 );
    TcpChannel( const QHostAddress serverAddress , int serverPort, QObject *parent = 0 );

    template< typename ...Args  >
    void sendInstruction( Args ...args ){
        sendInstruction( Instruction( args... ));
    }

    void sendInstruction( const Instruction &instruction );

    template< class... Args >
    void sendInstruction_SYNC( Args&& ...args ){
        _transmit( Instruction(  args... ));
    }

    const QTcpSocket &getSocket() const;

Q_SIGNALS:

    /**
     * @brief instructionReceived_SIGNAL
     * @param instruction
     */
    void instructionReceived_SIGNAL( Instruction );

    /**
     * @brief instructionTransmitted_SIGNAL
     * @param sent
     */
    void instructionTransmitted_SIGNAL( Instruction );

    /**
     * @brief moveSocketToThread_SIGNAL
     * @param socket
     * @param thread
     */
    void moveSocketToThread_SIGNAL( QThread *thread );


    /**
     * @brief disconneted_SIGNAL
     */
    void disconnected_SIGNAL();

private Q_SLOTS:
    void incomingInstruction_SLOT();
    void socketDisconnected_SLOT();
    void moveSocketToThread_SLOT( QThread *thread );
    void checkSocket_SLOT();
protected:
    static const uint8_t _acknowledgeByte ;
    static const int _acknowledgementTimeout ;
    typedef void (TcpChannel::*TransmissionProtocol)( const Instruction & );
    typedef void (TcpChannel::*AcknowledgementRoutine)( void );
    typedef std::pair< TransmissionProtocol , AcknowledgementRoutine > Protocol;
    typedef std::array< Protocol , static_cast< int >( Serializable::Scalability::EnumOffset ) > Protocols;

protected:

    static void _configureDataStream( QDataStream &stream );
    void _sendACK();
    bool _waitACK();
    void _transmissionFailure( const std::string errorMsg );

    void _receive();
    static void _serialize( const Serializable *instruction , QByteArray *byteArray );

    void _transmit( const Instruction instruction );
    void _acknowledge( const Instruction &instruction );

    void _heavyTransmission( const Instruction &instruction );
    void _moderateTransmission( const Instruction &instruction );
    void _lightTransmission( const Instruction &instruction );

    void _heavyAcknowledgement();
    void _moderateAcknowledgement();
    void _lightAcknowledgement();

    static Protocols _getProtocols();
    void _pullSocket();
    void _pushSocket();
    void _constructConnections();
private:
    std::unique_ptr< QTcpSocket >_socket;
    QThread *_socketParentThread ;
    std::mutex _socketMutex ;
    std::unique_ptr< Instruction > _instructionOut;
    std::unique_ptr< Instruction > _instructionIn;
    std::deque< std::future< void >> _tasksQueue;
    static const Protocols _protocols;

};

#endif // TCPCHANNEL_H
