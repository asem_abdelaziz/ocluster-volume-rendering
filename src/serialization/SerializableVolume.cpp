#include "SerializableVolume.h"
#include "Logger.h"

template< typename T >
struct SegmentData
{
    SegmentData()
        : segmented( false ) ,
          segmentsCount( 1 ),
          currentSegmentRank( 0 ),
          serializedSegmentsCount( new std::atomic_int( 0 )) { }

    // Segmentation info
    bool  segmented ;
    int   segmentsCount ;
    int   currentSegmentRank ;
    std::pair< uint64_t , uint64_t > range;
    std::map< int , std::shared_ptr< SerializableVolume< T >>> allSegments ;
    std::shared_ptr< std::atomic_int >  serializedSegmentsCount ;

};


template< typename T >
struct VolumeData
{
    VolumeData( const BrickParameters< T > &bp )
        : dimensions( bp.dimensions_  ),
          coordinates( bp.coordinates_ ),
          unitCubeCenter( bp.unitCubeCenter_ ),
          unitCubeScaleFactors( bp.unitCubeScaleFactors_ ) ,
          baseDimensions( bp.baseDimensions_) ,
          origin( bp.origin_) ,
          baseData( bp.baseData_ ){}

    VolumeData( const Volume< T > &v )
        : data( v.getData()) ,
          dimensions( v.getDimensions()),
          coordinates( v.getCubeCenter()),
          unitCubeCenter( v.getUnitCubeCenter()),
          unitCubeScaleFactors( v.getUnitCubeScaleFactors()){}

    VolumeData() = default;

    bool isSameMetaData( const VolumeData< T > &vd ) const
    {
        return dimensions == vd.dimensions &&
                coordinates == vd.coordinates &&
                unitCubeCenter == vd.unitCubeCenter &&
                unitCubeScaleFactors == vd.unitCubeScaleFactors ;
    }


    // self
    std::shared_ptr< T > data ;
    Dimensions3D    dimensions ;
    Coordinates3D   coordinates ;
    Coordinates3D   unitCubeCenter ;
    Coordinates3D   unitCubeScaleFactors ;

    // base
    std::shared_ptr< T > baseData ;
    Dimensions3D    baseDimensions;
    Dimensions3D    origin ;
};

template< class T >
SerializableVolume< T >::SerializableVolume()
    : _volumeData( new VolumeData< T > ),
      _segmentData( new SegmentData< T > )
{

}

template< class T >
SerializableVolume< T >::SerializableVolume( const SerializableVolume<T> &other )
    : _targetGPUIndex( other._targetGPUIndex )
{
    if( other._volumeData )
        _volumeData.reset( new VolumeData< T >( *other._volumeData ));
    if( other._segmentData )
        _segmentData.reset( new SegmentData< T >( *other._segmentData ));
}

template< class T >
SerializableVolume< T >::SerializableVolume(
        const BrickParameters< T > brickParameters ,
        bool deepCopy )
    : _volumeData( new VolumeData< T >( brickParameters ) ),
      _segmentData( new SegmentData< T > )
{
    if( deepCopy )
    {
        _volumeData->data.reset( new T[ volumeSize() ] , &Volume< T >::bufferDeleter );
        auto data = _volumeData->data.get();
        const auto dimensions = _volumeData->dimensions;
        const auto baseDimensions = _volumeData->baseDimensions;
        const auto origin = _volumeData->origin;
        const auto baseData = _volumeData->baseData.get();
        const auto zFinal = _volumeData->dimensions.z;
        const auto yFinal = _volumeData->dimensions.y;
        const auto xFinal = _volumeData->dimensions.x;

        for( uint64_t k = 0; k < zFinal; k++ )
            for( uint64_t j = 0; j < yFinal; j++ )
                for( uint64_t i = 0; i < xFinal; i++ )
                {
                    const uint64_t brickIndex =
                            VolumeUtilities::get1DIndex( i , j , k ,
                                                         dimensions );
                    const uint64_t volumeIndex =
                            VolumeUtilities::get1DIndex( origin.x + i,
                                                         origin.y + j,
                                                         origin.z + k ,
                                                         baseDimensions );
                    data[ brickIndex] = baseData[ volumeIndex ];
                }
    }
}

template< class T >
SerializableVolume< T >::~SerializableVolume()
{

}


template< class T >
SerializableVolume< T >::SerializableVolume( const Volume<T> &volume )
{
    _volumeData.reset( new VolumeData< T >( volume ));
    _segmentData.reset( new SegmentData< T > );
}

template< class T >
const T *SerializableVolume< T >::getConstData() const
{
    return _volumeData->data.get() ;
}

template< class T >
T *SerializableVolume< T >::getData()
{
    return _volumeData->data.get() ;
}

template< class T >
uint64_t SerializableVolume< T >::volumeSize() const
{
    return _volumeData->dimensions.volumeSize( ) ;
}

template< class T >
uint64_t SerializableVolume< T >::dataSize() const
{
    return volumeSize( ) * sizeof( T ) ;
}

template< class T >
uint64_t SerializableVolume< T >::dataSizeMB() const
{
    return  ( dataSize( ) / BYTES_IN_MB )  +
            static_cast< uint64_t >
            (( dataSize( ) % BYTES_IN_MB != 0 )? 1 : 0 );
}

template< class T >
SerializableVolume< T > &
SerializableVolume< T >::operator=( const Volume< T > &volume )
{
    _volumeData.reset( new VolumeData< T >( volume ));
}

template< class T >
void SerializableVolume< T >::setTargetGPUIndex( int index )
{
    _targetGPUIndex = index ;
}

template< class T >
int SerializableVolume< T >::getTargetGPUIndex( ) const
{
    return _targetGPUIndex ;
}

template< class T >
Dimensions3D SerializableVolume<T>::getDimensions() const
{
    return _volumeData->dimensions ;
}

template< class T >
Coordinates3D SerializableVolume< T >::getCoordinates() const
{
    return _volumeData->coordinates ;
}

template< class T >
Coordinates3D SerializableVolume< T >::getUnitCubeCenter() const
{
    return _volumeData->unitCubeCenter ;
}

template< class T >
Coordinates3D SerializableVolume< T >::getUnitCubeScaleFactors() const
{
    return _volumeData->unitCubeScaleFactors ;
}

template< class T >
uint64_t SerializableVolume< T >::maxSegmentSizeMB() const
{
    return MAX_SEGMENT_SIZE_MB ;
}

template< class T >
uint64_t SerializableVolume< T >::maxSegmentSize() const
{
    return maxSegmentSizeMB() * BYTES_IN_MB ;
}

template< class T >
std::map< int , std::shared_ptr<SerializableVolume<T>>> &SerializableVolume<T>::makeSegments( )
{

    _segmentData->allSegments.clear();
    _segmentData->segmentsCount = dataSize() /  maxSegmentSize() +
                                  static_cast< uint64_t >
                                  (( dataSize() %  maxSegmentSize() != 0 )? 1 : 0 );

    uint64_t offset = 0;
    for( auto i = 0 ; i < _segmentData->segmentsCount ; i++ )
    {
        _segmentData->allSegments[ i ] = std::make_shared< SerializableVolume< T >>( *this );
        auto segment = _segmentData->allSegments.at( i ).get();

        segment->_segmentData->segmented = true ;
        segment->_segmentData->segmentsCount = _segmentData->segmentsCount ;
        segment->_segmentData->currentSegmentRank = i ;

        segment->_segmentData->range.first = offset ;
        if( i != _segmentData->segmentsCount - 1 )
        {
            offset += maxSegmentSize() / sizeof( T );
            segment->_segmentData->range.second = offset;
        }
        else segment->_segmentData->range.second = volumeSize();
    }
    return _segmentData->allSegments ;
}

template< class T >
void SerializableVolume< T >::insertSegment(
        const SerializableVolume< T > &volumeSegment )
{
    // Clone details for the first arriving segment!
    if( !_segmentData->segmented )
    {
        _segmentData->segmented = true ;
        _segmentData->segmentsCount = volumeSegment._segmentData->segmentsCount ;
        *_volumeData = *volumeSegment._volumeData ;
        _targetGPUIndex = volumeSegment._targetGPUIndex ;
    }

    else if( !_volumeData->isSameMetaData( *volumeSegment._volumeData ) ||
             volumeSegment._targetGPUIndex != _targetGPUIndex ||
             volumeSegment._segmentData->segmentsCount != _segmentData->segmentsCount )

    {
        const Dimensions3D &d1 = volumeSegment._volumeData->dimensions;
        const Coordinates3D &c1 = volumeSegment._volumeData->coordinates ;
        const Coordinates3D &uc1 = volumeSegment._volumeData->unitCubeCenter ;
        const Coordinates3D &us1 = volumeSegment._volumeData->unitCubeScaleFactors ;
        const quint32 g1 = volumeSegment._targetGPUIndex ;
        const quint64 s1 = volumeSegment._segmentData->segmentsCount ;

        const Dimensions3D &d2 = _volumeData->dimensions;
        const Coordinates3D &c2 = _volumeData->coordinates ;
        const Coordinates3D &uc2 = _volumeData->unitCubeCenter ;
        const Coordinates3D &us2 = _volumeData->unitCubeScaleFactors ;
        const quint32 g2 = _targetGPUIndex ;
        const quint64 s2 = _segmentData->segmentsCount ;

        LOG_DEBUG("D: (%ld,%ld,%ld)|(%ld,%ld,%ld) "
                  "C: (%f,%f,%f)|(%f,%f,%f) "
                  "UC: (%f,%f,%f)|(%f,%f,%f) "
                  "US: (%f,%f,%f)|(%f,%f,%f) "
                  "GPU: %d|%d "
                  "S: %ld|%ld " , d1.x ,d1.y , d1.z , d2.x , d2.y , d2.z ,
                  c1.x , c1.y , c1.z , c2.x , c2.y , c2.z ,
                  uc1.x , uc1.y , uc1.z , uc2.x , uc2.y , uc2.z ,
                  us1.x , us1.y , us1.z , us2.x , us2.y , us2.z ,
                  g1 , g2 , s1 , s2 ) ;

        LOG_ERROR("Inserted segment is inconsistent!");
    }
    else if( volumeSegment._segmentData->currentSegmentRank > _segmentData->segmentsCount - 1 )
        LOG_ERROR("Segment rank is exceeding!");

    _segmentData->allSegments[ volumeSegment._segmentData->currentSegmentRank ] =
            std::make_shared< SerializableVolume >( volumeSegment );

}

template< class T >
bool SerializableVolume< T >::allSegmentsAvailable() const
{
    return _segmentData->allSegments.size() == _segmentData->segmentsCount ;
}

template< class T >
void SerializableVolume< T >::mergeSegments( )
{
    if( !allSegmentsAvailable( ))
        LOG_ERROR("%d Missing segment(s)!" ,
                  _segmentData->segmentsCount - _segmentData->allSegments.size( ));
    try
    {
        _volumeData->data.reset( new T[ volumeSize() ], &Volume<T>::bufferDeleter );
    }
    catch( std::exception &e )
    {
        LOG_ERROR("Bad Alloc: %s" , e.what());
    }

    auto data = _volumeData->data.get();
    auto segmentsCount = _segmentData->segmentsCount;
    for( uint64_t i = 0 ; i < segmentsCount ; i++ )
    {
        auto segment = _segmentData->allSegments.at( i );
        auto offset = segment->_segmentData->range.first;
        auto segmentSize = segment->_segmentData->range.second - offset;

        std::copy( &segment->getData()[ 0 ]  ,
                &segment->getData()[ segmentSize - 1 ] ,
                &data[ offset ] );
    }
    _segmentData->allSegments.clear();
}

template< class T >
uint64_t SerializableVolume< T >::checksum() const
{

    uint64_t from ,until ;
    if( _segmentData->segmented )
    {
        from =  _segmentData->range.first;
        until =  _segmentData->range.second ;
    }
    else
    {
        from = 0 ;
        until =  volumeSize();
    }

    uint64_t checksum = 0 ;
    // If brick buffer (data_) is valid, serialize it.
    // Otherwise, extract the brick volume from the base volume.
    if( _volumeData->data )
    {
        auto data = _volumeData->data.get();
        for( uint64_t i = 0 ; i < until - from ; i++ )
            checksum += data[ i ];
    }

    else
    {
        const auto dimensions = _volumeData->dimensions;
        const auto baseDimensions = _volumeData->baseDimensions;
        const auto origin = _volumeData->origin ;

        const auto zFinal = _volumeData->dimensions.z;
        const auto yFinal = _volumeData->dimensions.y;
        const auto xFinal = _volumeData->dimensions.x;
        const auto baseData = _volumeData->baseData.get();
        for( uint64_t k = 0; k < zFinal ; k++ )
            for( uint64_t j = 0; j < yFinal ; j++ )
                for( uint64_t i = 0; i < xFinal; i++ )
                {

                    const uint64_t brickIndex =
                            VolumeUtilities::get1DIndex( i , j , k ,
                                                         dimensions );

                    if( brickIndex >= from &&
                        brickIndex < until )
                    {
                        const uint64_t volumeIndex =
                                VolumeUtilities::get1DIndex( origin.x + i,
                                                             origin.y + j,
                                                             origin.z + k ,
                                                             baseDimensions );
                        checksum += baseData[ volumeIndex ];
                    }

                }
    }
    return checksum ;
}

template< class T >
int SerializableVolume< T >::getSerializedSegmentsCount() const
{
    return *_segmentData->serializedSegmentsCount;
}

template< class T >
int SerializableVolume<T>::getSegmentsCount() const
{
    return _segmentData->segmentsCount;
}

template< class T >
Serializable::SharedPointerType SerializableVolume< T >::toSharedPointer() const
{
    return std::make_shared< SerializableVolume< T >>( *this );
}

template< class T >
Serializable::Scalability SerializableVolume< T >::getScalability() const
{
    return Scalability::GB;
}

template< class T >
Serializable::StreamType &SerializableVolume< T >::_serialize( StreamType &stream ) const
{
    quint64 dx , dy , dz , from , until ;
    dx = static_cast< quint64 >( _volumeData->dimensions.x );
    dy = static_cast< quint64 >( _volumeData->dimensions.y );
    dz = static_cast< quint64 >( _volumeData->dimensions.z );

    if( _segmentData->segmented )
    {
        from = static_cast< quint64 >( _segmentData->range.first );
        until = static_cast< quint64 >( _segmentData->range.second );
    }
    else
    {
        from = 0;
        until = static_cast< quint64 >( volumeSize());
    }

    const auto coordinates = _volumeData->coordinates;
    const auto unitCubeCenter = _volumeData->unitCubeCenter;
    const auto unitCubeScaleFactors = _volumeData->unitCubeScaleFactors;

    stream << dx << dy << dz
           << coordinates.x << coordinates.y << coordinates.z
           << unitCubeCenter.x << unitCubeCenter.y << unitCubeCenter.z
           << unitCubeScaleFactors.x << unitCubeScaleFactors.y
           << unitCubeScaleFactors.z
           << _targetGPUIndex << _segmentData->segmented << _segmentData->segmentsCount
           << _segmentData->currentSegmentRank << from << until;




    uint64_t checksum = 0 ;
    // If brick buffer (data_) is valid, serialize it.
    // Otherwise, extract the brick volume from the base volume.
    if( _volumeData->data )
    {
        auto data = _volumeData->data.get();
        for( auto i = from ; i < until ; i++ )
        {
            stream << data[ i ];
            checksum += data[ i ];
        }
    }
    else
    {
        const auto dimensions = _volumeData->dimensions;
        const auto zFinal = dimensions.z;
        const auto yFinal = dimensions.y;
        const auto xFinal = dimensions.x;
        const auto baseData = _volumeData->baseData.get();
        const auto baseDimensions = _volumeData->baseDimensions;
        const auto origin = _volumeData->origin;

        for( uint64_t k = 0; k < zFinal; k++ )
            for( uint64_t j = 0; j < yFinal; j++ )
                for( uint64_t i = 0; i < xFinal; i++ )
                {

                    const uint64_t brickIndex =
                            VolumeUtilities::get1DIndex( i , j , k ,
                                                         dimensions );

                    if( brickIndex >= from &&
                        brickIndex < until )
                    {
                        const uint64_t volumeIndex =
                                VolumeUtilities::get1DIndex( origin.x + i,
                                                             origin.y + j,
                                                             origin.z + k ,
                                                             baseDimensions );
                        stream << baseData[ volumeIndex ] ;
                        checksum += baseData[ volumeIndex ];
                    }

                }
    }


    _segmentData->serializedSegmentsCount->fetch_add( 1 ) ;
    return stream ;
}

template< class T >
Serializable::StreamType &SerializableVolume<T>::_deserialize( StreamType &stream )
{
    quint64 dx , dy , dz , from , until ;

    auto &dimensions = _volumeData->dimensions;
    auto &coordinates = _volumeData->coordinates;
    auto &unitCubeCenter = _volumeData->unitCubeCenter;
    auto &unitCubeScaleFactors = _volumeData->unitCubeScaleFactors;


    stream >> dx >> dy >> dz
            >> coordinates.x >> coordinates.y >> coordinates.z
            >> unitCubeCenter.x >> unitCubeCenter.y >> unitCubeCenter.z
            >> unitCubeScaleFactors.x >> unitCubeScaleFactors.y
            >> unitCubeScaleFactors.z
            >> _targetGPUIndex >> _segmentData->segmented >> _segmentData->segmentsCount
            >> _segmentData->currentSegmentRank >> from >> until ;

    dimensions.x = static_cast< u_int64_t >( dx );
    dimensions.y = static_cast< u_int64_t >( dy );
    dimensions.z = static_cast< u_int64_t >( dz );

    auto &range = _segmentData->range;
    range.first = from;
    range.second = until;
    const auto nVoxels = until - from;
    _volumeData->data.reset( new T[ nVoxels ] , &Volume< T >::bufferDeleter );
    auto data = _volumeData->data.get();
    uint64_t checksum = 0 ;
    for( auto i = 0 ; i < nVoxels  ; i++ )
    {
        stream >> data[ i ];
        checksum += data[ i ];
    }
    return stream ;
}

template< class T >
std::string SerializableVolume< T >::_getSerializableName() const
{
    if( std::is_same< T , uint8_t >::value )
        return "SerializableVolume<uint8_t>";
    else LOG_ERROR("Unexpected!");
}

template< class T >
void SerializableVolume< T >::_logData( uint64_t checksum ) const
{
    LOG_DEBUG("[DeSerialized] SegmentRank:%ld ,"
              "GPUIndex:%ld, CheckSum:%ld", _segmentData->currentSegmentRank
              ,  _targetGPUIndex , checksum );

    auto &coordinates = _volumeData->coordinates;
    auto &unitCubeCenter = _volumeData->unitCubeCenter;
    auto &unitCubeScaleFactors = _volumeData->unitCubeScaleFactors;
    LOG_DEBUG("[Serialized] Volume Size:%ld; checksum:%ld"
              ";Coordinates(%f,%f,%f);UnitCubeCenter(%f,%f,%f);"
              ";unitCubeScales(%f,%f,%f)", volumeSize() , checksum ,
              coordinates.x , coordinates.y , coordinates.z,
              unitCubeCenter.x , unitCubeCenter.y , unitCubeCenter.z,
              unitCubeScaleFactors.x , unitCubeScaleFactors.y,
              unitCubeScaleFactors.z );
}
#include "SerializableVolume.ipp"
