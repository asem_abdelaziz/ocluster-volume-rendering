#ifndef METASERIALIZATION_HH
#define METASERIALIZATION_HH

// std
#include <type_traits>

// local
#include "SerializableVolume.h"
#include "SerializableFrame.h"
#include "SerializableTransformation.h"
#include "SerializableNumber.h"
#include "SerializableString.h"
#include "CLRendererNodeInfo.h"
#include "CLRendererDeviceInfo.h"
#include "Instruction.h"


class MetaSerialization
{
public:
    template<typename T , typename... Args , typename F = int >
    static void init() {
        if( std::is_same< T , int >::value ) return;
        registerSerializable< T >();
        init< Args... , F >();
    }

    template< typename T >
    static void registerSerializable(){
        static_assert( std::is_base_of< Serializable , T >::value ||
                       std::is_same< T , int >::value ,
                       "T must be a subtype of Serializable.");

        static_assert( !std::is_same< T , Serializable >::value ,
                       "The base class 'Serializable' is pure abstract.");

        if( std::is_same< T , int >::value ) return;

        auto p = new T;
        auto concreteSerializable = ( Serializable *) p;
        registerSerializable< T >( concreteSerializable->_getSerializableName());
        delete p;
    }

    template< typename T >
    static void registerSerializable( const std::string name )
    {
        static_assert( std::is_base_of< Serializable , T >::value ||
                       std::is_same< T , int >::value ,
                       "T must be a subtype of Serializable.");

        static_assert( !std::is_same< T , Serializable >::value ,
                       "The base class 'Serializable' is pure abstract.");

        if( std::is_same< T , int >::value ) return;
        auto factory = &getSerializableFactory< T >;
        Serializable::_registerSerializableObject( name , factory );
    }

    template< typename T >
    static Serializable* getSerializableFactory()
    {
        return ( Serializable* ) new T ;
    }
};


#define CORE_SERIALIZATION_INIT() do \
{ \
    MetaSerialization::init< \
    Instruction, \
    CLRendererNodeInfo , \
    CLRendererDeviceInfo , \
    SerializableTransformation ,\
    SerializableNumber< uint32_t > , \
    SerializableFrame< uint8_t > ,\
    SerializableFrame< float > , \
    SerializableVolume< uint8_t >>();\
    }while( 0 )

#endif // SERIALIZABLE_HH

