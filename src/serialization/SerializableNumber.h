#ifndef SERIALIZABLENUMBER_H
#define SERIALIZABLENUMBER_H

// local
#include "Serializable.h"

template< class T >
class SerializableNumber : public Serializable
{

public:
    /**
     * @brief SerializableFrame
     */
    SerializableNumber();

    SerializableNumber( T number );
    SerializableNumber( const SerializableNumber &other );
    T getNumber() const;
    SharedPointerType toSharedPointer() const override;
protected:
    StreamType &_serialize( StreamType &stream ) const override;
    StreamType &_deserialize( StreamType &stream ) override;
    std::string _getSerializableName() const override;

private:
    T _number;
};

#endif // SERIALIZABLENUMBER_H
