#ifndef SERIALIZABLEFRAME_H
#define SERIALIZABLEFRAME_H

// local
#include "Serializable.h"

// clparen
#include "clparen/clparen.h"


template< class T >
class SerializableFrame : public Serializable
{
public:
    /**
     * @brief SerializableFrame
     * @param data
     * @param width
     * @param height
     */
    explicit SerializableFrame( T *data , const Dimensions2D dimensions );


    /**
     * @brief SerializableFrame
     * @param volume
     */
    explicit SerializableFrame( const clparen::CLData::CLImage2D< T > &clImage2D );

    /**
     * @brief SerializableFrame
     */
    SerializableFrame();

    /**
     * @brief SerializableFrame
     * @param copy
     */
    SerializableFrame( const SerializableFrame< T > &copy );

    /**
     * @brief getConstData
     * @return
     */
    const T *getConstData( ) const ;


    /**
     * @brief shareData
     * @param frame
     */
    void shareData( clparen::CLData::CLFrame< T > &frame ) const;

    /**
     * @brief getData
     * @return
     */
    T *getData( );

    /**
     * @brief imageSize
     * @return
     */
    uint64_t imageSize() const;

    /**
     * @brief dataSize
     * @return
     */
    uint64_t dataSize() const ;


    /**
     * @brief operator =
     * @param volume
     * @return
     */
    SerializableFrame< T >& operator=(
            const clparen::CLData::CLImage2D< T > &clImage2D );


    /**
     * @brief setTargetGPUIndex
     * @param index
     */
    void setGPUIndex( const quint32 index );

    /**
     * @brief getTargetGPUIndex
     */
    quint32 getGPUIndex( ) const ;


    /**
     * @brief getDimensions
     * @return
     */
    Dimensions2D getDimensions() const;

    /**
     * @brief checksum
     * @return
     */
    T checksum() const ;

    /**
     * @brief channelsInPixel
     * @return
     */
    uint8_t getChannelsPerPixel() const ;

    SharedPointerType toSharedPointer() const override;
    Scalability getScalability() const override;
protected:
    StreamType &_serialize( StreamType &stream ) const override;
    StreamType &_deserialize( StreamType &stream ) override;
    std::string _getSerializableName() const override;
private:
    /**
     * @brief data_
     */
    std::shared_ptr< T >data_ ;


    /**
     * @brief dimensions_
     */
    Dimensions2D dimensions_ ;


    /**
     * @brief channelsInPixel_
     */
    uint8_t channelsInPixel_ ;

    /**
     * @brief frameChannelOrder_
     */
    clparen::CLData::FRAME_CHANNEL_ORDER frameChannelOrder_ ;

    /**
     * @brief channelType_
     */
    clparen::CLData::FRAME_CHANNEL_TYPE channelType_ ;

    /**
     * @brief targetGPUIndex_
     */
    quint32 gpuIndex_ ;
};

#endif // SERIALIZABLEFRAME_H
