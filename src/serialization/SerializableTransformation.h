#ifndef SERIALIZABLETRANSFORMATION_H
#define SERIALIZABLETRANSFORMATION_H

// local
#include "Serializable.h"

// clparen
#include "clparen/clparen.h"


class SerializableTransformation : public Serializable , public Transformation
{
public:

    /**
     * @brief operator =
     * @param transformation
     * @return
     */
    SerializableTransformation&
    operator=( const Transformation &transformation );
    SerializableTransformation();

    SharedPointerType toSharedPointer() const;
protected:
    StreamType &_serialize( StreamType &stream ) const override;
    StreamType &_deserialize( StreamType &stream ) override;
    std::string _getSerializableName() const override;

};



#endif // SERIALIZABLETRANSFORMATION_H
