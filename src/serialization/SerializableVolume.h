#ifndef SERIALIZABLEVOLUME_H
#define SERIALIZABLEVOLUME_H


// std
#include <map>
#include <mutex>
#include <memory>
#include <atomic>

// local
#include "Serializable.h"

// clparen
#include "clparen/Volume.h"
#include "clparen/clparen.h"



// Maximum size to serialize at a time in MegaBytes.
// If volume size exceeds, segment the big volume into
// smaller segments. Then serialize consequently.

// Caution: Large segment size may results in undefined
// behaviour for both QTcpSocket and QThreadPool (pooled threads).
#define MAX_SEGMENT_SIZE_MB 50

// Bytes in MegaByte.
#define BYTES_IN_MB 1048576


// Forward declaration
template< typename T > struct SegmentData;
template< typename T > struct VolumeData;


template< class T >
class SerializableVolume : public Serializable
{
public:

    /**
     * @brief SerializableVolume
     */
    SerializableVolume( );

    /**
     * @brief SerializableVolume
     * @param other
     */
    explicit SerializableVolume( const SerializableVolume< T > &other );

    /**
     * @brief SerializableVolume
     * @param brickParameters
     */
    explicit SerializableVolume( const BrickParameters< T > brickParameters ,
                        bool deepCopy = false );

    ~SerializableVolume();

    /**
     * @brief SerializableVolume
     * @param volume
     */
    explicit SerializableVolume( const Volume< T > &volume );


    /**
     * @brief getConstData
     * @return
     */
    const T *getConstData( ) const ;

    /**
     * @brief getData
     * @return
     */
    T *getData( );

    /**
     * @brief volumeSize
     * @return
     */
    uint64_t volumeSize() const;

    /**
     * @brief dataSize
     * @return
     */
    uint64_t dataSize() const ;

    /**
     * @brief dataSizeMB
     * @return
     */
    uint64_t dataSizeMB() const ;

    /**
     * @brief operator =
     * @param volume
     * @return
     */
    SerializableVolume& operator=( const Volume< T > &volume );


    /**
     * @brief setTargetGPUIndex
     * @param index
     */
    void setTargetGPUIndex( int index );

    /**
     * @brief getTargetGPUIndex
     */
    int getTargetGPUIndex( ) const ;

    /**
     * @brief getDimensions
     * @return
     */
    Dimensions3D getDimensions() const;

    /**
     * @brief getCoordinates
     * @return
     */
    Coordinates3D getCoordinates() const;

    /**
     * @brief getUnitCubeCenter
     * @return
     */
    Coordinates3D getUnitCubeCenter() const;

    /**
     * @brief getUnitCubeScaleFactors
     * @return
     */
    Coordinates3D getUnitCubeScaleFactors() const;

    /**
     * @brief maxSegmentSizeMB
     * @return
     */
    uint64_t maxSegmentSizeMB() const ;

    /**
     * @brief maxSegmentSize
     * @return
     */
    uint64_t maxSegmentSize() const ;

    /**
     * @brief makeSegments
     * @return
     */
    std::map< int, std::shared_ptr< SerializableVolume< T >>> &makeSegments( );

    /**
     * @brief insertSegment
     * @param volumeSegment
     */
    void insertSegment( const SerializableVolume< T > &volumeSegment );


    /**
     * @brief allSegmentsAvailable
     * @return
     */
    bool allSegmentsAvailable() const ;

    /**
     * @brief mergeSegments
     */
    void mergeSegments( );


    /**
     * @brief checksum
     * @return
     */
    uint64_t checksum() const ;


    /**
     * @brief getSerializedSegmentsCount
     * @return
     */
    int getSerializedSegmentsCount() const;

    /**
     * @brief getSegmentsCount
     * @return
     */
    int getSegmentsCount() const;

    SharedPointerType toSharedPointer() const override;

    Scalability getScalability() const override;
protected:
    StreamType &_serialize( StreamType &stream ) const override;
    StreamType &_deserialize( StreamType &stream ) override;
    std::string _getSerializableName() const override;

private:
    /**
     * @brief _logData
     */
    void _logData( uint64_t checksum ) const;

private:
    std::unique_ptr< SegmentData< T >> _segmentData;
    std::unique_ptr< VolumeData< T >> _volumeData;
    int _targetGPUIndex ;


};

#endif // SERIALIZABLEVOLUME_H
