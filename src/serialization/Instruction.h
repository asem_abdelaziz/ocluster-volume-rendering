#ifndef INSTRUCTION_H
#define INSTRUCTION_H

// std
#include <memory>

// Qt
#include <QTcpSocket>

// local
#include "Serializable.h"
#include "SerializableNumber.h"

// forward declaration
class InstructionData;

class Instruction : public Serializable
{

public :

    /**
     * @brief The DataType enum
     */
    enum class DataType : quint8 {
        String ,
        Volume ,
        VolumeSegment ,
        UInterger32 ,
        UInterger32List ,
        Frame ,
        NodeInfo ,
        Transformation ,
        NoData ,
    };

    /**
 * @brief The Message enum
 * Information: Request Information about client and its renderer device.
 * Render: Apply the attached Transformation and start rendering.
 * LoadVolume: Load the attached volume to the given local GPU Index.
 */

    enum class Message : quint8 {
        NodeInfo ,
        DeployGPU ,
        Render ,
        LoadVolume ,
        NodeReady ,
        SwitchRenderingKernel ,
        Error ,
        Echo
    };

    /**
 * @brief The MessageDirection enum
 * Request: Instruction From Source to Destination.
 * Reply: Response (+Acknoweldgement) from Destination to Source.
 */

    enum class MessageDirection : quint8 {
        Request ,
        Reply
    };



public:

    /**
     * @brief Instruction
     */
    Instruction( );

    /**
     * @brief Instruction
     * @param other
     */
    Instruction( const Instruction &other );

    /**
     * @brief Instruction
     * @param message
     * @param messageDirection
     * @param dataType
     * @param data
     */
    Instruction( const Message getMessage ,
                 const MessageDirection getMessageDirection ,
                 const DataType getDataType = DataType::NoData );

    /**
     * @brief Instruction
     * @param message
     * @param messageDirection
     * @param dataType
     * @param data
     */
    Instruction( const Message getMessage ,
                 const MessageDirection getMessageDirection ,
                 const DataType getDataType ,
                 const Serializable *data );

    /**
     * @brief Instruction
     * @param message
     * @param messageDirection
     * @param dataType
     * @param data
     */
    Instruction( const Message getMessage ,
                 const MessageDirection getMessageDirection ,
                 const DataType dataType ,
                 const SerializableNumber<uint32_t> &data );


    /**
     * @brief Instruction
     * @param message
     * @param messageDirection
     * @param dataType
     * @param data
     */
    Instruction( const Message getMessage ,
                 const MessageDirection getMessageDirection ,
                 const DataType getDataType ,
                 Serializable::SharedPointerType data );


    ~Instruction();

    /**
     * @brief data
     * @return
     */
    const Serializable &getData( ) const;

    const Serializable *getDataPtr( ) const;

    /**
     * @brief message
     * @return
     */
    Message getMessage( ) const ;

    /**
     * @brief messageDirection
     * @return
     */
    MessageDirection getMessageDirection( ) const ;

    /**
     * @brief dataType
     * @return
     */
    DataType getDataType() const ;

    /**
     * @brief toString
     * @return
     */
    std::string toString() const;

    /**
     * @brief setData
     * @param data
     */
    void setData( Serializable::SharedPointerType data );
    void setData( Serializable const * data );
    void setData( const SerializableNumber< uint32_t > &data );

    SharedPointerType toSharedPointer() const override;
    Scalability getScalability() const override;
protected:
    StreamType &_serialize( StreamType &stream ) const override;
    StreamType &_deserialize( StreamType &stream ) override;
    std::string _getSerializableName() const override;

private:

    /**
     * @brief message_
     */
    Message _message ;

    /**
     * @brief messageDirection_
     */
    MessageDirection _messageDirection ;

    /**
     * @brief dataType_
     */
    DataType _dataType ;

    /**
     * @brief _data
     */
    std::unique_ptr< InstructionData > _data;
};


#endif // INSTRUCTION_H
