#ifndef ABSTRACTSERIALIZABLE
#define ABSTRACTSERIALIZABLE

// std
#include <string>
#include <memory>
#include <mutex>

// Qt
#include <QDataStream>

// local
#include "Logger.h"


// Forward declarations
class MetaSerialization;
class Instruction;


class Serializable
{

public:
    enum class Scalability : int {
        KB = 0 ,
        MB ,
        GB ,
        EnumOffset
    };

public:
    typedef QDataStream StreamType;
    typedef std::shared_ptr< Serializable > SharedPointerType;
    typedef Serializable* (*FactoryFunction)();

public:
    Serializable();
    virtual ~Serializable();
    static Serializable* getInstance( const std::string &name );


    QDataStream &serialize(QDataStream &stream ) const;
    /**
     * @brief toSharedPointer
     * @return
     */
    virtual SharedPointerType toSharedPointer() const = 0;
    virtual Scalability getScalability() const;
protected:
    friend class Instruction;

    virtual StreamType &_serialize( StreamType &stream ) const = 0 ;
    virtual StreamType &_deserialize( StreamType &stream ) = 0 ;
    virtual std::string _getSerializableName() const = 0;

protected:
    friend StreamType&
    operator<< (  StreamType& stream ,
                  const Serializable* serializable );


    friend StreamType&
    operator>> ( StreamType& stream ,
                 Serializable* &serializable );

private:
    friend class MetaSerialization;
    static void _registerSerializableObject( const std::string name , const FactoryFunction creator );
    static std::map< std::string , FactoryFunction > _registeredObjects;
    static std::mutex _registeredObjectsMutex;
};

Serializable::StreamType& operator<<( Serializable::StreamType& stream ,
                                      const Serializable* serializable );


Serializable::StreamType& operator>>( Serializable::StreamType& stream ,
                                      Serializable* &serializable );


#endif // ABSTRACTSERIALIZABLE

