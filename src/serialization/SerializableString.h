#ifndef SERIALIZABLESTRING_H
#define SERIALIZABLESTRING_H

// std
#include <string>

// local
#include "Serializable.h"

class SerializableString : public Serializable
{
public:
    SerializableString();
};

#endif // SERIALIZABLESTRING_H
