#include "Serializable.h"

std::map< std::string , Serializable::FactoryFunction >
Serializable::_registeredObjects = std::map< std::string , Serializable::FactoryFunction >();
std::mutex Serializable::_registeredObjectsMutex;

void Serializable::_registerSerializableObject(
        const std::string name,
        const Serializable::FactoryFunction creator)
{
    std::lock_guard<std::mutex> guard( _registeredObjectsMutex );
    _registeredObjects[ name ] = creator;
    LOG_INFO("Type %s is registered!", name.c_str());
}

Serializable* Serializable::getInstance(
        const std::string &name )
{
    try{
        return  _registeredObjects.at( name )();
    }catch( const std::out_of_range &e ){
        printf("Registered Types:\n");
        for( auto it = _registeredObjects.begin() ;
             it != _registeredObjects.end() ;
             it++ )
            printf("Type:%s\n", it->first.c_str());

        LOG_ERROR("Type: %s is not registered!",name.c_str());
    }
}

QDataStream &Serializable::serialize( QDataStream &stream ) const
{
    return operator<<( stream , this );
}

Serializable::Scalability Serializable::getScalability() const
{
    return Scalability::KB;
}

Serializable::Serializable()
{

}

Serializable::~Serializable()
{

}

Serializable::StreamType &operator<<(
        Serializable::StreamType &stream,
        const Serializable* serializable )
{
    const std::string name = serializable->_getSerializableName();
    const QString qName = QString::fromStdString( name );
    stream << qName ;
    LOG_DEBUG("Serialize:%s", name.c_str());
    return serializable->_serialize( stream );
}

Serializable::StreamType &operator>>(
        Serializable::StreamType &stream,
        Serializable* &serializable )
{
    QString name;
    stream >> name;
    LOG_DEBUG("DeSerialize:%s", name.toStdString().c_str());
    serializable = Serializable::getInstance( name.toStdString());
    return serializable->_deserialize( stream );
}
