#include "SerializableFrame.h"
#include "Logger.h"

template< class T >
SerializableFrame< T >::SerializableFrame()
{
    gpuIndex_ = 0 ;
    dimensions_.x = 0 ;
    dimensions_.y = 0 ;
    channelsInPixel_ = 1 ;
    channelType_ = clparen::CLData::CLFrame< T >::frameChannelType();
    frameChannelOrder_ = clparen::CLData::CLFrame< T >::defaultChannelOrder();

}

template< class T >
SerializableFrame< T >::SerializableFrame( T *data ,
                                           const Dimensions2D dimensions )
    : data_( data ) ,
      dimensions_( dimensions )
{
    channelsInPixel_ = 1 ;
    channelType_ = clparen::CLData::CLFrame< T >::frameChannelType();
    frameChannelOrder_ = clparen::CLData::CLFrame< T >::defaultChannelOrder();

}

template< class T >
SerializableFrame< T >::SerializableFrame(
        const clparen::CLData::CLImage2D<T> &clImage2D )
    : data_( clImage2D.hostData_ ) ,
      dimensions_( clImage2D.dimensions_ ),
      channelsInPixel_( clImage2D.channelsInPixel()) ,
      channelType_( clparen::CLData::CLFrame< T >::frameChannelType( )),
      frameChannelOrder_( clImage2D.channelOrder())
{

}


template< class T >
SerializableFrame< T >::SerializableFrame( const SerializableFrame< T > &copy )
    : data_( copy.data_ ) ,
      dimensions_( copy.dimensions_ ) ,
      gpuIndex_( copy.gpuIndex_ ),
      channelsInPixel_( copy.channelsInPixel_ ) ,
      channelType_( copy.channelType_ ),
      frameChannelOrder_( copy.frameChannelOrder_ )
{

}

template< class T >
const T *SerializableFrame< T >::getConstData() const
{
    return data_.get();
}

template< class T >
void SerializableFrame< T >::shareData( clparen::CLData::CLFrame<T> &frame ) const
{
    frame.hostData_ =  data_;
}

template< class T >
T *SerializableFrame< T >::getData()
{
    return data_.get();
}

template< class T >
uint64_t SerializableFrame< T >::imageSize() const
{
    return dimensions_.imageSize() ;
}

template< class T >
uint64_t SerializableFrame< T >::dataSize() const
{
    return imageSize() * channelsInPixel_ * sizeof( T );
}

template< class T >
SerializableFrame< T > &
SerializableFrame< T >::operator=(
        const clparen::CLData::CLImage2D<T> &clImage2D )
{
    data_ = clImage2D.hostData_ ;
    dimensions_ = clImage2D.dimensions_ ;
    channelsInPixel_ = clImage2D.channelsInPixel();
    channelType_ = clparen::CLData::CLFrame< T >::frameChannelType();
    frameChannelOrder_ = clImage2D.channelOrder();
}

template< class T >
void SerializableFrame< T >::setGPUIndex( const quint32 index )
{
    gpuIndex_ = index ;
}

template< class T >
quint32 SerializableFrame< T >::getGPUIndex() const
{
    return gpuIndex_ ;
}

template< class T >
Dimensions2D SerializableFrame< T >::getDimensions() const
{
    return dimensions_ ;
}

template< class T >
T SerializableFrame< T >::checksum() const
{
    T checksum = 0 ;
    auto data = data_.get();
    for( quint64 i = 0 ; i < dimensions_.imageSize() * channelsInPixel_ ; i++ )
        checksum += data[ i ];

    return checksum ;
}

template< class T >
uint8_t SerializableFrame< T >::getChannelsPerPixel() const
{
    return channelsInPixel_;
}

template< class T >
Serializable::SharedPointerType SerializableFrame< T >::toSharedPointer() const
{
    return SharedPointerType(
                new SerializableFrame< T >( *this ));
}

template< class T >
Serializable::Scalability SerializableFrame< T >::getScalability() const
{
    return Scalability::MB;
}

template< class T >
Serializable::StreamType &SerializableFrame< T >::_serialize(
        StreamType &stream ) const
{
    quint32 dx , dy ;
    dx = static_cast< quint32 >( dimensions_.x );
    dy = static_cast< quint32 >( dimensions_.y );


    stream << dx
           << dy
           << static_cast< quint32 >( channelType_ )
           << static_cast< quint32 >( frameChannelOrder_ )
           << channelsInPixel_
           << gpuIndex_ ;

    const uint64_t imgSize = imageSize();
    const uint8_t channelsInPixel = getChannelsPerPixel();
    auto data = data_.get();

    for( uint64_t i = 0 ; i < imgSize * channelsInPixel ; i++ )
        stream << data[ i ];


    LOG_DEBUG("[Serialize] Image Size = %d x %d = %ld"
              "| GPU< %d >; checksum:%f",
              dx , dy , imgSize , gpuIndex_ ,
              checksum() );

    return stream ;
}

template< class T >
Serializable::StreamType &SerializableFrame< T >::_deserialize(
        StreamType &stream )
{
    quint32 dx , dy , channelType ,channelOrder;

    stream >> dx
            >> dy
            >> channelType
            >> channelOrder
            >> channelsInPixel_
            >> gpuIndex_ ;

    dimensions_.x = static_cast< u_int64_t >( dx );
    dimensions_.y = static_cast< u_int64_t >( dy );

    channelType_ =
            static_cast< clparen::CLData::FRAME_CHANNEL_TYPE >( channelType );
    frameChannelOrder_ =
            static_cast< clparen::CLData::FRAME_CHANNEL_ORDER >( channelOrder );

    const uint64_t imgSize = imageSize();
    const uint8_t channelsInPixel = getChannelsPerPixel();

    data_.reset( new T[ imgSize * channelsInPixel ] ,
            []( T const * p ){ delete []p ;} );
    auto data = data_.get();

    for( uint64_t i = 0 ; i < imgSize * channelsInPixel ; i++ )
        stream >> data[ i ];


    //    LOG_DEBUG("[DeSerialize] Image Size = %ld x %ld = %ld"
    //              "- GPU< %d >; checksum:%f",
    //              f.dimensions_.x , f.dimensions_.y , imgSize ,
    //              f.gpuIndex_ , checksum );
    return stream ;
}

template< class T >
std::string SerializableFrame< T >::_getSerializableName() const
{
    if( std::is_same< T , float >::value )
        return "SerializableFrame<float>";
    if( std::is_same< T , uint8_t >::value )
        return "SerializableFrame<uint8_t>";
    else LOG_ERROR("Unexpected!");
}


#include "SerializableFrame.ipp"


