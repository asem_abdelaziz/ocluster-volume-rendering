#ifndef SERIALIZABLENUMBER_IPP
#define SERIALIZABLENUMBER_IPP

template class SerializableNumber< uint8_t > ;
template class SerializableNumber< uint16_t > ;
template class SerializableNumber< uint32_t > ;
//template class SerializableNumber< uint64_t > ;
template class SerializableNumber< int8_t > ;
template class SerializableNumber< int16_t > ;
template class SerializableNumber< int32_t > ;
//template class SerializableNumber< int64_t > ;
template class SerializableNumber< float > ;
template class SerializableNumber< double > ;


#endif // SERIALIZABLENUMBER_IPP

