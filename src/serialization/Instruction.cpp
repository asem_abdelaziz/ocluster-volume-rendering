#include "Instruction.h"
#include "Logger.h"


class InstructionData {
    enum class Store : uint8_t { SharedPointer , StackPointer , Value };

public:
    explicit InstructionData( Serializable::SharedPointerType sharedData )
        : _sharedDataPointer( sharedData ) , _store( Store::SharedPointer ){}
    explicit InstructionData( Serializable const *stackData )
        : _stackDataPointer( stackData ) , _store( Store::StackPointer ){}
    explicit InstructionData( SerializableNumber< uint32_t > number )
        : _number( number ) , _store( Store::Value ){}
    explicit InstructionData()
        : _store( Store::Value ){}

    inline const Serializable &getData() const {
        return *getDataPtr();
    }

    inline const Serializable *getDataPtr() const {
        if( _store == Store::SharedPointer )
            return _sharedDataPointer.get();
        if( _store == Store::StackPointer )
            return _stackDataPointer ;
        if( _store == Store::Value )
            return  &_number ;
    }

    inline void setData( Serializable::SharedPointerType data ){
        _sharedDataPointer = data ;
        _store = Store::SharedPointer;
    }

    inline void setData( Serializable const * data ){
        _stackDataPointer = data;
        _store = Store::StackPointer;
    }

    inline void setData( const SerializableNumber< uint32_t > &number  ){
        _number = number;
        _store = Store::Value;
    }

    inline Serializable::Scalability getScalability() const
    {
        return getData().getScalability();
    }

private:
    /**
     * @brief store
     */
    Store _store;

    /**
     * @brief data_
     */
    Serializable::SharedPointerType _sharedDataPointer ;

    /**
     * @brief _staticDataPtr
     */
    Serializable const * _stackDataPointer;

    /**
     * @brief _data
     */
    SerializableNumber< uint32_t > _number;
};


Instruction::Instruction( const Instruction::Message message ,
                          const Instruction::MessageDirection messageDirection ,
                          const Instruction::DataType dataType )
    : _message( message ),
      _messageDirection( messageDirection ),
      _dataType( dataType ),
      _data( new InstructionData )
{

}
Instruction::Instruction( const Instruction::Message message ,
                          const Instruction::MessageDirection messageDirection ,
                          const Instruction::DataType dataType ,
                          Serializable const *data )
    : _message( message ),
      _messageDirection( messageDirection ),
      _dataType( dataType ),
      _data( new InstructionData( data ))
{


}

Instruction::Instruction( const Instruction::Message message,
                          const Instruction::MessageDirection messageDirection,
                          const Instruction::DataType dataType,
                          const SerializableNumber< uint32_t > &data )
    : _message( message ),
      _messageDirection( messageDirection ),
      _dataType( dataType ),
      _data( new InstructionData( data ))
{

}

Instruction::Instruction(
        const Instruction::Message message,
        const Instruction::MessageDirection messageDirection,
        const Instruction::DataType dataType,
        Serializable::SharedPointerType data )
    : _message( message ),
      _messageDirection( messageDirection ),
      _dataType( dataType ),
      _data( new InstructionData( data ))
{
}

Instruction::~Instruction()
{

}

const Serializable &Instruction::getData() const
{
    return _data->getData();
}

Serializable const* Instruction::getDataPtr() const
{
    return _data->getDataPtr();
}

Instruction::Instruction() :
    _data( new InstructionData )
{

}

Instruction::Instruction( const Instruction &other )
    : _message( other._message ),
      _messageDirection( other._messageDirection ),
      _dataType( other._dataType ),
      _data( new InstructionData( *other._data ))

{

}


Instruction::Message
Instruction::getMessage() const
{
    return _message ;
}

Instruction::MessageDirection
Instruction::getMessageDirection() const
{
    return _messageDirection ;
}

Instruction::DataType Instruction::getDataType() const
{
    return _dataType ;
}

std::string Instruction::toString() const
{
    static auto messages = std::map< Instruction::Message , std::string >();
    static auto direction = std::map< Instruction::MessageDirection , std::string >();
    static auto dataTypes = std::map< Instruction::DataType , std::string >();
    if( messages.empty())
    {
        messages[ Message::NodeInfo ] = "NodeInfo";
        messages[ Message::DeployGPU ] = "DeployGPU";
        messages[ Message::Render ] = "Render";
        messages[ Message::LoadVolume ] = "LoadVolume" ;
        messages[ Message::NodeReady ] = "NodeReady" ;
        messages[ Message::SwitchRenderingKernel ] = "SwitchRenderingKernel";
        messages[ Message::Error ] = "Error" ;
        messages[ Message::Echo ] = "Echo";
    }
    if( direction.empty())
    {
        direction[ MessageDirection::Reply ] = "Reply";
        direction[ MessageDirection::Request ] = "Request";
    }
    if( dataTypes.empty())
    {
        dataTypes[ DataType::String ] = "String" ;
        dataTypes[ DataType::Volume ] = "Volume" ;
        dataTypes[ DataType::VolumeSegment ] = "VolumeSegment" ;
        dataTypes[ DataType::UInterger32 ] = "UInterger32" ;
        dataTypes[ DataType::UInterger32List ] = "UInterger32List" ;
        dataTypes[ DataType::Frame ] = "Frame" ;
        dataTypes[ DataType::NodeInfo ] = "NodeInfo" ;
        dataTypes[ DataType::Transformation ] = "Transformation" ;
        dataTypes[ DataType::NoData ] = "NoData" ;
    }

    std::string s = "[Message:" + messages[ _message ] + "]" +
                    "[Type:" + dataTypes[ _dataType ] + "]" +
                    "[Direction:" + direction[ _messageDirection ] + "]" +
                    "[Data:" + _data->getData()._getSerializableName() + "]";
    return s;
}



void Instruction::setData( Serializable const* data )
{
    _data->setData( data );
}

void Instruction::setData( const SerializableNumber<uint32_t> &data )
{
    _data->setData( data );
}

void Instruction::setData( Serializable::SharedPointerType data )
{
    _data->setData( data );
}

Serializable::SharedPointerType Instruction::toSharedPointer() const
{
    return std::make_shared< Instruction >( *this );
}

Serializable::Scalability Instruction::getScalability() const
{
    return _data->getScalability();
}

Serializable::StreamType &Instruction::_serialize(
        Serializable::StreamType &stream ) const
{
    LOG_DEBUG("Serialize Instruction:%s", toString().c_str() );
    stream << static_cast< quint8 >( _message )
           << static_cast< quint8 >( _messageDirection )
           << static_cast< quint8 >( _dataType );


    if( _dataType != DataType::NoData )
    {
        const std::string name = _data->getData()._getSerializableName();
        stream << QString::fromStdString( name );
        _data->getData()._serialize( stream );
    }
    else LOG_DEBUG("No data");

    return stream ;
}

Serializable::StreamType &Instruction::_deserialize(
        Serializable::StreamType &stream )
{
    quint8 message , messageDirection , dataType;
    stream  >> message
            >> messageDirection
            >> dataType ;
    _message =
            static_cast< Instruction::Message >( message ) ;
    _messageDirection =
            static_cast< Instruction::MessageDirection >( messageDirection ) ;
    _dataType =
            static_cast< Instruction::DataType >( dataType ) ;

    if( _dataType != DataType::NoData )
    {
        QString name;
        stream >> name;
        auto data = Serializable::getInstance( name.toStdString());
        data->_deserialize( stream );
        _data->setData( std::shared_ptr< Serializable >( data ));
    }
    LOG_DEBUG("DeSerialize Instruction:%s",toString().c_str());
    return stream;
}

std::string Instruction::_getSerializableName() const
{
    return "Instruction";
}
