#include "SerializableNumber.h"

template< class T >
SerializableNumber< T >::SerializableNumber( T number )
    : _number( number )
{


}

template< class T >
SerializableNumber< T >::SerializableNumber( const SerializableNumber &other )
    : _number( other._number )
{


}

template< class T >
T SerializableNumber< T >::getNumber() const
{
    return _number;
}

template< class T >
Serializable::SharedPointerType SerializableNumber< T >::toSharedPointer() const
{
    return std::make_shared< SerializableNumber< T >>( _number );
}

template< class T >
Serializable::StreamType &SerializableNumber< T >::_serialize(
        Serializable::StreamType &stream ) const
{
    stream << _number;
    return stream;
}

template< class T >
Serializable::StreamType &SerializableNumber< T >::_deserialize(
        Serializable::StreamType &stream )
{
    stream >> _number;
    return stream;
}

template< class T >
std::string SerializableNumber< T >::_getSerializableName() const
{
    if( std::is_same< T , uint8_t >::value )
        return "SerializableNumber<uint8_t>";
    if( std::is_same< T , uint16_t >::value )
        return "SerializableNumber<uint16_t>";
    if( std::is_same< T , uint32_t >::value )
        return "SerializableNumber<uint32_t>";
    if( std::is_same< T , uint64_t >::value )
        return "SerializableNumber<uint64_t>";
    if( std::is_same< T , int8_t >::value )
        return "SerializableNumber<int8_t>";
    if( std::is_same< T , int16_t >::value )
        return "SerializableNumber<int16_t>";
    if( std::is_same< T , int32_t >::value )
        return "SerializableNumber<int32_t>";
    if( std::is_same< T , int64_t >::value )
        return "SerializableNumber<int64_t>";
    if( std::is_same< T , float >::value )
        return "SerializableNumber<float>";
    if( std::is_same< T , double >::value )
        return "SerializableNumber<double>";
    else LOG_ERROR("Unexpected!");
}

template< class T >
SerializableNumber< T >::SerializableNumber()
    : _number( 0 )
{


}

#include "SerializableNumber.ipp"
