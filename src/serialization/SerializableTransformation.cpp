#include "SerializableTransformation.h"
#include "Logger.h"


SerializableTransformation::SerializableTransformation()
{

}

Serializable::SharedPointerType
SerializableTransformation::toSharedPointer() const
{
    return std::make_shared< SerializableTransformation >( *this );
}

SerializableTransformation &SerializableTransformation::
operator=( const Transformation &t )
{
    rotation                = t.rotation ;
    translation             = t.translation ;
    scale                   = t.scale ;
    brightness              = t.brightness ;
    volumeDensity           = t.volumeDensity  ;
    transferFunctionFlag    = t.transferFunctionFlag;
    transferFunctionScale   = t.transferFunctionScale;
    transferFunctionOffset  = t.transferFunctionOffset ;
    stepSize                = t.stepSize;
    maxSteps                = t.maxSteps;
    isoValue                = t.isoValue;
}

Serializable::StreamType &SerializableTransformation::_serialize(
        StreamType &stream ) const
{
    const Transformation &t = *this;
    stream << t.rotation.x << t.rotation.y << t.rotation.z
           << t.translation.x << t.translation.y << t.translation.z
           << t.scale.x << t.scale.y << t.scale.z
           << t.brightness << t.volumeDensity << t.stepSize << t.maxSteps << t.isoValue ;

    //    LOG_DEBUG("[Serialize]R(%f,%f,%f)-T(%f,%f,%f)-S(%f,%f,%f)-B(%f)-D(%f)",
    //              t.rotation.x , t.rotation.y , t.rotation.z ,
    //              t.translation.x , t.translation.y , t.translation.z ,
    //              t.scale.x , t.scale.y , t.scale.z ,
    //              t.brightness , t.volumeDensity );

    return stream ;
}

Serializable::StreamType &SerializableTransformation::_deserialize(
        StreamType &stream )
{
    Transformation &t = *this;
    stream >> t.rotation.x >> t.rotation.y >> t.rotation.z
            >> t.translation.x >> t.translation.y >> t.translation.z
            >> t.scale.x >> t.scale.y >> t.scale.z
            >> t.brightness >> t.volumeDensity >> t.stepSize >> t.maxSteps >> t.isoValue ;

    //    LOG_DEBUG("[DeSerialize]R(%f,%f,%f)-T(%f,%f,%f)-S(%f,%f,%f)-B(%f)-D(%f)",
    //              t.rotation.x , t.rotation.y , t.rotation.z ,
    //              t.translation.x , t.translation.y , t.translation.z ,
    //              t.scale.x , t.scale.y , t.scale.z ,
    //              t.brightness , t.volumeDensity );

    return stream ;
}

std::string SerializableTransformation::_getSerializableName() const
{
    return "SerializableTransformation";
}

