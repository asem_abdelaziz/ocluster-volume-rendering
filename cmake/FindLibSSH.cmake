# - Try to find LibSSH
# Once done this will define
#
#  LIBSSH_FOUND - system has LibSSH
#  LIBSSH_INCLUDE_DIRS - the LibSSH include directory
#  LIBSSH_LIBRARIES - Link these to use LibSSH
#  LIBSSH_DEFINITIONS - Compiler switches required for using LibSSH
#
#  Copyright (c) 2009 Andreas Schneider <mail@cynapses.org>
#
#  Redistribution and use is allowed according to the terms of the New
#  BSD license.
#  For details see the accompanying COPYING-CMAKE-SCRIPTS file.
#

if (LIBSSH_LIBRARIES AND LIBSSH_INCLUDE_DIRS)
  # in cache already
  set(LIBSSH_FOUND TRUE)
else (LIBSSH_LIBRARIES AND LIBSSH_INCLUDE_DIRS)

  find_path(LIBSSH_INCLUDE_DIRS
    NAMES
      libsshpp.hpp
    PATHS
      /usr/local/include/libssh
      /usr/include/libssh
      /opt/local/include/libssh
      ${CMAKE_INCLUDE_PATH}/libssh
      ${CMAKE_INSTALL_PREFIX}/include/libssh
    NO_DEFAULT_PATH
  )
  
  find_library(LIBSSH_LIBRARIES
    NAMES
      ssh
    PATHS      
    /usr/local/lib
    /usr/lib
    /opt/local/lib
    ${CMAKE_LIBRARY_PATH}
    ${CMAKE_INSTALL_PREFIX}/lib
    NO_DEFAULT_PATH
  )

  if (LIBSSH_INCLUDE_DIRS AND LIBSSH_LIBRARIES)
    set(LIBSSH_FOUND TRUE)
  endif (LIBSSH_INCLUDE_DIRS AND LIBSSH_LIBRARIES)


  if (LIBSSH_FOUND)
    include(FindPackageHandleStandardArgs)
    find_package_handle_standard_args(LibSSH DEFAULT_MSG LIBSSH_LIBRARIES LIBSSH_INCLUDE_DIRS)
    message("libssh found.")
    message( ${LIBSSH_LIBRARIES} )
    message( ${LIBSSH_INCLUDE_DIRS} )
  else(LIBSSH_FOUND)
    message("libssh not found.")
  endif (LIBSSH_FOUND)

  # show the LIBSSH_INCLUDE_DIRS and LIBSSH_LIBRARIES variables only in the advanced view
  mark_as_advanced( LIBSSH_LIBRARIES LIBSSH_INCLUDE_DIRS )
endif (LIBSSH_LIBRARIES AND LIBSSH_INCLUDE_DIRS)
