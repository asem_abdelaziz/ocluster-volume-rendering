### Find the include directory 
find_path( CLPAREN_INCLUDE_DIR
    NAMES
    clparen/clparen.h
    PATHS
    /usr/include
    /usr/local/include
    /opt/local/include
    /projects/clparen/include
    )

### Find the kernels directory
find_path( CLPAREN_KERNELS_DIR
    NAMES
    xray.cl
    PATHS
    /usr/share
    /usr/local/share
    /opt/local/share
    /projects/clparen/share
    )

### Find the library 
find_library( CLPAREN_LIBRARY
    NAMES
    clparen
    PATHS
    /usr/lib
    /usr/local/lib
    /opt/local/lib
    /projects/clparen/lib
    )

include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( CLPAREN
    DEFAULT_MSG CLPAREN_LIBRARY CLPAREN_INCLUDE_DIR CLPAREN_KERNELS_DIR
    )

if( CLPAREN_FOUND )
    message( STATUS "clparen Found" )
    message("-- clparen include directory : ${CLPAREN_INCLUDE_DIR}")
    message("-- clparen library directory : ${CLPAREN_LIBRARY}")
    message("-- clparen kernels directory : ${CLPAREN_KERNELS_DIR}")

    include_directories( ${CLPAREN_INCLUDE_DIR} )
    link_libraries( ${CLPAREN_LIBRARY} )
    file(GLOB CLPAREN_KERNELS_FILES
        ${CLPAREN_KERNELS_DIR}/*.cl )
#    SET(CLPAREN_KERNELS_FILES  "${CLPAREN_KERNELS_FILES}" CACHE INTERNAL "")

    foreach( kernel_file ${CLPAREN_KERNELS_FILES} )
        message("kernel:${kernel_file}")
    endforeach( )

else( CLPAREN_FOUND )
    message(FATAL_ERROR "clparen NOT Found")
endif( CLPAREN_FOUND )
